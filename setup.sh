#!/bin/bash -f

alias cwb_mkchunk="${CWB_CONFIG}/SCRIPTS/cwb_mkchunk.csh"

alias cwb_ckchunk="${CWB_CONFIG}/SCRIPTS/cwb_ckchunk.csh"

alias cwb_clchunk="${CWB_CONFIG}/SCRIPTS/cwb_clchunk.csh"

alias cwb_ppchunk="${CWB_CONFIG}/SCRIPTS/cwb_ppchunk.csh"

alias cwb_lschunk="${CWB_CONFIG}/SCRIPTS/cwb_lschunk.csh"

alias cwb_mksdirs="${CWB_CONFIG}/SCRIPTS/cwb_mksdirs.csh"

alias cwb_obchunk="${CWB_CONFIG}/SCRIPTS/cwb_obchunk.csh"

alias cwb_mklinks="${CWB_CONFIG}/SCRIPTS/cwb_mklinks.csh"

alias cwb_otchunk="${CWB_CONFIG}/SCRIPTS/cwb_otchunk.csh"

source ${CWB_CONFIG}/O1/setup.sh 
source ${CWB_CONFIG}/O2/setup.sh 
source ${CWB_CONFIG}/O3/setup.sh 
