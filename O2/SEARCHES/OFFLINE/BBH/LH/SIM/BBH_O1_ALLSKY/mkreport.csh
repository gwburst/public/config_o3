#!/bin/tcsh -f

onintr irq_ctrlc

unsetenv CWB_MERGE_LABEL

if ((( $1 == '' ))) then
  echo ""
  echo "CWB_MERGE_LABEL not defined"
  echo ""
  exit 1
else
  setenv CWB_MERGE_LABEL $1
endif

${CWB_SCRIPTS}/cwb_setcuts.csh $CWB_MERGE_LABEL ' --wcuts (factor==1)||(factor==2)||(factor==7)||(factor==8)  --label Q1 --mcuts (factor==1)||(factor==2)||(factor==7)||(factor==8)'
${CWB_SCRIPTS}/cwb_setcuts.csh $CWB_MERGE_LABEL ' --wcuts (factor==3)||(factor==4)||(factor==9)||(factor==10)  --label Q2 --mcuts (factor==3)||(factor==4)||(factor==9)||(factor==10)'
${CWB_SCRIPTS}/cwb_setcuts.csh $CWB_MERGE_LABEL ' --wcuts (factor==5)||(factor==6)||(factor==11)||(factor==12)  --label Q4 --mcuts (factor==5)||(factor==6)||(factor==11)||(factor==12)'

$HOME_WAT/tools/cwb/postproduction/scripts/cbc_plots_ifar.csh $CWB_MERGE_LABEL.C_Q1
$HOME_WAT/tools/cwb/postproduction/scripts/cbc_plots_ifar.csh $CWB_MERGE_LABEL.C_Q2
$HOME_WAT/tools/cwb/postproduction/scripts/cbc_plots_ifar.csh $CWB_MERGE_LABEL.C_Q4

unsetenv CWB_MERGE_LABEL

exit 0
irq_ctrlc:
  ps T | grep root | awk '{print $1}' | xargs kill -9
  exit 1


