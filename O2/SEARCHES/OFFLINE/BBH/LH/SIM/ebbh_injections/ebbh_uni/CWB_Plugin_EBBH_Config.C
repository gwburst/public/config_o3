{
  cout << "-----> macro/CWB_Plugin_EBBH_Config.C" << endl;

  CWB::mdc MDC(net);

  char wf_name[256];
  char fname[256];
  waveform wf;
  vector<mdcpar> par; 

  int seed = net->nRun;  // WARNING : seed must be the same for each detector within the same job
  sprintf(fname,"%s/O2/SEARCHES/OFFLINE/BBH/LH/SIM/ebbh_mwb2/eBBH_%d.lst",cwb_config_env,gIFACTOR);
  par.resize(1);
  par[0].name=fname; 
  MDC.AddWaveform(MDC_EBBH, par);
/*
  par.resize(2);
  par[0].name="macro/eBBH.root"; 
  par[1].name="rp0>26"; 
  MDC.AddWaveform(MDC_EBBH, par);
*/
  MDC.Print(0);

  // --------------------------------------------------------
  // define injection parameters
  // --------------------------------------------------------
  //MDC.SetInjHrss(2.5e-21); Overriding Hrss
  MDC.SetInjRate(0.0125);   // No of injections per second
  MDC.SetInjJitter(3.0);  // +- injection time

  // --------------------------------------------------------
  // define sky distribution
  // --------------------------------------------------------
  vector<mdcpar> par(4);
  par[0].name="entries"; par[0].value=15000  ;     // Number of entries in lst file
//  par[1].name="rho_min"; par[1].value=1;      // min rho // Kpc
//  par[2].name="rho_max"; par[2].value=100000;      // max rho // Kpc
  par[3].name="iota"   ; par[3].value=-1;         // the angle iota (deg) is the inclination of the system
                                                  // which originates the burst with respect to the line of sight
  MDC.SetSkyDistribution(MDC_RANDOM,"x^2",par,seed+gIFACTOR*4000);

 // CWB::mdc MDC(net);

//  MDC.SetInspiral("EBBH",inspOptions);
}
