#define HVETO_VETO
//#define CAT4_VETO
//#define CAT2_VETO
#define SAVE_IMAGES
#define TGRAPH_KEY

#define LIVINGSTON
#define HANFORD
//#define VIRGO

#define NTUPLE_BKG_WAVE "/home/vedovato/O2/SEARCHES/OFFLINE/BBH/LH/BKG/O2_K03_C02c_LH_BBH_BKG_run1/merge/"
#define file_tail "M1.V_hvetoLH.C_bin1_cut.root"

#define RUN_LABEL "O2 - EBBH : Search for stellar mass eccentric binary blackholes : beta 2 MW"

{

  chunkID       = #CWB_CHUNK_NUMBER;
  calibVer      = "#CWB_CALIB_VER";     // C00, C01, C02
  TString channelName   = "#CWB_CHANNEL_NAME";  // C00 -> "GDS-CALIB_STRAIN", C01 -> "DCS-CALIB_STRAIN_C01", C02 -> "DCS-CALIB_STRAIN_C02"

  #include "/home/shubhanshu.tiwari/ebbh_o2_chunk_wise/shubhanshu.tiwari/O2/SEARCHES/OFFLINE/BBH/LH/PP_Cuts.hh"
  #include "/home/shubhanshu.tiwari/ebbh_o2_chunk_wise/shubhanshu.tiwari/O2/CHUNKS/Chunks_Cuts.hh"

  // ----------------------------------------------------------
  // Declare standard cuts & standard pp_label
  // ----------------------------------------------------------

  TString user_pp_label = "";

  // ----------------------------------------------------------
  // thresholds declaration
  // ----------------------------------------------------------

  double T_win 	    = 0.2;//0.2; 
  double T_out      = 6.0;//6.0;
  double T_cor      = 0.;        // cc cut
  double T_cut      = 0;        // rho high frequency cut
  double T_scc      = 0.;
  double T_ifar	    = 1.; 
  double T_low      = -5.;//-2;
  double T_high     = 5;//2;
  double T_mchirp   = 1.;//1.0;
  double T_sphr     = 0.;
  double T_efrac    = 0.;
  double T_freq     = 0.;

  int pp_irho    = 1;
  int pp_inetcc  = 1;

  int colFAD = 39; //color of curve in FAD plot
  // ----------------------------------------------------------
  // VETO cuts
  // dq file list
  // {ifo, dqcat_file, dqcat[0/1/2], shift[sec], inverse[false/true], 4columns[true/false]}
  // ----------------------------------------------------------

  const int nvdqf=2;
  dqfile vdqf[nvdqf] = {
         {"L1" ,"",     CWB_HVETO, 0., false,   false},
         {"H1" ,"",     CWB_HVETO, 0., false,   false}
  };

  sprintf(vdqf[0].file, "/home/shubhanshu.tiwari/ebbh_o2_chunk_wise/shubhanshu.tiwari/O2/DATA/%s/HVETO/L1/HVETO_L1_ANALYSIS_SEGMENTS_MERGED.txt",calibVer.Data());
  sprintf(vdqf[1].file, "/home/shubhanshu.tiwari/ebbh_o2_chunk_wise/shubhanshu.tiwari/O2/DATA/%s/HVETO/H1/HVETO_H1_ANALYSIS_SEGMENTS_MERGED.txt",calibVer.Data());

}
