// -------------------------------------------------------------------------
// WARNING, NOT EDIT : This is a multi plugin generated with cwb_mplugin !!!
// NOTE              : The main is listed on the bottom                      
// 
// INPUT PLUGINS     :                                                       
// 
// 1 - macro/CWB_Plugin_QLveto_Gating.C
// 2 - macro/CWB_Plugin_EBBH.C
// 
// -------------------------------------------------------------------------


// -------------------------------------------------------------------------
// --> BEGIN CWB_USER PLUGIN CODE 1
// 
// macro/CWB_Plugin_QLveto_Gating.C
// -------------------------------------------------------------------------

namespace CWB_UserPluginNamespace_1 {

// -------------------------------------------------------------------------
// WARNING, NOT EDIT : This is a multi plugin generated with cwb_mplugin !!!
// NOTE              : The main is listed on the bottom                      
// 
// INPUT PLUGINS     :                                                       
// 
// 1 - /home/vedovato/Y2/coherent/SVN/watrepo/wat/trunk_wp_merge/trunk_4886_clean/tools/cwb/plugins/CWB_Plugin_QLveto.C
// 2 - /home/vedovato/Y2/coherent/SVN/watrepo/wat/trunk_wp_merge/trunk_4886_clean/tools/cwb/plugins/CWB_Plugin_Gating.C
// 
// -------------------------------------------------------------------------


// -------------------------------------------------------------------------
// --> BEGIN CWB_USER PLUGIN CODE 1
// 
// /home/vedovato/Y2/coherent/SVN/watrepo/wat/trunk_wp_merge/trunk_4886_clean/tools/cwb/plugins/CWB_Plugin_QLveto.C
// -------------------------------------------------------------------------

}
#define XIFO 4

#pragma GCC system_header

#include "cwb.hh"
#include "config.hh"
#include "network.hh"
#include "wavearray.hh"
#include "TString.h"
#include "TObjArray.h"
#include "TObjString.h"
#include "TRandom.h"
#include "TComplex.h"
#include "TMath.h"
#include "mdc.hh"
#include "watplot.hh"
#include "gwavearray.hh"
#include <vector>

namespace CWB_UserPluginNamespace_1 {

namespace CWB_UserPluginNamespace_1 {

//#define PLOT_LIKELIHOOD
//#define PLOT_WHITENED_WAVEFORMS

}
}
#define NTHR 1   		
#define ATHR 7.58859   		

namespace CWB_UserPluginNamespace_1 {

namespace CWB_UserPluginNamespace_1 {

float GetQveto(wavearray<double>* wf);
void  GetLveto(netcluster* pwc, int cid, int nifo, float* Lveto);
void  PlotWaveform(TString ifo, wavearray<double>* wfREC,
                   CWB::config* cfg, bool fft=false, bool strain=false);
void ClearWaveforms(detector* ifo);

std::vector<netpixel> DoPCA(network* NET, CWB::config* cfg, int lag, int id);
void ResetPCA(network* NET, CWB::config* cfg, netcluster* pwc, std::vector<netpixel>* vPIX, int ID);


void 
CWB_Plugin(TFile* jfile, CWB::config* cfg, network* NET, WSeries<double>* x, TString ifo, int type)  {
//!MISCELLANEA
// Extract whitened reconstructed waveforms, and compute the Qveto, Lveto parameters

  cout << endl;
  cout << "-----> CWB_Plugin_QLveto.C" << endl;
  cout << "ifo " << ifo.Data() << endl;
  cout << "type " << type << endl;
  cout << endl;

  float Qveto[2*NIFO_MAX];				// Qveto
  float Lveto[3];                               	// Lveto

  if(type==CWB_PLUGIN_CONFIG) {  
    cfg->outPlugin=true;  				// disable built-in output root file
  }

  if(type==CWB_PLUGIN_ILIKELIHOOD) {
    NET->wfsave=true;                                   // enable waveform save

    // search output root file in the system list
    TFile* froot = NULL;                         
    TList *files = (TList*)gROOT->GetListOfFiles();
    TString outDump="";
    netevent* EVT;
    int nIFO = NET->ifoListSize();			// number of detectors
    if (files) {                                   
      TIter next(files);                           
      TSystemFile *file;                           
      TString fname;                               
      bool check=false;                            
      while ((file=(TSystemFile*)next())) {        
         fname = file->GetName();                  
         // set output root file as the current file
         if(fname.Contains("wave_")) {
           froot=(TFile*)file;froot->cd();
           outDump=fname;
           outDump.ReplaceAll(".root.tmp",".txt");
           //cout << "output file name : " << fname << endl;
         }
      }                                                               
      if(!froot) {                                                    
        cout << "CWB_Plugin_QLveto.C : Error - output root file not found" << endl;
        gSystem->Exit(1);                                                                             
      }                                                                                      
    } else {                                                                                 
      cout << "CWB_Plugin_QLveto.C : Error - output root file not found" << endl;  
      gSystem->Exit(1);                                                                               
    }                                                                                        

    TTree* net_tree = (TTree *) froot->Get("waveburst");
    if(net_tree==NULL) {
      EVT = new netevent(nIFO);
      net_tree = EVT->setTree();
      net_tree->Branch("Qveto",Qveto,TString::Format("Qveto[%i]/F",2*cfg->nIFO));
      net_tree->Branch("Lveto",Lveto,TString::Format("Lveto[%i]/F",3));
    }
  }

  if(type==CWB_PLUGIN_OLIKELIHOOD) {

    if(TString(cfg->analysis)!="2G") {
      cout << "CWB_Plugin_QLveto.C -> "
           << "CWB_PLUGIN_OLIKELIHOOD implemented only for 2G" << endl;
      gSystem->Exit(1);
    }

    // import ifactor
    int gIFACTOR=-1; IMPORT(int,gIFACTOR)
    cout << "-----> CWB_Plugin_QLveto.C -> " 
         << " gIFACTOR : " << gIFACTOR << endl;

    // import slagShift
    float* gSLAGSHIFT=NULL; IMPORT(float*,gSLAGSHIFT)

    int nIFO = NET->ifoListSize();			// number of detectors
    int K = NET->nLag;  				// number of time lag          
    netevent* EVT;
    wavearray<double> id;
    //double factor = cfg->simulation==3||cfg->simulation==4 ? -gIFACTOR : cfg->factors[gIFACTOR];                 
    double factor = cfg->factors[gIFACTOR];                 
    int rate = 0;					// select all resolutions

    // search output root file in the system list
    TFile* froot = NULL;                         
    TList *files = (TList*)gROOT->GetListOfFiles();
    TString outDump="";
    if (files) {                                   
      TIter next(files);                           
      TSystemFile *file;                           
      TString fname;                               
      bool check=false;                            
      while ((file=(TSystemFile*)next())) {        
         fname = file->GetName();                  
         // set output root file as the current file
         if(fname.Contains("wave_")) {
           froot=(TFile*)file;froot->cd();
           outDump=fname;
           outDump.ReplaceAll(".root.tmp",".txt");
           //cout << "output file name : " << fname << endl;
         }
      }                                                               
      if(!froot) {                                                    
        cout << "CWB_Plugin_QLveto.C : Error - output root file not found" << endl;
        gSystem->Exit(1);                                                                             
      }                                                                                      
    } else {                                                                                 
      cout << "CWB_Plugin_QLveto.C : Error - output root file not found" << endl;  
      gSystem->Exit(1);                                                                               
    }                                                                                        

    TTree* net_tree = (TTree *) froot->Get("waveburst");
    if(net_tree!=NULL) {
      EVT = new netevent(net_tree,nIFO);
      net_tree->SetBranchAddress("Qveto",Qveto);
      net_tree->SetBranchAddress("Lveto",Lveto);
    } else {
      EVT = new netevent(nIFO);
      net_tree = EVT->setTree();
      net_tree->Branch("Qveto",Qveto,TString::Format("Qveto[%i]/F",2*cfg->nIFO));
      net_tree->Branch("Lveto",Lveto,TString::Format("Lveto[%i]/F",3));
    }
    EVT->setSLags(gSLAGSHIFT);        			// set slags into netevent

    for(int k=0; k<K; k++) {  				// loop over the lags

      id = NET->getwc(k)->get(const_cast<char*>("ID"), 0, 'L', rate);

      for(int j=0; j<(int)id.size(); j++) {  		// loop over cluster index

        int ID = size_t(id.data[j]+0.5);

        if(NET->getwc(k)->sCuts[ID-1]!=-1) continue;    // skip rejected/processed clusters

        double ofactor=0;
        if(cfg->simulation==4)      ofactor=-factor;
        else if(cfg->simulation==3) ofactor=-gIFACTOR;
        else                        ofactor=factor;

        EVT->output2G(NULL,NET,ID,k,ofactor);		// get reconstructed parameters

        wavearray<double>** pwfREC = new wavearray<double>*[nIFO];
        detector* pd = NET->getifo(0);
        int idSize = pd->RWFID.size();

        int wfIndex=-1;
        for (int mm=0; mm<idSize; mm++) if (pd->RWFID[mm]==ID) wfIndex=mm;
        if(wfIndex==-1) continue;

        netcluster* pwc = NET->getwc(k);
        cout << endl << "----------------------------------------------------------------" << endl;

        // extract whitened reconstructed waveforms
        for(int n=0; n<nIFO; n++) {

           pd = NET->getifo(n);

           pwfREC[n] = pd->RWFP[wfIndex];
           wavearray<double>* wfREC = pwfREC[n];	// array of reconstructed waveforms

#ifdef PLOT_WHITENED_WAVEFORMS
           //PlotWaveform(NET->ifoName[n], wfREC, cfg, false, false);
           PlotWaveform(NET->ifoName[n], wfREC, cfg, true, false);
#endif
	   // reconstructed whitened waveform
           NET->getMRAwave(ID,k,'S',0,true);
           Qveto[n] = GetQveto(&(pd->waveForm));
	   // whitened waveform
           NET->getMRAwave(ID,k,'W',0,true);
           Qveto[n+nIFO] = GetQveto(&(pd->waveBand));

           //Qveto[n] = GetQveto(wfREC);
           cout << "Qveto : " << pd->Name << " Qveto[R] = " << Qveto[n] 
                                          << " Qveto[W] = " << Qveto[n+nIFO] << endl;

           if(!cfg->simulation) ClearWaveforms(pd);	// release waveform memory
        }
        delete [] pwfREC;

        std::vector<netpixel> vPIX;
        if(cfg->pattern>0) vPIX = DoPCA(NET, cfg, k, ID);            // do PCA analysis
        GetLveto(pwc, ID, nIFO, Lveto);
        if(cfg->pattern>0) ResetPCA(NET, cfg, pwc, &vPIX, ID);	     // restore WP pwc

	cout << endl;
        cout << "Lveto : " << "fmean : " << Lveto[0] << " frms : " << Lveto[1] 
             << " Energy Ratio : " << Lveto[2] << endl << endl;
        cout << "----------------------------------------------------------------" << endl;

        std::vector<int> sCuts = NET->getwc(k)->sCuts;  // save cCuts
        // set sCuts=1 to the events which must be not copied with cps to pwc
        for(int i=0; i<(int)sCuts.size(); i++) if(i!=ID-1) NET->getwc(k)->sCuts[i]=1;

        // ID can not be used to get the event, to get event use ID=1 (ex: for watplot)
        NET->getwc(k)->sCuts = sCuts;                   // restore cCuts

        if(cfg->dump) EVT->dopen(outDump.Data(),const_cast<char*>("a"),false);
        EVT->output2G(net_tree,NET,ID,k,ofactor);       // get reconstructed parameters
        if(cfg->dump) {                                 
          // add Qveto to dump file
          fprintf(EVT->fP,"Qveto:      ");
          for(int i=0; i<2*nIFO; i++) fprintf(EVT->fP,"%f ",Qveto[i]);
          fprintf(EVT->fP,"\n");
          // add Lveto to dump file
          fprintf(EVT->fP,"Lveto:      ");
          for(int i=0; i<3; i++) fprintf(EVT->fP,"%f ",Lveto[i]);
          fprintf(EVT->fP,"\n");
        }
        if(cfg->dump) EVT->dclose();
        if(!cfg->cedDump) NET->getwc(k)->sCuts[ID-1]=1; // mark as processed
      }
    }

    jfile->cd();
    if(EVT) delete EVT;
  }
  return;
}

float
GetQveto(wavearray<double>* wf) { 

  wavearray<double> x = *wf;; 

  // resample data by a factor 4
  int xsize=x.size();
  x.FFTW(1);
  x.resize(4*x.size());
  x.rate(4*x.rate());
  for(int j=xsize;j<x.size();j++) x[j]=0;
  x.FFTW(-1);

  // extract max/min values and save the absolute values in the array 'a'
  wavearray<double> a(x.size());
  int size=0;
  double dt = 1./x.rate();
  double prev=x[0];
  double xmax=0;
  for (int i=1;i<x.size();i++) {
    if(fabs(x[i])>xmax) xmax=fabs(x[i]);
    if(prev*x[i]<0) {
      a[size]=xmax;
      size++;
      xmax=0;
    }
    prev=x[i];
  }

  // find max value/index ans save on  amax/imax  
  int imax=-1;
  double amax=0;
  for (int i=1;i<size;i++) {
    if(a[i]>amax) {amax=a[i];imax=i;}
  }

/*
  cout << endl;
  cout << "a[imax-2] " << a[imax-2] << endl;
  cout << "a[imax-1] " << a[imax-1] << endl;
  cout << "a[imax]   " << a[imax] << endl;
  cout << "a[imax+1] " << a[imax+1] << endl;
  cout << "a[imax+2] " << a[imax+2] << endl;
  cout << endl;
*/

  // compute Qveto 
  double ein=0;	// energy of max values inside NTHR
  double eout=0;	// energy of max values outside NTHR
  for (int i=0;i<size;i++) {
    if(abs(imax-i)<=NTHR) {
      ein+=a[i]*a[i];
      //cout << i << " ein " << a[i] << " " << amax << endl;
    } else {
      if(a[i]>amax/ATHR) eout+=a[i]*a[i];
      //if(a[i]>amax/ATHR) cout << i << " eout " << a[i] << " " << amax << endl;
    }
  }
  float Qveto = ein>0 ? eout/ein : 0.;
  //cout << "Qveto : " << Qveto << " ein : " << ein << " eout : " << eout << endl;

  return Qveto;
}

void 
GetLveto(netcluster* pwc, int cid, int nifo, float* Lveto) {
//          
// input                                                                                      
//        pwc    : pointer to netcluster object                                                          
//        cid    : cluster id                                                                            
//        nifo   : number of detectors      
// output                                                             
//     Lveto[0]  : line frequency
//     Lveto[1]  : line bandwitdh
//     Lveto[2]  : line enery ratio (line_energy / total_energy)
//   
                                                                                             
  Lveto[0] = Lveto[1] = Lveto[2] = 0;

  std::vector<int>* vint = &(pwc->cList[cid-1]);        // pixel list
  int V = vint->size();                                 // cluster size
  if(!V) return;                                                       

  // ------------------------------------------------------------------
  // Find max pixel parameters                                          
  // ------------------------------------------------------------------

  double likeMax=0;	// maximum pixel's energy 
  double likeTot=0;	// total cluster energy
  double freqMax;	// frequency of the pixel with max energy
  double dfreqMax;	// df of the pixel with max energy
  for(int n=0; n<V; n++) {
    netpixel* pix = pwc->getPixel(cid,n);
    if(pix->layers%2==0) {        
      cout << "CWB_Plugin_QLveto.C - Error : is enabled only for WDM (2G)" << endl;
      exit(1);                                                                                     
    }                                                                                              
    if(!pix->core) continue;                  // select only the principal components pixels

    double likePix=0;                                                                       
    for(int m=0; m<nifo; m++) {                                                          
      likePix += pow(pix->getdata('S',m),2);  // like whitened reconstructed signal 00
      likePix += pow(pix->getdata('P',m),2);  // like whitened reconstructed signal 90
    }                                                                                     

    double freq = pix->frequency*pix->rate/2.; 
    double df = pix->rate/2.;

    likeTot+=likePix;
    if(likePix>likeMax) {likeMax=likePix;freqMax=freq;dfreqMax=df;}
  }
  //cout << "likeMax : " << likeMax << " likeTot : " << likeTot 
  //     << " freqMax : " << freqMax << " dfreqMax : " << dfreqMax << endl;

  // ------------------------------------------------------------------
  // Compute Lveto parameters                                          
  // ------------------------------------------------------------------

  double fmean=0;	// line mean frequency
  double frms=0;	// line bandwidth	
  double likeLin=0;	// line energy
  for(int n=0; n<V; n++) {
    netpixel* pix = pwc->getPixel(cid,n);
    if(!pix->core) continue;                  // select only the principal components pixels

    double likePix=0;                                                                       
    for(int m=0; m<nifo; m++) {                                                          
      likePix += pow(pix->getdata('S',m),2);  // like whitened reconstructed signal 00
      likePix += pow(pix->getdata('P',m),2);  // like whitened reconstructed signal 90
    }                                                                                     

    // the estimation is done for all pixels 
    // with freq in the range [freqMax-dfreqMax, freqMax+dfreqMax]
    double freq = pix->frequency*pix->rate/2.; 
    if(fabs(freq-freqMax)<=dfreqMax) {
      likeLin += likePix;
      fmean   += likePix*freq;
      frms    += likePix*freq*freq;
    }
  }                                                                   
   
  fmean = fmean/likeLin;
  frms  = frms/likeLin-fmean*fmean;
  frms  = frms>0 ? sqrt(frms) : 0.;

  if(frms<dfreqMax/2.) frms=dfreqMax/2.;

  // ------------------------------------------------------------------
  // Save Lveto parameters                                          
  // ------------------------------------------------------------------

  Lveto[0] = fmean;     			// line mean frequency     
  Lveto[1] = frms;        			// line bandwidth   
  Lveto[2] = likeTot>0. ? likeLin/likeTot : 0.;	// energy ratio energy inside_line/total

  // ------------------------------------------------------------------
  // plot time-frequency energy                                             
  // ------------------------------------------------------------------

#if defined PLOT_LIKELIHOOD 
  watplot WTS(const_cast<char*>("wts"));
  WTS.plot(pwc, cid, nifo, 'L', 0, const_cast<char*>("COLZ"));
  WTS.canvas->Print("l_tfmap_scalogram.png");
#endif

  return;
}

void 
PlotWaveform(TString ifo, wavearray<double>* wfREC, 
             CWB::config* cfg, bool fft, bool strain) {

  watplot PTS(const_cast<char*>("ptswrc"),200,20,800,500);

  //cout << "Print " << fname << endl;
  double tmin = wfREC->start();
  wfREC->start(wfREC->start()-tmin);
  if(fft) {
    PTS.plot(wfREC, const_cast<char*>("ALP"), 1, 0, 0, true, cfg->fLow, cfg->fHigh);
  } else {
    PTS.plot(wfREC, const_cast<char*>("ALP"), 1, 0, 0);
  }
  PTS.graph[0]->SetLineWidth(1);
  wfREC->start(wfREC->start()+tmin);

  char label[64]="";
  if(fft) sprintf(label,"%s","fft");
  else    sprintf(label,"%s","time");
  if(strain) sprintf(label,"%s_%s",label,"strain");
  else       sprintf(label,"%s_%s",label,"white"); 

  char fname[1024];
  //sprintf(fname, "%s_wf_%s_rec_gps_%d.root",ifo.Data(),label,int(tmin));
  sprintf(fname, "%s_wf_%s_rec_gps_%d.png",ifo.Data(),label,int(tmin));
  PTS.canvas->Print(fname); 
  cout << "write : " << fname << endl;
  //PTS.canvas->Write(REPLACE(fname,dirCED,gtype));
}

void 
ClearWaveforms(detector* ifo) {

  int n;

  n = ifo->IWFP.size();
  for (int i=0;i<n;i++) {
    wavearray<double>* wf = (wavearray<double>*)ifo->IWFP[i];
    delete wf;
  }
  ifo->IWFP.clear();
  ifo->IWFID.clear();

  n = ifo->RWFP.size();
  for (int i=0;i<n;i++) {
    wavearray<double>* wf = (wavearray<double>*)ifo->RWFP[i];
    delete wf;
  }
  ifo->RWFP.clear();
  ifo->RWFID.clear();
}

std::vector<netpixel> DoPCA(network* NET, CWB::config* cfg, int lag, int id) {

  double ee;

  size_t nIFO = NET->ifoList.size();

  float  En = 2*NET->acor*NET->acor*nIFO;     // network energy threshold in the sky loop

  int size = NET->a_00.size();
  int f_ = NIFO/4;

  netpixel* pix;
  netcluster* pwc = NET->getwc(lag);
  std::vector<netpixel> vPIX;

  std::vector<int> pI = NET->wdmMRA.getXTalk(pwc, id, false);   // buffer for pixel IDs
  int V = pI.size();
  if(V>cfg->BATCH) return vPIX;                                 // attach TD amp to pixels < V

  wavearray<float>  xi(size); xi=0;           // PC 00 array
  wavearray<float>  XI(size); XI=0;           // PC 90 array

  __m128* _xi  = (__m128*) xi.data;           // set pointer to PC 00 array
  __m128* _XI  = (__m128*) XI.data;           // set pointer to PC 90 array

  __m128* _aa  = (__m128*) NET->a_00.data;    // set pointer to 00 array
  __m128* _AA  = (__m128*) NET->a_90.data;    // set pointer to 90 array

  int nPC = 0;
  for(int j=0; j<V; j++) {
    int jf = j*f_;                            // source sse pointer increment
    _sse_zero_ps(_xi+jf);                     // zero MRA amplitudes
    _sse_zero_ps(_XI+jf);                     // zero MRA amplitudes
    ee = _sse_abs_ps(_aa+jf,_AA+jf);          // total pixel energy / quadrature
    if(ee>En) nPC++; else ee=0.;              // count core pixels
    NET->rNRG.data[j] = ee;                   // init residual energy array
    NET->pNRG.data[j] = NET->rNRG.data[j];    // init residual energy array
  }

  nPC = NET->_sse_mra_ps(xi.data,XI.data,En,nPC);  // get principal components

  for(int j=0; j<V; j++) {                    // loop over principal components
     pix = pwc->getPixel(id,pI[j]);
     vPIX.push_back(*pix);		      // save original pixels
     pix->core = false;
     ee = NET->pNRG.data[j];                  // total pixel energy
     if(ee<En) continue;
     pix->core = true;
     for(int i=0; i<nIFO; i++) {
        pix->setdata(double(xi.data[j*NIFO+i]),'S',i);    // store 00 whitened response PC
        pix->setdata(double(XI.data[j*NIFO+i]),'P',i);    // store 90 whitened response PC
     }
  }

  return vPIX;
}

void ResetPCA(network* NET, CWB::config* cfg, netcluster* pwc, std::vector<netpixel>* vPIX, int ID) {

  std::vector<int> pI = NET->wdmMRA.getXTalk(pwc, ID, false);   // buffer for pixel IDs
  int V = pI.size();
  if(V>cfg->BATCH) return;                                 	// attach TD amp to pixels < V
  for(int j=0; j<V; j++) {
    netpixel* pix = pwc->getPixel(ID,pI[j]);
    *pix = (*vPIX)[j];
  }

  while(!vPIX->empty()) vPIX->pop_back();
  vPIX->clear(); std::vector<netpixel>().swap(*vPIX);
}
}

// -------------------------------------------------------------------------
// --> END CWB_USER PLUGIN CODE 1
// 
// /home/vedovato/Y2/coherent/SVN/watrepo/wat/trunk_wp_merge/trunk_4886_clean/tools/cwb/plugins/CWB_Plugin_QLveto.C
// -------------------------------------------------------------------------


// -------------------------------------------------------------------------
// --> BEGIN CWB_USER PLUGIN CODE 2
// 
// /home/vedovato/Y2/coherent/SVN/watrepo/wat/trunk_wp_merge/trunk_4886_clean/tools/cwb/plugins/CWB_Plugin_Gating.C
// -------------------------------------------------------------------------

}
#define XIFO 4

#pragma GCC system_header

#include "cwb.hh"
#include "cwb2G.hh"
#include "config.hh"
#include "network.hh"
#include "wavearray.hh"
#include "TString.h"
#include "TObjArray.h"
#include "TObjString.h"
#include "TRandom.h"
#include "Toolbox.hh"

namespace CWB_UserPluginNamespace_1 {

namespace CWB_UserPluginNamespace_2 {

//!DATA_CONDITIONING

// This plugin implements the time gating in the pixel's selection stage.
// Gating is a veto of the pixels belonging to a time interval.
// This plugin is used to exclude from the analysis the pixels 
// in a time interval where there is a huge glitch.
// Huge glitches are harmful because they affect the correct estimation 
// of the selection threshold and could mask the nearby events at lower SNR. 
// Warning : events with high SNR can be rejected by this procedure (see SETHR)

}
}
#define SETHR	1000000	// Is the threshold which define the time slices to be cutted
namespace CWB_UserPluginNamespace_1 {

namespace CWB_UserPluginNamespace_2 {

                        // Warning : this value depends on the frequency interval [fHigh:fLow] 

}
}
#define TWIN    0.5     // Is the window (sec) used to integrate the energies in time
namespace CWB_UserPluginNamespace_1 {

namespace CWB_UserPluginNamespace_2 {

                        // TWIN must be a multiple of the greatest time resolution used in the analysis 

}
}
#define TEDG	1.5	// Is the time window (sec) to be cutted when the time integrated energy is > SETHR

namespace CWB_UserPluginNamespace_1 {

namespace CWB_UserPluginNamespace_2 {

void 
CWB_Plugin(TFile* jfile, CWB::config* cfg, network* net, WSeries<double>* x, TString ifo, int type)  {

// this plugin is called in cwb2G.cc after the production of the TF maps with max over the sky energy (TFmax)
// The procedure is applied for each detector and for each resolution level using the TFmax.

  cout << endl;
  cout << "-----> CWB_Plugin_Gating.C" << endl;
  cout << "ifo " << ifo.Data() << endl;
  cout << "type " << type << endl;
  cout << endl;

  if(type==CWB_PLUGIN_ECOHERENCE) {

    // WSeries contains energy (it is stored in the 0 phase amplitudes)

    // import resolution level
    size_t gILEVEL=-1; IMPORT(size_t,gILEVEL)
    char slevel[256];sprintf(slevel,",%d,",gILEVEL); 

    int nIFO = net->ifoListSize();
    detector* pD[NIFO_MAX];             // pointers to detectors
    for(int n=0;n<nIFO;n++) pD[n] = net->getifo(n);
    WSeries<double>* WS[NIFO_MAX];
    for(int n=0; n<nIFO; n++) WS[n] = pD[n]->getTFmap();

    int layers = WS[0]->maxLayer()+1;  	// numbers of frequency bins (first & last bins have df/2)
    int slices = WS[0]->sizeZero();    	// number of time bins

    double df = WS[0]->resolution();    // frequency bin resolution (hz)
    double dt = 1./(2*df);              // time bin resolution (sec)

    int rate = int(1./dt);                                                                           

    cout << "layers : " << layers << "\t slices : " << slices << "\t rate : " << rate
         << "\t dt : " << dt << "\t df : " << df << endl;                            

    double rTWIN = fabs(TWIN/dt-TMath::Nint(TWIN/dt));
    if(TWIN<dt || rTWIN>1e-7) {
      cout << "-----> CWB_Plugin_Gating.C : Error-> " << " TWIN=" << TWIN 
           << " is not a multiple of dt=" << dt << endl;
      gSystem->Exit(1);
    }      

    double rTEDG = fabs(TEDG/dt-TMath::Nint(TEDG/dt));
    if(TEDG<dt || rTEDG>1e-7) {
      cout << "-----> CWB_Plugin_Gating.C : Error-> " << " TEDG=" << TEDG 
           << " is not a multiple of dt=" << dt << endl;
      gSystem->Exit(1);
    }      

    // For each time slice (time index) we compute the sum of the pixel 
    // energy over all frequency layers (freq index)
    // The sum is stored in the array 'se[NIFO_MAX]' 
    // se = projection of TF map energy on the time axis
    wavearray<double> se[NIFO_MAX];	
    for(int n=0; n<nIFO; n++) {
      se[n].resize(slices); se[n]=0;
      for(int i=0;i<slices;i++) {
        for(int j=0;j<layers;j++) se[n][i] += WS[n]->getSample(i,j);
      }                                                                                                     
    }              

    // A new array 'SE[NIFO_MAX]' is obtained from the arrays 'se[NIFO_MAX]'
    // It contains the time integrated energies over the time window TWIN
    // NOTE : this procedure makes the energies independents from the resolution level
    int N = int(TWIN/dt);		// number of time samples in TWIN
    if(N<1) N=1;
    wavearray<double> SE[NIFO_MAX];
    for(int n=0; n<nIFO; n++) {
      SE[n].resize(slices); SE[n]=0;
      for(int i=N-1;i<slices;i++) for(int j=0;j<N;j++) SE[n][i]+=se[n][i-j];
    }                                                                                               

    // find the time slices to be excluded :
    // 1) from the computation of the pixel's selection threshold
    // 2) from the pixel's selection
    // A time slice is marked as to be cutted when the energy SE>SETHR
    // When a time slice is cutted also the nearby M time slices are cutted [slice-M:slice+M]
    // where M is the number of samples contained in TEDG sec 
    int X = int(cfg->segEdge/dt+0.5); 	// samples in segEdge : this the is scratch time
    int M = int(TEDG/dt)+1; 		// samples in TEDG sec
    wavearray<int> sCut[NIFO_MAX];	// array which contains the slices to be cut
    for(int n=0; n<nIFO; n++) {
      sCut[n].resize(slices); sCut[n]=0;
      for(int i=0;i<slices;i++) {
        if(SE[n][i]>SETHR) { 		
          int is = i-M>X ? i-M : X; 
          int es = i+M<(slices-X) ? i+M : slices-X; 
          for(int ii=is;ii<es;ii++) sCut[n][ii]=1;
        }
        //cout << i << " " << SE[n][i] << endl;  
      }
    }

    // All pixels in the layers which time slice is marked as cutted are filled with a negative energy = -1e20
    // -> these pixels do not partecipate to the computation of the pixel's selection thereshold
    //    and moreover these pixels are excluded from the selection
    double gating_time=0.;	// total time vetoed by gating
    for(int n=0; n<nIFO; n++) {
      for(int i=0;i<slices;i++) {	// set to -1e20 the energy
        if(sCut[n][i]) {
          gating_time+=dt;
          for(int j=0;j<layers;j++) WS[n]->putSample(-1e20,i,j);
        }
      }
    } 

    // add infos to history
    char info[256];
    sprintf(info,"-RES:%d-GT:%g",cfg->l_high-gILEVEL,gating_time);
    cwb2G* gCWB2G; IMPORT(cwb2G*,gCWB2G)
    gCWB2G->PrintAnalysisInfo(CWB_STAGE_COHERENCE,"cwb2G::Coherence",info,false);
  }

  return;
}
}

// -------------------------------------------------------------------------
// --> END CWB_USER PLUGIN CODE 2
// 
// /home/vedovato/Y2/coherent/SVN/watrepo/wat/trunk_wp_merge/trunk_4886_clean/tools/cwb/plugins/CWB_Plugin_Gating.C
// -------------------------------------------------------------------------


// -------------------------------------------------------------------------
// --> MAIN CWB_USER PLUGIN CODE 
// 
// INPUT PLUGINS     :                                                       
// 
// 1 - /home/vedovato/Y2/coherent/SVN/watrepo/wat/trunk_wp_merge/trunk_4886_clean/tools/cwb/plugins/CWB_Plugin_QLveto.C
// 2 - /home/vedovato/Y2/coherent/SVN/watrepo/wat/trunk_wp_merge/trunk_4886_clean/tools/cwb/plugins/CWB_Plugin_Gating.C
// 
// -------------------------------------------------------------------------

}
#define XIFO 4
#pragma GCC system_header
#include "cwb.hh"


namespace CWB_UserPluginNamespace_1 {

void
CWB_Plugin(TFile* jfile, CWB::config* cfg, network* net, WSeries<double>* x, TString ifo, int type)  {

  CWB_UserPluginNamespace_1::CWB_Plugin(jfile, cfg, net, x, ifo, type); // CALL USER PLUGIN CODE 1
  CWB_UserPluginNamespace_2::CWB_Plugin(jfile, cfg, net, x, ifo, type); // CALL USER PLUGIN CODE 2
}
}

// -------------------------------------------------------------------------
// --> END CWB_USER PLUGIN CODE 1
// 
// macro/CWB_Plugin_QLveto_Gating.C
// -------------------------------------------------------------------------


// -------------------------------------------------------------------------
// --> BEGIN CWB_USER PLUGIN CODE 2
// 
// macro/CWB_Plugin_EBBH.C
// -------------------------------------------------------------------------

#define XIFO 4

#pragma GCC system_header

#include "cwb.hh"
#include "config.hh"
#include "network.hh"
#include "wavearray.hh"
#include "TString.h"
#include "TObjArray.h"
#include "TObjString.h"
#include "TRandom.h"
#include "TComplex.h"
#include "TMath.h"
#include "mdc.hh"
#include <vector>

namespace CWB_UserPluginNamespace_2 {

void 
CWB_Plugin(TFile* jfile, CWB::config* cfg, network* net, WSeries<double>* x, TString ifo, int type)  {

// Plugin to generate simulated gaussian noise and injected 'on the fly' EBBH

  cout << endl;
  cout << "-----> plugins/CWB_Plugin_EBBH.C" << endl;
  cout << "ifo " << ifo.Data() << endl;
  cout << "type " << type << endl;
  cout << endl;

  if(type==CWB_PLUGIN_CONFIG) {  
//    cfg->dataPlugin=true; // disable read data from frames
    cfg->mdcPlugin=true;  // disable read mdc from frames
  }

  if(type==CWB_PLUGIN_MDC) {  

    char cmd[128];
    sprintf(cmd,"network* net = (network*)%p;",net);
    gROOT->ProcessLine(cmd);
    sprintf(cmd,"CWB::config* cfg = (CWB::config*)%p;",cfg);
    gROOT->ProcessLine(cmd);

    CWB::mdc MDC(net);

    // ---------------------------------
    // read plugin config 
    // ---------------------------------

    cfg->configPlugin.Exec();

    // ---------------------------------
    // set list of mdc waveforms
    // ---------------------------------
   
    IMPORT(CWB::mdc,MDC) 
    MDC.Print();

    // ---------------------------------
    // get mdc data
    // ---------------------------------

    MDC.Get(*x,ifo);

    // ---------------------------------
    // set mdc list in the network class 
    // ---------------------------------

    //if(type==CWB_PLUGIN_INIT_JOB) 
      {  
      if(ifo.CompareTo(net->ifoName[0])==0) {
        net->mdcList.clear();
        net->mdcType.clear();
        net->mdcTime.clear();
        net->mdcList=MDC.mdcList;
        net->mdcType=MDC.mdcType;
        net->mdcTime=MDC.mdcTime;

        double Tb = x->start();
        double dT = x->size()/x->rate();
        char log_label[512];
        char tmpFile[1024];
        sprintf(log_label,"%d_%d_%s_job%d",int(Tb),int(dT),cfg->data_label,net->nRun);
        sprintf(tmpFile,"%s/%s-LogTMP.txt",cfg->log_dir,log_label);
        cout << "Write MDC Log : " << tmpFile << endl;
        MDC.DumpLog(tmpFile);
  
      }
    } 
  

    cout.precision(14);
    for(int k=0;k<(int)net->mdcList.size();k++) cout << k << " mdcList " << MDC.mdcList[k] << endl;
    for(int k=0;k<(int)net->mdcTime.size();k++) cout << k << " mdcTime " << MDC.mdcTime[k] << endl;
    for(int k=0;k<(int)net->mdcType.size();k++) cout << k << " mdcType " << MDC.mdcType[k] << endl;
  }
/*
  if(type==CWB_PLUGIN_CLOSE_JOB) {  
    if(ifo.CompareTo(net->ifoName[0])==0) {
      double Tb = x->start();
      double dT = x->size()/x->rate();
      char log_label[512];
      char tmpFile[1024];
      char outFile[1024];
      sprintf(log_label,"%d_%d_%s_job%d",int(Tb),int(dT),cfg->data_label,net->nRun);
      sprintf(tmpFile,"%s/%s-LogTMP.txt",cfg->log_dir,log_label);
      sprintf(outFile,"%s/%s-Log.txt",cfg->log_dir,log_label);
      char command[1024];
      sprintf(command,"/bin/mv %s %s", tmpFile, outFile);
      gSystem->Exec(command);
    }
  }
*/
  return;
}
}

// -------------------------------------------------------------------------
// --> END CWB_USER PLUGIN CODE 2
// 
// macro/CWB_Plugin_EBBH.C
// -------------------------------------------------------------------------


// -------------------------------------------------------------------------
// --> MAIN CWB_USER PLUGIN CODE 
// 
// INPUT PLUGINS     :                                                       
// 
// 1 - macro/CWB_Plugin_QLveto_Gating.C
// 2 - macro/CWB_Plugin_EBBH.C
// 
// -------------------------------------------------------------------------

#define XIFO 4
#pragma GCC system_header
#include "cwb.hh"


void
CWB_Plugin(TFile* jfile, CWB::config* cfg, network* net, WSeries<double>* x, TString ifo, int type)  {

  CWB_UserPluginNamespace_1::CWB_Plugin(jfile, cfg, net, x, ifo, type); // CALL USER PLUGIN CODE 1
  CWB_UserPluginNamespace_2::CWB_Plugin(jfile, cfg, net, x, ifo, type); // CALL USER PLUGIN CODE 2
}
