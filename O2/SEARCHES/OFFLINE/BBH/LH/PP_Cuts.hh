// ----------------------------------------------------------------------------
// Definitions of the search bins used for the Offline Stallar Mass BBH Search
// ----------------------------------------------------------------------------

// definition of the selection selection cuts 
TCut dqveto("dqveto_hvetoLH","!veto_hveto_H1 && !veto_hveto_L1");
TCut norm_cut("norm_cut","norm>2.5");
TCut freq_cut("freq_cut","frequency[0]>48 && frequency[0]<992");
TCut netcc_cut("netcc_cut","netcc[0]>0.7 && netcc[2]>0.7");
TCut qveto_cut("qveto_cut","Qveto[0]>0.3");
TCut lveto_cut("lveto_cut","!(bandwidth[0]<5 || (Lveto[1]<5 && Lveto[2]>0.8))");
TCut chi2_cut("chi2_cut","log10(penalty)<0.2");
TCut chirp_cut("chirp_cut","chirp[1]>1");

// definition of the exclusive bins
TCut bin1_cut = TCut("bin1_cut",(dqveto+norm_cut+freq_cut+netcc_cut+qveto_cut+lveto_cut+chi2_cut+chirp_cut).GetTitle());

//cout << "bin1_cut : " << bin1_cut.GetTitle() << endl;
