#!/bin/tcsh -f

onintr irq_ctrlc

unsetenv CWB_MERGE_LABEL

if ((( $1 == '' ))) then
  echo ""
  echo "CWB_MERGE_LABEL not defined"
  echo ""
  exit 1
else
  setenv CWB_MERGE_LABEL $1
endif


${CWB_SCRIPTS}/cwb_setcuts.csh $CWB_MERGE_LABEL.V_hvetoLH.S_bin1_cut ' --wcuts factor==4  --label 100_20_0_0_0_0_0_0        --mcuts factor==4'
${CWB_SCRIPTS}/cwb_setcuts.csh $CWB_MERGE_LABEL.V_hvetoLH.S_bin1_cut ' --wcuts factor==5  --label 100_50_0_0_0_0_0_0        --mcuts factor==5'
${CWB_SCRIPTS}/cwb_setcuts.csh $CWB_MERGE_LABEL.V_hvetoLH.S_bin1_cut ' --wcuts factor==1  --label 100_100_0_0_0d8_0_0_0d8   --mcuts factor==1'
${CWB_SCRIPTS}/cwb_setcuts.csh $CWB_MERGE_LABEL.V_hvetoLH.S_bin1_cut ' --wcuts factor==3  --label 100_100_0_0_0_0_0_0       --mcuts factor==3'
${CWB_SCRIPTS}/cwb_setcuts.csh $CWB_MERGE_LABEL.V_hvetoLH.S_bin1_cut ' --wcuts factor==2  --label 100_100_0_0_m0d8_0_0_m0d8 --mcuts factor==2'

${CWB_SCRIPTS}/cwb_setcuts.csh $CWB_MERGE_LABEL.V_hvetoLH.S_bin1_cut ' --wcuts factor==8  --label 200_50_0_0_0_0_0_0        --mcuts factor==8'
${CWB_SCRIPTS}/cwb_setcuts.csh $CWB_MERGE_LABEL.V_hvetoLH.S_bin1_cut ' --wcuts factor==6  --label 200_100_0_0_0_0_0_0       --mcuts factor==6'
${CWB_SCRIPTS}/cwb_setcuts.csh $CWB_MERGE_LABEL.V_hvetoLH.S_bin1_cut ' --wcuts factor==7  --label 200_200_0_0_0_0_0_0       --mcuts factor==7'
${CWB_SCRIPTS}/cwb_setcuts.csh $CWB_MERGE_LABEL.V_hvetoLH.S_bin1_cut ' --wcuts factor==12 --label 300_50_0_0_0_0_0_0        --mcuts factor==12'
${CWB_SCRIPTS}/cwb_setcuts.csh $CWB_MERGE_LABEL.V_hvetoLH.S_bin1_cut ' --wcuts factor==9  --label 300_100_0_0_0_0_0_0       --mcuts factor==9'
${CWB_SCRIPTS}/cwb_setcuts.csh $CWB_MERGE_LABEL.V_hvetoLH.S_bin1_cut ' --wcuts factor==10 --label 300_200_0_0_0_0_0_0       --mcuts factor==10'
${CWB_SCRIPTS}/cwb_setcuts.csh $CWB_MERGE_LABEL.V_hvetoLH.S_bin1_cut ' --wcuts factor==11 --label 300_300_0_0_0_0_0_0       --mcuts factor==11'


$HOME_WAT/tools/cwb/postproduction/scripts/cbc_plots_ifar.csh $CWB_MERGE_LABEL.V_hvetoLH.S_bin1_cut.C_100_20_0_0_0_0_0_0
$HOME_WAT/tools/cwb/postproduction/scripts/cbc_plots_ifar.csh $CWB_MERGE_LABEL.V_hvetoLH.S_bin1_cut.C_100_50_0_0_0_0_0_0
$HOME_WAT/tools/cwb/postproduction/scripts/cbc_plots_ifar.csh $CWB_MERGE_LABEL.V_hvetoLH.S_bin1_cut.C_100_100_0_0_0d8_0_0_0d8
$HOME_WAT/tools/cwb/postproduction/scripts/cbc_plots_ifar.csh $CWB_MERGE_LABEL.V_hvetoLH.S_bin1_cut.C_100_100_0_0_m0d8_0_0_m0d8
$HOME_WAT/tools/cwb/postproduction/scripts/cbc_plots_ifar.csh $CWB_MERGE_LABEL.V_hvetoLH.S_bin1_cut.C_100_100_0_0_0_0_0_0
$HOME_WAT/tools/cwb/postproduction/scripts/cbc_plots_ifar.csh $CWB_MERGE_LABEL.V_hvetoLH.S_bin1_cut.C_200_100_0_0_0_0_0_0
$HOME_WAT/tools/cwb/postproduction/scripts/cbc_plots_ifar.csh $CWB_MERGE_LABEL.V_hvetoLH.S_bin1_cut.C_200_200_0_0_0_0_0_0
$HOME_WAT/tools/cwb/postproduction/scripts/cbc_plots_ifar.csh $CWB_MERGE_LABEL.V_hvetoLH.S_bin1_cut.C_200_50_0_0_0_0_0_0
$HOME_WAT/tools/cwb/postproduction/scripts/cbc_plots_ifar.csh $CWB_MERGE_LABEL.V_hvetoLH.S_bin1_cut.C_300_50_0_0_0_0_0_0
$HOME_WAT/tools/cwb/postproduction/scripts/cbc_plots_ifar.csh $CWB_MERGE_LABEL.V_hvetoLH.S_bin1_cut.C_300_100_0_0_0_0_0_0
$HOME_WAT/tools/cwb/postproduction/scripts/cbc_plots_ifar.csh $CWB_MERGE_LABEL.V_hvetoLH.S_bin1_cut.C_300_200_0_0_0_0_0_0
$HOME_WAT/tools/cwb/postproduction/scripts/cbc_plots_ifar.csh $CWB_MERGE_LABEL.V_hvetoLH.S_bin1_cut.C_300_300_0_0_0_0_0_0

unsetenv CWB_MERGE_LABEL

exit 0
irq_ctrlc:
  ps T | grep root | awk '{print $1}' | xargs kill -9
  exit 1


