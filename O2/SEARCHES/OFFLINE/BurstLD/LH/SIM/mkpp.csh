#!/bin/tcsh -f

onintr irq_ctrlc

unsetenv CWB_MERGE_LABEL
unsetenv CWB_CHUNK_START
unsetenv CWB_CHUNK_STOP
unsetenv CWB_PP_OPTIONS

if ((( $1 == '' ))) then
  echo ""
  echo "CWB_MERGE_LABEL not defined"
  echo ""
  exit 1
else
  setenv CWB_MERGE_LABEL $1
endif

if ((( $2 == '' ))) then
  echo ""
  echo "CWB_CHUNK_START not defined"
  echo ""
  exit 1
else
  setenv CWB_CHUNK_START $2
endif

if ((( $3 == '' ))) then
  echo ""
  echo "CWB_CHUNK_STOP not defined"
  echo ""
  exit 1
else
  setenv CWB_CHUNK_STOP $3
endif

if ((( $4 != 'merge' ) && ( $4 != 'veto' ) && ( $4 != 'cut' ) && ( $4 != 'report' ) && ( $4 != 'all' ) &&( $4 != '' ))) then
  echo ""
  echo \'$4\' "is a wrong cwb pp option"
  echo ""
  echo "available options"
  echo "          no options -> all (default)"
  echo "          merge : execute merge"
  echo "          veto  : execute veto"
  echo "          cut   : execute cut"
  echo "          report: execute report"
  echo "          all   : execute merge+veto+cut+report"
  echo ""
  exit 1
else
  if ((( $4 == '' ))) then
    setenv CWB_PP_OPTIONS  "all"
  else
    setenv CWB_PP_OPTIONS  $4
  endif
endif

if ( ( $CWB_PP_OPTIONS == "merge" ) || ( $CWB_PP_OPTIONS == "all" ) )then
  ${CWB_SCRIPTS}/cwb_merge.csh   $CWB_MERGE_LABEL '--nthreads 10'
  if ( $? != 0) exit 1
endif

if ( ( $CWB_PP_OPTIONS == "veto" ) || ( $CWB_PP_OPTIONS == "all" ) )then
  ${CWB_SCRIPTS}/cwb_setveto.csh $CWB_MERGE_LABEL
  if ( $? != 0) exit 1
endif

if ((( $CWB_PP_OPTIONS == "cut" ) || ( $CWB_PP_OPTIONS == "all" ))) then
  ${CWB_SCRIPTS}/cwb_setcuts.csh $CWB_MERGE_LABEL.V_hvetoLH '--unique true'
  if ( $? != 0) exit 1
endif

if ( ( $CWB_PP_OPTIONS == "report" ) || ( $CWB_PP_OPTIONS == "all" ) )then
  @ K = $CWB_CHUNK_START
  while ($K <= $CWB_CHUNK_STOP)
    set KK=`echo $K | awk '{ printf "%02d\n", $0 }'`
    if($K == $CWB_CHUNK_START) then
      ${CWB_SCRIPTS}/cwb_setifar.csh $CWB_MERGE_LABEL.V_hvetoLH.C_U '           --tsel bin1FR_cut&&O2_K'$KK'_cut  --label bin1FR_cut --file far_bin1_cut_file['$KK'] --mode exclusive'
      if ( $? != 0) exit 1
    else 
      ${CWB_SCRIPTS}/cwb_setifar.csh $CWB_MERGE_LABEL.V_hvetoLH.C_U.S_bin1FR_cut '--tsel bin1FR_cut&&O2_K'$KK'_cut  --label same     --file far_bin1_cut_file['$KK'] --mode exclusive'
      if ( $? != 0) exit 1
    endif
    @ K += 1
  end

  @ K = $CWB_CHUNK_START
  while ($K <= $CWB_CHUNK_STOP)
    set KK=`echo $K | awk '{ printf "%02d\n", $0 }'`
    if($K == $CWB_CHUNK_START) then
      ${CWB_SCRIPTS}/cwb_setifar.csh $CWB_MERGE_LABEL.V_hvetoLH.C_U.S_bin1FR_cut '           --tsel bin2FR_cut&&O2_K'$KK'_cut  --label bin2FR_cut --file far_bin2_cut_file['$KK'] --mode exclusive'
      if ( $? != 0) exit 1
    else 
      ${CWB_SCRIPTS}/cwb_setifar.csh $CWB_MERGE_LABEL.V_hvetoLH.C_U.S_bin1FR_cut.S_bin2FR_cut '--tsel bin2FR_cut&&O2_K'$KK'_cut  --label same     --file far_bin2_cut_file['$KK'] --mode exclusive'
      if ( $? != 0) exit 1
    endif
    @ K += 1
  end

  ${CWB_SCRIPTS}/cwb_report.csh $CWB_MERGE_LABEL.V_hvetoLH.C_U.S_bin1FR_cut.S_bin2FR_cut create
  if ( $? != 0) exit 1
endif

unsetenv CWB_MERGE_LABEL
unsetenv CWB_CHUNK_START
unsetenv CWB_CHUNK_STOP
unsetenv CWB_PP_OPTIONS

exit 0
irq_ctrlc:
  ps T | grep root | awk '{print $1}' | xargs kill -9
  exit 1

