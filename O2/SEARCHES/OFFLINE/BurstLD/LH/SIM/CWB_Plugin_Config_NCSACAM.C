#include "CWB_Plugin.h"

void CWB_PluginConfig() {

  network** net;
  CWB_PLUGIN_IMPORT(network**,net);

  int* gIFOID;
  CWB_PLUGIN_IMPORT(int*,gIFOID);

  int* gIFACTOR;
  CWB_PLUGIN_IMPORT(int*,gIFACTOR);

  CWB::mdc* MDC;
  CWB_PLUGIN_IMPORT(CWB::mdc*,MDC);

  CWB::config** cfg;
  CWB_PLUGIN_IMPORT(CWB::config**,cfg);


  cout << "-----> CWB_Plugin_LT_Config.C -> " << " run : " << (*net)->nRun << endl;

  int seed = (*net)->nRun; 

  unsigned int Pid = gSystem->GetPid();  // used to tag in a unique way the temporary files

  #define LT_WF_DIR 	"/home/claudia.lazzaro/O2/LongSearch/SIMULATION/waveform/O2Official"
  #define LT_INJ_HRSS	3.12e-20
  #define LT_INJ_RATE   1200.    // sec
  #define LT_INJ_OFFSET 200.    // sec
  #define LT_INJ_JITTER	10.	// sec
  const int LT_NCSACAM= 6;
  int LT_WF_TYPE;

  
  TString type = "#CWB_MDC_TYPE";

  if (type=="NCSACAM_A") LT_WF_TYPE=0;
  else if (type=="NCSACAM_B") LT_WF_TYPE=1;
  else if (type=="NCSACAM_C") LT_WF_TYPE=2;
  else if (type=="NCSACAM_D") LT_WF_TYPE=3;
  else if (type=="NCSACAM_E") LT_WF_TYPE=4;
  else if (type=="NCSACAM_F") LT_WF_TYPE=5;
  else  exit(1);
  
  
  CWB::Toolbox TB;
  TString wName[LT_NCSACAM][3];
  double inj_hrss_factor[LT_NCSACAM];
  double wrate[LT_NCSACAM];
  double window[LT_NCSACAM];
  int wfn=0;

  // ---------------------------------------------------
  // NCSACAM waveforms
  // ---------------------------------------------------

  // 0
  wrate[wfn]=4096;
  inj_hrss_factor[wfn] = pow(1.6,3);
  window[wfn]= 600;
  wName[wfn][0] = "NCSACAM_A_4096Hz_hp.dat";
  wName[wfn][1] = "NCSACAM_A_4096Hz_hc.dat";
  wName[wfn++][2] = "NCSACAM_A";

  // 1
  wrate[wfn]=4096;
  inj_hrss_factor[wfn] = pow(1.6,3);
  window[wfn]= 400;
  wName[wfn][0] = "NCSACAM_B_4096Hz_hp.dat";
  wName[wfn][1] = "NCSACAM_B_4096Hz_hc.dat";
  wName[wfn++][2] = "NCSACAM_B";

  // 2
  wrate[wfn]=4096;
  inj_hrss_factor[wfn] = pow(1.6,3);
  window[wfn]= 150;
  wName[wfn][0] = "NCSACAM_C_4096Hz_hp.dat";
  wName[wfn][1] = "NCSACAM_C_4096Hz_hc.dat";
  wName[wfn++][2] = "NCSACAM_C";

  // 3
  wrate[wfn]=4096;
  inj_hrss_factor[wfn] = pow(1.6,3);
  window[wfn]= 200;
  wName[wfn][0] = "NCSACAM_D_4096Hz_hp.dat";
  wName[wfn][1] = "NCSACAM_D_4096Hz_hc.dat";
  wName[wfn++][2] = "NCSACAM_D";

  // 4
  wrate[wfn]=4096;
  window[wfn]= 120;
  inj_hrss_factor[wfn] = pow(1.6,3);
  wName[wfn][0] = "NCSACAM_E_4096Hz_hp.dat";
  wName[wfn][1] = "NCSACAM_E_4096Hz_hc.dat";
  wName[wfn++][2] = "NCSACAM_E";

    // 5
  wrate[wfn]=4096;
  window[wfn]= 45;
  inj_hrss_factor[wfn] = pow(1.6,3);
  wName[wfn][0] = "NCSACAM_F_4096Hz_hp.dat";
  wName[wfn][1] = "NCSACAM_F_4096Hz_hc.dat";
  wName[wfn++][2] = "NCSACAM_F";


    // ---------------------------------------------------
  // copy in tmp the data waveform files
  // ---------------------------------------------------
  for(int i=LT_WF_TYPE;i<LT_WF_TYPE+1;i++) {
    for(int j=0;j<2;j++) {
      TString  inFile = TString::Format("%s/%s.gz",LT_WF_DIR,wName[i][j].Data());
      TString gzipFile = TString::Format("%s/%s.%d.gz",(*cfg)->nodedir,wName[i][j].Data(),Pid);
      TString tmpFile = TString::Format("%s/%s.%d",(*cfg)->nodedir,wName[i][j].Data(),Pid);
      if(!TB.isFileExisting(tmpFile)) {
        gSystem->Exec(TString::Format("cp %s %s",inFile.Data(),gzipFile.Data()));
        cout << "-----> CWB_Plugin_LT_Config.C -> copy " << wName[i][j] << endl;
        gSystem->Exec(TString::Format("gunzip %s",gzipFile.Data()));
        cout << "-----> CWB_Plugin_LT_Config.C -> gunzip " << wName[i][j] << endl;
      }
    }
  }


  // ---------------------------------------------------
  // add LT waveforms
  // ---------------------------------------------------
  vector<mdcpar> par(1);
  par[0].name="hrss";
  par[0].value=-1;      // hrss is not modified in the MDC->AddWaveform
  for(int i=LT_WF_TYPE;i<LT_WF_TYPE+1;i++) {
    MDC->AddWaveform(TString::Format("%s",wName[i][2].Data()),
                    TString::Format("%s/%s.%d",(*cfg)->nodedir,wName[i][0].Data(),Pid),
                    TString::Format("%s/%s.%d",(*cfg)->nodedir,wName[i][1].Data(),Pid),wrate[LT_WF_TYPE],par);
  }

  MDC->Print();

  // ---------------------------------------------------
  // remove the data waveform temporary files from tmp
  // ---------------------------------------------------
  if((*gIFOID)==(*cfg)->nIFO-1) {  
    cout << "-----> CWB_Plugin_LT_Config.C -> remove waveforms temporary files" << endl;
    for(int i=LT_WF_TYPE;i<LT_WF_TYPE+1;i++) for(int j=0;j<2;j++) {
      TString tmpFile = TString::Format("%s/%s.%d",(*cfg)->nodedir,wName[i][j].Data(),Pid);
      gSystem->Exec(TString::Format("rm %s",tmpFile.Data()));
    }
  }

  // --------------------------------------------------------
  // define iwindow parameters
  // --------------------------------------------------------
  (*cfg)->iwindow =window[LT_WF_TYPE] ;
  // --------------------------------------------------------
  // define injection parameters
  // --------------------------------------------------------
  MDC->SetInjHrss(LT_INJ_HRSS*inj_hrss_factor[LT_WF_TYPE]);
  MDC->SetInjRate(1./LT_INJ_RATE);
  MDC->SetInjOffset(LT_INJ_OFFSET);
  MDC->SetInjJitter(LT_INJ_JITTER);

  
  // --------------------------------------------------------
  // define sky distribution
  // --------------------------------------------------------
  par.resize(4);
  par[0].name="entries"; par[0].value=100000;
  par[1].name="rho_min"; par[1].value=0;
  par[2].name="rho_max"; par[2].value=0;
  par[3].name="iota";    par[3].value=-1;	// random
  MDC->SetSkyDistribution(MDC_RANDOM,par,seed);
}
