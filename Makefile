
ifndef DATA
ifndef O3
$(error : missing DATA & O3 options. Ex: 'make DATA=LVC O3=O3a'. Available options are DATA=GWOSC/LVC, O3=O3a/O3b)
endif
endif

ifndef DATA
$(error : missing DATA option. Available options are DATA=GWOSC or DATA=LVC)
else
CFG_DATA_DIR = $(DATA)
endif

ifneq ($(CFG_DATA_DIR),GWOSC)
ifneq ($(CFG_DATA_DIR),LVC)
$(error : error, wrong DATA option. Available options are DATA=GWOSC or DATA=LVC)
endif
endif

ifndef O3
$(error : missing O3 option. Available options are O3=O3a or O3=O3b)
else
CFG_O3_DIR = $(O3)
endif

ifneq ($(CFG_O3_DIR),O3a)
ifneq ($(CFG_O3_DIR),O3b)
$(error : error, wrong DATA option. Available options are O3=O3a or O3=O3b)
endif
endif

all: clean o3 data_o3 data
clean: xdata xdata_o3 xo3

data:
	if [ -d $(CFG_DATA_DIR) ]; then						\
	   cd $(CFG_DATA_DIR); make;						\
	   echo "";								\
	   echo "cWB Config DATA Directory is: $(CFG_DATA_DIR)";		\
	   echo "";								\
	else									\
	   echo "";								\
	   echo "cWB Config DATA Directory: $(CFG_DATA_DIR) not exist";		\
	   echo "";								\
	fi;

data_o3:
	if [ -d $(CFG_DATA_DIR)/$(CFG_O3_DIR) ]; then				\
	   cd $(CFG_DATA_DIR)/$(CFG_O3_DIR); make;				\
	   echo "";								\
	   echo "cWB Config DATA/O3 Dir is: $(CFG_DATA_DIR)/$(CFG_O3_DIR)";	\
	   echo "";								\
	else									\
	   echo "";								\
	   echo "cWB Config DATA/O3 Dir: $(CFG_DATA_DIR)/$(CFG_O3_DIR) not exist";	\
	   echo "";								\
	fi;

o3:
	if [ -d $(CFG_O3_DIR) ]; then						\
	   cd $(CFG_O3_DIR); make;						\
	   echo "";								\
	   echo "cWB Config O3 Directory is: $(CFG_O3_DIR)";			\
	   echo "";								\
	else									\
	   echo "";								\
	   echo "cWB Config O3 Directory: $(CFG_O3_DIR) not exist";		\
	   echo "";								\
	fi;

xdata:
	if [ -d DATA/O3 ]; then							\
	   cd DATA/O3; make clean;						\
	   echo "";								\
	   echo "Removed cWB Config DATA/O3 Directory";				\
	   echo "";								\
	fi;

xdata_o3:
	if [ -d DATA ]; then							\
	   cd DATA; make clean;							\
	   echo "";								\
	   echo "Removed cWB Config DATA Directory";				\
	   echo "";								\
	fi;

xo3:
	if [ -d O3 ]; then							\
	   cd O3; make clean;cd -;						\
	   echo "";								\
	   echo "Removed cWB Config O3 Directory";				\
	   echo "";								\
	fi;

