#!/bin/tcsh -f

setenv	O2_GPS_START		"1164556817"  	# 2016-11-30 16:00:00 UTC Wed

setenv	O2_GPS_STOP		"1187733618"   	# 2017-08-25 22:00:00 UTC Fri

setenv  O2_CHUNK_START          2
setenv  O2_CHUNK_STOP           22

if ("$SITE_CLUSTER" == CIT) then
  setenv  O2_CWB_CONFIG_C02c_CHNAME     "GWOSC-16KHZ_R1_STRAIN"
  setenv  O2_CWB_CONFIG_C02c_V_CHNAME   "GWOSC-16KHZ_R1_STRAIN"
else
  setenv  O2_CWB_CONFIG_C02c_CHNAME     "GWOSC-4KHZ_R1_STRAIN"
  setenv  O2_CWB_CONFIG_C02c_V_CHNAME   "GWOSC-4KHZ_R1_STRAIN"
endif

setenv  O2_SN_PROD_CONDOR_TAG           "disable"
setenv  O2_SN_SIM_CONDOR_TAG            "disable"

setenv  O2_BURSTLF_PROD_CONDOR_TAG      "disable"
setenv  O2_BURSTLF_SIM_CONDOR_TAG       "disable"

setenv  O2_BURSTHF_PROD_CONDOR_TAG      "disable"
setenv  O2_BURSTHF_SIM_CONDOR_TAG       "disable"

setenv  O2_BURSTLD_PROD_CONDOR_TAG      "disable"
setenv  O2_BURSTLD_SIM_CONDOR_TAG       "disable"

setenv  O2_BBH_PROD_CONDOR_TAG          "disable"
setenv  O2_BBH_SIM_CONDOR_TAG           "disable"

setenv  O2_IMBHB_PROD_CONDOR_TAG        "disable"
setenv  O2_IMBHB_SIM_CONDOR_TAG         "disable"

