#!/bin/tcsh -f

setenv	O3_VETO_DEFINITIONS		"/home/waveburst/git/veto-definitions/burst/O3"

setenv  O3_CHUNK_START                  23
setenv  O3_CHUNK_STOP                   40

setenv  O3_GPS_START_C01c               "1256655618"    # 2019-11-01 15:00:00 UTC Fri
setenv  O3_GPS_STOP_C01c                "1269363618"    # 2020-03-27 17:00:00 UTC Fri

if ("$SITE_CLUSTER" == CIT) then
  setenv  O3_CWB_CONFIG_C01c_CHNAME 	"GWOSC-16KHZ_R1_STRAIN"
  setenv  O3_CWB_CONFIG_C01c_V_CHNAME 	"GWOSC-16KHZ_R1_STRAIN"
else
  setenv  O3_CWB_CONFIG_C01c_CHNAME 	"GWOSC-4KHZ_R1_STRAIN"
  setenv  O3_CWB_CONFIG_C01c_V_CHNAME 	"GWOSC-4KHZ_R1_STRAIN"
endif

setenv  O3_DQ_L1CAT0FLAG_C01		"L1:DCS-ANALYSIS_READY_C01:1"
setenv  O3_DQ_H1CAT0FLAG_C01		"H1:DCS-ANALYSIS_READY_C01:1"
setenv  O3_DQ_V1CAT0FLAG_C01		"V1:ITF_SCIENCE:1"

setenv  O3_SN_PROD_CONDOR_TAG           "disable"
setenv  O3_SN_SIM_CONDOR_TAG            "disable"

setenv  O3_BURSTLF_PROD_CONDOR_TAG      "disable"
setenv  O3_BURSTLF_SIM_CONDOR_TAG       "disable"

setenv  O3_BURSTHF_PROD_CONDOR_TAG      "disable"
setenv  O3_BURSTHF_SIM_CONDOR_TAG       "disable"

setenv  O3_BURSTLD_PROD_CONDOR_TAG      "disable"
setenv  O3_BURSTLD_SIM_CONDOR_TAG       "disable"

setenv  O3_BBH_PROD_CONDOR_TAG          "disable"
setenv  O3_BBH_SIM_CONDOR_TAG           "disable"

setenv  O3_IMBHB_PROD_CONDOR_TAG        "disable"
setenv  O3_IMBHB_SIM_CONDOR_TAG         "disable"

