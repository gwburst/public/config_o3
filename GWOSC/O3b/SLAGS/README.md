The SLAG files are used for the background analysis.
The procedure used to generate such files uses the DQ files and chunk lists.

Before to generate SLAG files, check the following data:

- $CWB_CONFIG/O3/CHUNKS/Chunk_List.lst

and

- $CWB_CONFIG/DATA/O3/DATA/

