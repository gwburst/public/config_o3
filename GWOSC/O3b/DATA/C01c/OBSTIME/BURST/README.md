-------------------------------------------------------
single detector cat0 observation time
-------------------------------------------------------

H1_time = 11218675 sec - 129.846 days
-> H1_cat0.txt

L1_time = 11956179 sec - 138.382 days
-> L1_cat0.txt

V1_time = 12038929 sec - 139.339 days
-> V1_cat0.txt


-------------------------------------------------------
double detectors cat0 coincident observation time
-------------------------------------------------------

H1_L1_time = 9226511 sec - 106.788 days
-> H1_L1_cat0.txt

H1_V1_time = 8494284 sec - 98.3135 days
-> H1_V1_cat0.txt

L1_V1_time = 9172309 sec - 106.161 days
-> L1_V1_cat0.txt


-------------------------------------------------------
triple detectors cat0 coincident observation time
-------------------------------------------------------

H1_L1_V1_time = 6985052 sec - 80.8455 days
-> H1_L1_V1_cat0.txt


-------------------------------------------------------
double detectors cat0 exclusive observation time
-------------------------------------------------------

H1_L1_xtime = 2241459 sec - 25.9428 days
-> H1_L1_xcat0.txt

H1_V1_xtime = 1509232 sec - 17.468 days
-> H1_V1_xcat0.txt

L1_V1_xtime = 2187257 sec - 25.3155 days
-> L1_V1_xcat0.txt


-------------------------------------------------------
single detector cat0 exclusive observation time
-------------------------------------------------------

H1_xtime = 482932 sec - 5.58949 days
-> H1_xcat0.txt

L1_xtime = 542411 sec - 6.27791 days
-> L1_xcat0.txt

V1_xtime = 1357388 sec - 15.7105 days
-> V1_xcat0.txt


-------------------------------------------------------
single detector cat0 observation time
-------------------------------------------------------

H1_time = 11218675 sec - 129.846 days
-> H1_cat0.txt

L1_time = 11956179 sec - 138.382 days
-> L1_cat0.txt

V1_time = 12038929 sec - 139.339 days
-> V1_cat0.txt


-------------------------------------------------------
double detectors cat0 coincident observation time
-------------------------------------------------------

H1_L1_time = 9226511 sec - 106.788 days
-> H1_L1_cat0.txt

H1_V1_time = 8494284 sec - 98.3135 days
-> H1_V1_cat0.txt

L1_V1_time = 9172309 sec - 106.161 days
-> L1_V1_cat0.txt


-------------------------------------------------------
triple detectors cat0 coincident observation time
-------------------------------------------------------

H1_L1_V1_time = 6985052 sec - 80.8455 days
-> H1_L1_V1_cat0.txt


-------------------------------------------------------
double detectors cat0 exclusive observation time
-------------------------------------------------------

H1_L1_xtime = 2241459 sec - 25.9428 days
-> H1_L1_xcat0.txt

H1_V1_xtime = 1509232 sec - 17.468 days
-> H1_V1_xcat0.txt

L1_V1_xtime = 2187257 sec - 25.3155 days
-> L1_V1_xcat0.txt


-------------------------------------------------------
single detector cat0 exclusive observation time
-------------------------------------------------------

H1_xtime = 482932 sec - 5.58949 days
-> H1_xcat0.txt

L1_xtime = 542411 sec - 6.27791 days
-> L1_xcat0.txt

V1_xtime = 1357388 sec - 15.7105 days
-> V1_xcat0.txt


-------------------------------------------------------
single detector cat0 observation time
-------------------------------------------------------

V1_time = 9591207 sec - 111.009 days
-> V1_cat0.txt

H1_time = 9967195 sec - 115.361 days
-> H1_cat0.txt

L1_time = 9810816 sec - 113.551 days
-> L1_cat0.txt


-------------------------------------------------------
double detectors cat0 coincident observation time
-------------------------------------------------------

V1_H1_time = 7596208 sec - 87.9191 days
-> V1_H1_cat0.txt

V1_L1_time = 7534406 sec - 87.2038 days
-> V1_L1_cat0.txt

H1_L1_time = 8326187 sec - 96.3679 days
-> H1_L1_cat0.txt


-------------------------------------------------------
triple detectors cat0 coincident observation time
-------------------------------------------------------

V1_H1_L1_time = 6345479 sec - 73.443 days
-> V1_H1_L1_cat0.txt


-------------------------------------------------------
double detectors cat0 exclusive observation time
-------------------------------------------------------

V1_H1_xtime = 1250729 sec - 14.476 days
-> V1_H1_xcat0.txt

V1_L1_xtime = 1188927 sec - 13.7607 days
-> V1_L1_xcat0.txt

H1_L1_xtime = 1980708 sec - 22.9249 days
-> H1_L1_xcat0.txt


-------------------------------------------------------
single detector cat0 exclusive observation time
-------------------------------------------------------

V1_xtime = 806072 sec - 9.32954 days
-> V1_xcat0.txt

H1_xtime = 390279 sec - 4.51712 days
-> H1_xcat0.txt

L1_xtime = 295702 sec - 3.42248 days
-> L1_xcat0.txt


-------------------------------------------------------
single detector cat0 observation time
-------------------------------------------------------

V1_time = 9591207 sec - 111.009 days
-> V1_cat0.txt

H1_time = 9967195 sec - 115.361 days
-> H1_cat0.txt

L1_time = 9810816 sec - 113.551 days
-> L1_cat0.txt


-------------------------------------------------------
double detectors cat0 coincident observation time
-------------------------------------------------------

V1_H1_time = 7596208 sec - 87.9191 days
-> V1_H1_cat0.txt

V1_L1_time = 7534406 sec - 87.2038 days
-> V1_L1_cat0.txt

H1_L1_time = 8326187 sec - 96.3679 days
-> H1_L1_cat0.txt


-------------------------------------------------------
triple detectors cat0 coincident observation time
-------------------------------------------------------

V1_H1_L1_time = 6345479 sec - 73.443 days
-> V1_H1_L1_cat0.txt


-------------------------------------------------------
double detectors cat0 exclusive observation time
-------------------------------------------------------

V1_H1_xtime = 1250729 sec - 14.476 days
-> V1_H1_xcat0.txt

V1_L1_xtime = 1188927 sec - 13.7607 days
-> V1_L1_xcat0.txt

H1_L1_xtime = 1980708 sec - 22.9249 days
-> H1_L1_xcat0.txt


-------------------------------------------------------
single detector cat0 exclusive observation time
-------------------------------------------------------

V1_xtime = 806072 sec - 9.32954 days
-> V1_xcat0.txt

H1_xtime = 390279 sec - 4.51712 days
-> H1_xcat0.txt

L1_xtime = 295702 sec - 3.42248 days
-> L1_xcat0.txt


-------------------------------------------------------
single detector cat0 observation time
-------------------------------------------------------

V1_time = 9591207 sec - 111.009 days
-> V1_cat0.txt

H1_time = 9967195 sec - 115.361 days
-> H1_cat0.txt

L1_time = 9810816 sec - 113.551 days
-> L1_cat0.txt


-------------------------------------------------------
double detectors cat0 coincident observation time
-------------------------------------------------------

V1_H1_time = 7596208 sec - 87.9191 days
-> V1_H1_cat0.txt

V1_L1_time = 7534406 sec - 87.2038 days
-> V1_L1_cat0.txt

H1_L1_time = 8326187 sec - 96.3679 days
-> H1_L1_cat0.txt


-------------------------------------------------------
triple detectors cat0 coincident observation time
-------------------------------------------------------

V1_H1_L1_time = 6345479 sec - 73.443 days
-> V1_H1_L1_cat0.txt


-------------------------------------------------------
double detectors cat0 exclusive observation time
-------------------------------------------------------

V1_H1_xtime = 1250729 sec - 14.476 days
-> V1_H1_xcat0.txt

V1_L1_xtime = 1188927 sec - 13.7607 days
-> V1_L1_xcat0.txt

H1_L1_xtime = 1980708 sec - 22.9249 days
-> H1_L1_xcat0.txt


-------------------------------------------------------
single detector cat0 exclusive observation time
-------------------------------------------------------

V1_xtime = 806072 sec - 9.32954 days
-> V1_xcat0.txt

H1_xtime = 390279 sec - 4.51712 days
-> H1_xcat0.txt

L1_xtime = 295702 sec - 3.42248 days
-> L1_xcat0.txt


