# cWB Pipeline Configuration Files #

This repository contains the necessary cWB data files to
run cWB searches on [O1/O2/O3 GWOSC data](https://www.gw-openscience.org/data/).


This GWOSC DATA repository is based on the /cvmfs file system.
The instructions to configure cvmfs on local machine are reported in following link:
 [Install and configure CernVM-FS for GWOSC Bulk Data](https://www.gw-openscience.org/cvmfs/)

For cWB on VIRTUALBOX it is necessary to perform the following commands every time the virtual machine restart:

-   sudo mount -t cvmfs config-osg.opensciencegrid.org /cvmfs/config-osg.opensciencegrid.org
-   sudo mount -t cvmfs oasis.opensciencegrid.org /cvmfs/oasis.opensciencegrid.org
-   sudo mount -t cvmfs gwosc.osgstorage.org /cvmfs/gwosc.osgstorage.org


