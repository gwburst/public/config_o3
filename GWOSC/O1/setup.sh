#!/bin/bash -f

export O1_GPS_START="1126051217"

export O1_GPS_STOP="1137254417"

export O1_CHUNK_START=1
export O1_CHUNK_STOP=9

if [[ "$SITE_CLUSTER" = CIT ]]; then
  export O1_CWB_CONFIG_C02_CHNAME="GWOSC-16KHZ_R1_STRAIN"
else
  export O1_CWB_CONFIG_C02_CHNAME="LOSC-STRAIN"
fi

export O1_SN_PROD_CONDOR_TAG="disable"
export O1_SN_SIM_CONDOR_TAG="disable"

export O1_BURSTLF_PROD_CONDOR_TAG="disable"
export O1_BURSTLF_SIM_CONDOR_TAG="disable"

export O1_BURSTHF_PROD_CONDOR_TAG="disable"
export O1_BURSTHF_SIM_CONDOR_TAG="disable"

export O1_BURSTLD_PROD_CONDOR_TAG="disable"
export O1_BURSTLD_SIM_CONDOR_TAG="disable"

export O1_BBH_PROD_CONDOR_TAG="disable"
export O1_BBH_SIM_CONDOR_TAG="disable"

export O1_IMBHB_PROD_CONDOR_TAG="disable"
export O1_IMBHB_SIM_CONDOR_TAG="disable"

