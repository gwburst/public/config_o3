#!/bin/tcsh -f

setenv	O1_GPS_START		"1126051217"  	# 2015-09-12 00:00:00 UTC Sat

setenv	O1_GPS_STOP		"1137254417"   	# 2016-01-19 16:00:00 UTC Tue

setenv  O1_CHUNK_START          1
setenv  O1_CHUNK_STOP           9

if ("$SITE_CLUSTER" == CIT) then
  setenv  O1_CWB_CONFIG_C02_CHNAME      "GWOSC-16KHZ_R1_STRAIN"
else
  setenv  O1_CWB_CONFIG_C02_CHNAME 	"LOSC-STRAIN"
endif

setenv  O1_SN_PROD_CONDOR_TAG      	"disable"
setenv  O1_SN_SIM_CONDOR_TAG       	"disable"

setenv  O1_BURSTLF_PROD_CONDOR_TAG      "disable"
setenv  O1_BURSTLF_SIM_CONDOR_TAG       "disable"

setenv  O1_BURSTHF_PROD_CONDOR_TAG      "disable"
setenv  O1_BURSTHF_SIM_CONDOR_TAG       "disable"

setenv  O1_BURSTLD_PROD_CONDOR_TAG      "disable"
setenv  O1_BURSTLD_SIM_CONDOR_TAG       "disable"

setenv  O1_BBH_PROD_CONDOR_TAG          "disable"
setenv  O1_BBH_SIM_CONDOR_TAG           "disable"

setenv  O1_IMBHB_PROD_CONDOR_TAG        "disable"
setenv  O1_IMBHB_SIM_CONDOR_TAG         "disable"

