/*
# Copyright (C) 2019 Gabriele Vedovato
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


//
// Invert Segments
// Author : Gabriele Vedovato


void InvertSegments(TString ofile, TString ifile) {


  vector<waveSegment> ilist; 
  vector<waveSegment> olist; 

  ilist = CWB::Toolbox::readSegments(ifile);

  waveSegment seg;

//  cout << "List of input segments" << endl;
//  cout.precision(16);
//  for(int n=0;n<ilist.size();n++) cout << n << " " << ilist[n].start << " " << ilist[n].stop << endl;

  olist = CWB::Toolbox::invertSegments(ilist);

//  cout << "List of inverted segments" << endl;
//  cout.precision(16);
//  for(int n=0;n<olist.size();n++) cout << n << " " << olist[n].start << " " << olist[n].stop << endl;

  // --------------------------------------------------------------
  // Write output file segment list
  // --------------------------------------------------------------
  ofstream out;
  out.open(ofile.Data(),ios::out);
  out.precision(16);
  for(int n=0;n<olist.size();n++) {
    out << olist[n].start << " " << olist[n].stop << endl;
  }
  out.close();

  cout << "output file list : " << ofile << endl;

  exit(0);
}

