{                                                                                                    
  //!NOISE_MDC_SIMULATION
  // Config Plugin to generate injected 'on the fly' EOBNRv2 from LAL

  cout << "Execute CWB_Plugin_MDC_OTF_Config.C ..." << endl;

  CWB::mdc MDC(net);

  // ---------------------------------------------------------------------------
  // set inspiral parms
  // waveforms are produced by the LAL library
  // to dump all the available options do:
  // $LALINSPINJ_EXEC --help
  // for any details refer to the LAL documentation.
  // there are some special options added only for the mdc class
  // --approximant       : is used as alternative to --waveform to force 
  //                       the use of the new XLALSimInspiralChooseWaveformFromSimInspiral
  // --output "file.xml" : write mdc injection's parameters to xml file. default=/tmp   
  // WARNING : write a space characters at the end of each inspOptions line !!!
  // ---------------------------------------------------------------------------

  char st[512];
  sprintf(st,"--xml /home/waveburst/git/cWB/config/O1/SEARCHES/BBH/SIMULATIONS/BBH1-12/XML/HL-INJECTIONS_%d_BBH%d_cWB-1126051217-11206883.xml",78900+gIFACTOR,gIFACTOR);
  TString inspOptions="";
  inspOptions+=st;
  inspOptions+= " --dir "+TString(cfg->tmp_dir)+" ";

  // the first parameter a the name of MDC defined by the user
  MDC.SetInspiral("SEOBNRv2pseudoFourPN",inspOptions);
 
}

