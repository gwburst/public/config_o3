{

  int     chunkID       = #CWB_CHUNK_NUMBER;
  TString calibVer      = "#CWB_CALIB_VER";     // C00, C01, C02
  TString channelName   = "#CWB_CHANNEL_NAME";  // C00 -> "GDS-CALIB_STRAIN", C01 -> "DCS-CALIB_STRAIN_C01", C02 -> "DCS-CALIB_STRAIN_C02"

  strcpy(analysis,"2G");

  nIFO = 2;
  cfg_search = 'r';
  optim=false;

  strcpy(ifo[0],"L1");
  strcpy(ifo[1],"H1");
  strcpy(refIFO,"L1");

  //lags
  lagSize = 1;
  lagStep = 1.;
  lagOff  = 0;
  lagMax  = 0;

  //superlags
  slagSize = 1;		     // number of super lags (simulation=1) - if slagSize=0 -> Standard Segments
  slagMin  = 0;
  slagMax  = 0;
  slagOff  = 0;

  //jobs
  segLen  = 1200;
  segMLS  = 600;
  segTHR  = 200;
  segEdge = 10;

  //frequency
  fLow  = 16.;        // low frequency of the search
  fHigh = 1024.;      // high frequency of the search

  levelR   = 3;
  l_low    = 4;       // low frequency resolution level
  l_high   = 10;      // high frequency resolution level

  strcpy(wdmXTalk,"wdmXTalk/OverlapCatalog16-1024.bin");

  healpix=7;

  bpp    = 0.001;
  subnet = 0.5;
  subcut = 0.0;
  netRHO = 5.5;
  netCC  = 0.5;
  Acore  = 1.7;
  Tgap   = 0.2;
  Fgap   = 128.0;
  delta  = 0.5;
  cfg_gamma = -1.0;
  LOUD   = 300;

  pattern = 5;

  precision=GetPrecision(100,5);

  //simulation
  int simulation = 4;    // 1 for simulation, 0 for production
  int nfactor = 12;      // number of strain factors
  factors[0]=0;

  strcpy(injectionList,"input/injectionList.inj");

  sprintf(channelNamesRaw[0],"L1:%s",channelName.Data());
  sprintf(channelNamesRaw[1],"H1:%s",channelName.Data());

  sprintf(frFiles[0],"%s/O1/DATA/%s/FRAMES/%s/L1_frames.in",cwb_config_env,calibVer.Data(),site_cluster_env);
  sprintf(frFiles[1],"%s/O1/DATA/%s/FRAMES/%s/H1_frames.in",cwb_config_env,calibVer.Data(),site_cluster_env);


  // dq file list
  // {ifo, dqcat_file, dqcat[0/1/2], shift[sec], inverse[false/true], 4columns[true/false]}
  nDQF=10;
  dqfile dqf[10]={

                    {"L1" ,"",   CWB_CAT0, 0., false, false},
                    {"H1" ,"",   CWB_CAT0, 0., false, false},

                    {"L1" ,"",   CWB_CAT1, 0., true,  false},
                    {"H1" ,"",   CWB_CAT1, 0., true,  false},

                    {"L1" ,"",   CWB_CAT2, 0., true,  false},
                    {"H1" ,"",   CWB_CAT2, 0., true,  false},

                    {"L1" ,"",   CWB_CAT1, 0., true,  false},
                    {"H1" ,"",   CWB_CAT1, 0., true,  false},

                    {"L1" ,"",   CWB_CAT0, 0., false, false},
                    {"H1" ,"",   CWB_CAT0, 0., false, false}

                   };
  for(int i=0;i<10;i++) DQF[i]=dqf[i];

  sprintf(DQF[0].file, "%s/O1/DATA/%s/DQ/BURST/L1_cat0.txt",cwb_config_env,calibVer.Data());
  sprintf(DQF[1].file, "%s/O1/DATA/%s/DQ/BURST/H1_cat0.txt",cwb_config_env,calibVer.Data());
  
  sprintf(DQF[2].file, "%s/O1/DATA/%s/DQ/BURST/L1_cat1.txt",cwb_config_env,calibVer.Data());
  sprintf(DQF[3].file, "%s/O1/DATA/%s/DQ/BURST/H1_cat1.txt",cwb_config_env,calibVer.Data());
  
  sprintf(DQF[4].file, "%s/O1/DATA/%s/DQ/BURST/L1_cat2.txt",cwb_config_env,calibVer.Data());
  sprintf(DQF[5].file, "%s/O1/DATA/%s/DQ/BURST/H1_cat2.txt",cwb_config_env,calibVer.Data());

  sprintf(DQF[6].file, "%s/O1/DATA/%s/DQ/BURST/L1_cat4.txt",cwb_config_env,calibVer.Data());
  sprintf(DQF[7].file, "%s/O1/DATA/%s/DQ/BURST/H1_cat4.txt",cwb_config_env,calibVer.Data());

  sprintf(DQF[8].file, "%s/O1/CHUNKS/K%02d.period",cwb_config_env,chunkID);
  sprintf(DQF[9].file, "%s/O1/CHUNKS/K%02d.period",cwb_config_env,chunkID);


  plugin = TMacro("macro/CWB_Plugin_MDC_OTF_Gating_QLveto.C");            	// Macro source
  plugin.SetTitle("macro/CWB_Plugin_MDC_OTF_Gating_QLveto_C.so");
  configPlugin = TMacro("macro/CWB_Plugin_Config.C");       			// Macro config

  strcpy(condor_tag,"#BBH_SIM_CONDOR_TAG");

}
