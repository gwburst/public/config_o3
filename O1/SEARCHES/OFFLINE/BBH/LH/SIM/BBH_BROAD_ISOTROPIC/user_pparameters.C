
#define nRHO 300

  // -----------------------------------------------------------
  // CBC
  // -----------------------------------------------------------
//#define MIN_plot_mass1 0.0
//#define MAX_plot_mass1 150
//#define MIN_plot_mass2 0.0
//#define MAX_plot_mass2 100

//#define MAX_EFFECTIVE_RADIUS  2000.
//#define MASS_BIN 15.0
//#define MIN_MASS 0.0
//#define MAX_MASS 150

//#define MINCHI -1
// #define MAXCHI 1
//#define CHI_BIN 0.666666

//#define TOLERANCE 0.01
#define RUN_LABEL "O1: BBH rates from Broad (isotropic) distribution (SEOBNRv3_opt_rk4pseudoFourPN)"

//#define BKG_NTUPLE "True"
//#define PLOT_CHIRP
//#define PLOT_MASS_RATIO
//#define VOL_Mtotq  ///Volume calculated not correcting to make it uniform in M1 and M2
//#define FIXMINDISTANCE 0
//#define FIXMAXDISTANCE 2600000
//#define FIXMAXDISTANCE 5000000

//#define FIXMINRATIO 0.1
//#define FIXMAXRATIO 8.0
//#define FIXMINTOT 10
//#define FIXMAXTOT 150

//#define FIXMINRATIO 0.9
//#define FIXMAXRATIO 1.1
//#define FIXMINTOT 298
//#define FIXMAXTOT 302
//#define FIXMINTOT MIN_plot_mass1+MIN_plot_mass2
//#define FIXMAXTOT MAX_plot_mass1+MAX_plot_mass2
//#define WRITE_ASCII

//#define LIVE_ZERO  4141152 //WARNING: full O1 from Igor's standard segments, i.e. 47.93 days!
#define REDSHIFT

//#define CAT2_VETO
//#define CAT3_VETO
#define HVETO_VETO
//#define FAR_BIN1_FILE_NAME "report/postprod/M1.V_hvetoLH.C_bin1_cut.R_rMRA_hveto_i0cc00_i1rho0_freq16_1024/data/far_rho.txt"

{

  #include <O1/SEARCHES/OFFLINE/BBH/LH/PP_Cuts.hh>
  #include <O1/CHUNKS/Chunks_Cuts.hh>

  #define nCHUNKS	9

  // ---------------------------------------------------------------
  // All numbers, files etc. related to the the MonteCarlo....
  // ---------------------------------------------------------------
 
  user_pp_label = "";
  //INCLUDE_INTERNAL_VOLUME = 0;
  //TRIALS = 1;

  // fiducial space-time volume
  double vt[nCHUNKS] = {
			    1.85992714532,
			    1.56639882003,
			    1.6514397367,
			    2.09036059696,
			    2.05744153244,
			    1.62949369369,
			    1.944968062,
			    1.83249459156,
			    2.44771380665
    			};

  // number of events which do not pass the snr threshold used to generate the XML files
  int rejected[nCHUNKS] = {
			    139399,
			    128129,
			    115581,
			    145938,
			    150512,
			    115684,
			    145885,
			    176082,
			    228202
  			};

  // number of events which pass the snr threshold used to generate the XML files
  // events in the XML files
  int accept[nCHUNKS] = {
			    12204,
			    10278,
			    10836,
			    13716,
			    13500,
			    10692,
			    12762,
			    12024,
			    16061
  			};


  //All numbers, files etc. related to the the MonteCarlo....
  int SEGS2[nCHUNKS][3] = {
			    {1, 1126051217, 1127271617},
			    {2, 1127271617, 1128299417},
			    {3, 1128299417, 1129383017},
			    {4, 1129383017, 1130754617},
			    {5, 1130754617, 1132104617},
			    {6, 1132104617, 1133173817},
			    {7, 1133173817, 1134450017},
			    {8, 1134450017, 1135652417},
			    {9, 1135652417, 1137258496}
			};

  // compute fiducial space-time volume
  VT = 0.0;
  int NINJ[nCHUNKS];
  double acceptance[nCHUNKS];
  for(int n=1;n<=nCHUNKS;n++) {
    acceptance[n-1] = double(accept[n-1])/double(accept[n-1]+rejected[n-1]);
    VT += vt[n-1]*acceptance[n-1];
    NINJ[0] += accept[n-1];	
  }

  // read far vs rho background files 
  TString far_bin1_cut_file[1024];
  vector<TString> xfile = CWB::Toolbox::readFileList("config/far_rho.lst");
  for(int n=0;n<xfile.size();n++) far_bin1_cut_file[n+1]=xfile[n];
  if(xfile.size()!=nCHUNKS) {
     cout << endl;
     for(int n=0;n<xfile.size();n++) cout << n+1 << " " << far_bin1_cut_file[n+1] << endl;
     cout << endl << "user_pparameters.C error: config/far_rho.lst files' list size must be = "
          << nCHUNKS << endl << endl;
     exit(1);
  }

  // compute total MonteCarlo Time
  TMC = 0;
  TString XML[nCHUNKS];
  for(int i=0; i<nCHUNKS; i++){
     TMC += SEGS2[i][2]-SEGS2[i][1];
     XML[i].Form("bbh_broad_isotropic-%d-%d.xml", SEGS2[i][1], SEGS2[i][2]);
     //cout <<"XML[" << i <<"]=" <<XML[i]<<endl;
  }


  T_cor      = 0.0;       // cc cut
  T_cut      = 0.0;       // rho high frequency cut
  T_out	     = 0.0;
  hours      = 1.;        // bin size in hours for rate vs time plot
  //T_ifar     = 4.00812428565954726e-01;
  T_ifar     = 1;
  T_win      = 0.2;

  pp_irho    = 1;
  pp_inetcc  = 0;
  pp_rho_min = 4.5;
  pp_rho_max = 10;
  pp_drho    = 0.1;

  pp_max_nloudest_list = 20; 

//  pp_jet_benckmark = -1;
//  pp_mem_benckmark = -1;

  // ----------------------------------------------------------
  // VETO cuts
  // dq file list
  // {ifo, dqcat_file, dqcat[0/1/2], shift[sec], inverse[false/true], 4columns[true/false]}
  // ----------------------------------------------------------
  const int nvdqf=2;
  dqfile vdqf[2] = {
         {"L1" ,"",     CWB_HVETO, 0., false,   false},
         {"H1" ,"",     CWB_HVETO, 0., false,   false}
  };

  sprintf(vdqf[0].file, "%s/O1/DATA/%s/HVETO/L1/HVETO_L1_ALLO1_MERGED.txt",cwb_config_env,calibVer.Data());
  sprintf(vdqf[1].file, "%s/O1/DATA/%s/HVETO/H1/HVETO_H1_ALLO1_MERGED.txt",cwb_config_env,calibVer.Data());

}
