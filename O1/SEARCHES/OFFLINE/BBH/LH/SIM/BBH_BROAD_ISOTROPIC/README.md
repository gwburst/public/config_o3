# O2 BBH Catalog paper injection sets #
* https://git.ligo.org/RatesAndPopulations/lvc-rates-and-pop/tree/master/share/O2/injection_files
# Old wiki with some discussion on BBH parameters #
* https://www.lsc-group.phys.uwm.edu/ligovirgo/cbcnote/RatesAndSignificance/O2InjectionsForRatesStatements/ParameterDistributions#Location_of_XML_files
