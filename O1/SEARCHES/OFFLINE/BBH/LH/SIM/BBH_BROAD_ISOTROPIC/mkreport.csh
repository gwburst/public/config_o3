#!/bin/tcsh -f

onintr irq_ctrlc

unsetenv CWB_MERGE_LABEL

if ((( $1 == '' ))) then
  echo ""
  echo "CWB_MERGE_LABEL not defined"
  echo ""
  exit 1
else
  setenv CWB_MERGE_LABEL $1
endif

${CWB_SCRIPTS}/cwb_setcuts.csh $CWB_MERGE_LABEL '--tcuts O1_K01_cut --mcuts time[0]>1126051217&&time[0]<=1127271617 --label O1_K01_cut'
${CWB_SCRIPTS}/cwb_setcuts.csh $CWB_MERGE_LABEL '--tcuts O1_K02_cut --mcuts time[0]>1127271617&&time[0]<=1128299417 --label O1_K02_cut'
${CWB_SCRIPTS}/cwb_setcuts.csh $CWB_MERGE_LABEL '--tcuts O1_K03_cut --mcuts time[0]>1128299417&&time[0]<=1129383017 --label O1_K03_cut'
${CWB_SCRIPTS}/cwb_setcuts.csh $CWB_MERGE_LABEL '--tcuts O1_K04_cut --mcuts time[0]>1129383017&&time[0]<=1130754617 --label O1_K04_cut'
${CWB_SCRIPTS}/cwb_setcuts.csh $CWB_MERGE_LABEL '--tcuts O1_K05_cut --mcuts time[0]>1130754617&&time[0]<=1132104617 --label O1_K05_cut'
${CWB_SCRIPTS}/cwb_setcuts.csh $CWB_MERGE_LABEL '--tcuts O1_K06_cut --mcuts time[0]>1132104617&&time[0]<=1133173817 --label O1_K06_cut'
${CWB_SCRIPTS}/cwb_setcuts.csh $CWB_MERGE_LABEL '--tcuts O1_K07_cut --mcuts time[0]>1133173817&&time[0]<=1134450017 --label O1_K07_cut'
${CWB_SCRIPTS}/cwb_setcuts.csh $CWB_MERGE_LABEL '--tcuts O1_K08_cut --mcuts time[0]>1134450017&&time[0]<=1135652417 --label O1_K08_cut'
${CWB_SCRIPTS}/cwb_setcuts.csh $CWB_MERGE_LABEL '--tcuts O1_K09_cut --mcuts time[0]>1135652417&&time[0]<=1137254417 --label O1_K09_cut'

${CWB_SCRIPTS}/cwb_report_cbc.csh $CWB_MERGE_LABEL.C_O1_K01_cut
${CWB_SCRIPTS}/cwb_report_cbc.csh $CWB_MERGE_LABEL.C_O1_K02_cut
${CWB_SCRIPTS}/cwb_report_cbc.csh $CWB_MERGE_LABEL.C_O1_K03_cut
${CWB_SCRIPTS}/cwb_report_cbc.csh $CWB_MERGE_LABEL.C_O1_K04_cut
${CWB_SCRIPTS}/cwb_report_cbc.csh $CWB_MERGE_LABEL.C_O1_K05_cut
${CWB_SCRIPTS}/cwb_report_cbc.csh $CWB_MERGE_LABEL.C_O1_K06_cut
${CWB_SCRIPTS}/cwb_report_cbc.csh $CWB_MERGE_LABEL.C_O1_K07_cut
${CWB_SCRIPTS}/cwb_report_cbc.csh $CWB_MERGE_LABEL.C_O1_K08_cut
${CWB_SCRIPTS}/cwb_report_cbc.csh $CWB_MERGE_LABEL.C_O1_K09_cut

unsetenv CWB_MERGE_LABEL

exit 0
irq_ctrlc:
  ps T | grep root | awk '{print $1}' | xargs kill -9
  exit 1


