
#include "CWB_Plugin.h"

void CWB_PluginConfig() {

  // Config Plugin used to generate injections from posterior samples XML files

  CWB::mdc* MDC;
  CWB_PLUGIN_IMPORT(CWB::mdc*,MDC);

  CWB::config** cfg;
  CWB_PLUGIN_IMPORT(CWB::config**,cfg);

  TString inspOptions="";
  inspOptions+= "--dir "+TString((*cfg)->nodedir)+" ";
  inspOptions+= "--xml config/injections.xml";
  MDC->SetInspiral("PE",inspOptions);

//  MDC->SetInspiralCLB("config/injections.clb");

}
