
// -------------------------------------------------------------------------
// Definitions of the search bins used for Offline IMBH Search
// -------------------------------------------------------------------------

// definition of the selection selection cuts used in O3b run

TCut dqveto_cut("dqveto_cut","!veto_hveto_H1");
TCut norm_cut("norm_cut","norm>4");   // used in O2 paper
TCut freq_cut("freq_cut","frequency[0]>24 && frequency[0]<100");
TCut chi2_cut("chi2_cut","log10(penalty)<0.4");
TCut netcc_cut("netcc_cut","netcc[0]>0.5 && netcc[2]>0.5");
TCut qveto_cut("qveto_cut","Qveto[0]>0.09");
TCut chirp_cut("chirp_cut","abs(chirp[1])>10 && abs(chirp[1]/Qveto[0])>15 && chirp[1]>-100");
TCut TF_cut("TF_cut","3*bandwidth[0]*duration[0]*sqrt(size[0]/norm)/rho[1]<(chirp[1]>0?sqrt(Qveto[0]):0.3)");

// definition of the inclusive bins
TCut bin1_cut=TCut("bin1_cut",(dqveto_cut+norm_cut+freq_cut+chi2_cut+netcc_cut+qveto_cut+chirp_cut+TF_cut).GetTitle());

// definition of the bin0, used for selection in the exclusive HV time
// veto_user_H1 && veto_user_V1 are true in the exclusive HV time
// only the zero lag (abs(time[0]-time[1])<0.05) is affected by this cut
TCut exclusive_zerolag_cut("exclusive_zerolag_cut","((veto_user_H1 && veto_user_V1 && abs(time[0]-time[1])<0.05) || (abs(time[0]-time[1])>=0.05))");
TCut bin0_cut=TCut("bin0_cut",(bin1_cut+exclusive_zerolag_cut).GetTitle());

