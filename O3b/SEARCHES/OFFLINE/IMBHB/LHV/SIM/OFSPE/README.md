# PE configuration setup used for OFFSOURCE PE simulations

Example:

cd /home/vedovato/O3/SEARCHES/OFFLINE/IMBHB/LHV/EVT/S190521g

// OFF SOURCE

cwb_mkchunk --run O3 --chunk 06 --cal C00 --net LHV --search IMBHB --type SIM/OFSPE --tag S190521g_EXP2_dev1
ln -sf ../../PE/EXP2/freffix/posterior_samples_S190521g_EXP2_K06_TS150s.xml O3_K06_C00_LHV_IMBHB_SIM_OFSPE_S190521g_EXP2_dev1/config/injections.xml
cd O3_K06_C00_LH_IMBHB_SIM_OFSPE_S190521g_EXP2_dev1
cwb_condor create
cwb_condor submit

cwb_ppchunk --run O3 --chunk 06 --cal C00 --net LHV --search IMBHB --type SIM/OFSPE --tag S190521g_EXP2_dev1 --opt=all


// POST PRODUCTION

cwb_pereport CONFIG=Makefile.pe_config.dev1 GW=S190521g SIDE=full UTAG=maxl_dev1 pestat
cwb_pereport CONFIG=Makefile.pe_config.dev1 GW=S190521g SIDE=full UTAG=maxl_dev1 pewave

where Makefile.pe_config.dev1 is the Makefile configuration file:

Ex:


ifeq ($(GW_NAME),S190521g)
GW_GPS        = 1242442967.44
PE_TSTEP      = 150.
PE_ONPATH     = /home/vedovato/O3/SEARCHES/OFFLINE/IMBHB/LHV/EVT/S190521g/O3_K6_C00_LHV_IMBHB_SIM_ONSPE_S190521g_EXP2_maxl_dev1
PE_OFFPATH    = /home/vedovato/O3/SEARCHES/OFFLINE/IMBHB/LHV/EVT/S190521g/O3_K6_C00_LHV_IMBHB_SIM_OFSPE_S190521g_EXP2_dev1
#PE_OFFPATH    = /home/vedovato/O3/SEARCHES/OFFLINE/IMBHB/LHV/EVT/S190521g/O3_K6_C00_LHV_IMBHB_SIM_ONSPE_S190521g_EXP2_pe_dev1
PE_MTAG       = M1.C_U.C_bin1_cut
PE_RDIR       = pestat_maxl_dev1
endif


PE_WWW = /home/vedovato/O3/SEARCHES/OFFLINE/BBH/LHV/WWW/cWB_Waveform_Reconstruction_O3_Catalog/preliminary1

