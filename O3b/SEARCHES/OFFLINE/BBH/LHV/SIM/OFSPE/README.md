# PE configuration setup used for OFFSOURCE PE simulations

Example:

cd /home/vedovato/O3/SEARCHES/OFFLINE/BBH/LHV/EVT/S190707q

// OFF SOURCE

cwb_mkchunk --run O3 --chunk 12 --cal C00 --net LHV --search BBH --type SIM/OFSPE --tag S190707q_EXP3_dev1
ln -sf ../../PE/EXP3/freffix/posterior_samples_S190707q_EXP3_K12_TS150s.xml O3_K12_C00_LHV_BBH_SIM_OFSPE_S190707q_EXP3_dev1/config/injections.xml
cd O3_K12_C00_LHV_BBH_SIM_OFSPE_S190707q_EXP3_dev1
cwb_condor create
cwb_condor submit

cwb_ppchunk --run O3 --chunk 12 --cal C00 --net LHV --search BBH --type SIM/OFSPE --tag S190707q_EXP3_dev1 --opt=all


// POST PRODUCTION

cwb_pereport CONFIG=Makefile.pe_config.dev1 GW=S190707q SIDE=full UTAG=maxl_dev1 pestat
cwb_pereport CONFIG=Makefile.pe_config.dev1 GW=S190707q SIDE=full UTAG=maxl_dev1 pewave

where Makefile.pe_config.dev1 is the Makefile configuration file:

Ex:

ifeq ($(GW_NAME),S190707q)
GW_GPS        = 1246527224.18
PE_TSTEP      = 150.
PE_ONPATH     = /home/vedovato/O3/SEARCHES/OFFLINE/BBH/LHV/EVT/S190707q/O3_K12_C00_LHV_BBH_SIM_ONSPE_S190707q_EXP3_maxl_dev1
PE_OFFPATH    = /home/vedovato/O3/SEARCHES/OFFLINE/BBH/LHV/EVT/S190707q/O3_K12_C00_LHV_BBH_SIM_OFSPE_S190707q_EXP3_dev1
PE_MTAG       = M1.C_U.C_bin1_cut
PE_RDIR       = pestat_maxl_dbg4
endif

PE_WWW = /home/vedovato/O3/SEARCHES/OFFLINE/BBH/LHV/WWW/cWB_Waveform_Reconstruction_O3_Catalog/preliminary1

