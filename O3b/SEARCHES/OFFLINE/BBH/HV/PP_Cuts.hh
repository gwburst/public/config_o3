// ----------------------------------------------------------------------------
// Definitions of the search bins used for the Offline Stellar Mass BBH Search
// ----------------------------------------------------------------------------

// definition of the selection selection cuts used in O3b run
TCut dqveto("dqveto","!veto_hveto_H1");
TCut norm_cut("norm_cut","norm>2.5");
TCut freq_cut("freq_cut","frequency[0]>60 && frequency[0]<300");
TCut netcc_cut("netcc_cut","netcc[0]>0.5 && netcc[2]>0.5");
TCut qveto_cut("qveto_cut","Qveto[0]>0.25");
TCut lveto_cut("lveto_cut","!(bandwidth[0]<8 || (Lveto[1]<5 && Lveto[2]>0.8))");
TCut chi2_cut("chi2_cut","log10(penalty)<0.35");
TCut chirp_cut("chirp_cut","chirp[1]>1 && chirp[1]<70");

// definition of the exclusive bins
TCut bin1_cut = TCut("bin1_cut",(dqveto+norm_cut+freq_cut+netcc_cut+qveto_cut+lveto_cut+chi2_cut+chirp_cut).GetTitle());

// definition of the bin0, used for selection in the exclusive HV time
// veto_user_H1 && veto_user_V1 are true in the exclusive HV time
// only the zero lag (abs(time[0]-time[1])<0.05) is affected by this cut
TCut exclusive_zerolag_cut("exclusive_zerolag_cut","((veto_user_H1 && veto_user_V1 && abs(time[0]-time[1])<0.05) || (abs(time[0]-time[1])>=0.05))");
TCut bin0_cut=TCut("bin0_cut",(bin1_cut+exclusive_zerolag_cut).GetTitle());

