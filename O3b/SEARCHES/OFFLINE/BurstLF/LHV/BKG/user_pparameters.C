#define RUN_LABEL "BurstLF O3 #CWB_CALIB_VER K#CWB_CHUNK_NUMBER"
//#define CAT2_VETO
//#define CAT3_VETO
#define HVETO_VETO
{
  #include <O3/SEARCHES/OFFLINE/BurstLF/LHV/PP_Cuts.hh>

  chunkID = #CWB_CHUNK_NUMBER;
  calibVer      = "#CWB_CALIB_VER";

  T_cor      = 0.0;       // cc cut
  T_cut      = 0.0;       // rho high frequency cut
  T_out	     = 0.0;
  hours      = 1.;        // bin size in hours for rate vs time plot

  pp_irho    = 0;
  pp_inetcc  = 0;
  pp_rho_min = 5.0;
  pp_rho_max = 10;
  pp_drho    = 0.1;

  pp_max_nloudest_list = 20; 

//  pp_jet_benckmark = -1;
//  pp_mem_benckmark = -1;

  // ----------------------------------------------------------
  // VETO cuts
  // dq file list
  // {ifo, dqcat_file, dqcat[0/1/2], shift[sec], inverse[false/true], 4columns[true/false]}
  // ----------------------------------------------------------
  const int nvdqf=2;
  dqfile vdqf[nvdqf] = {
         {"L1" ,"",   	CWB_HVETO, 0., false,   false},
         {"H1" ,"",   	CWB_HVETO, 0., false,   false}
  };

  sprintf(vdqf[0].file, "%s/O3/DATA/%s/HVETO/L1/HVETO_L1_O3A_SEGMENTS_MERGED.txt",cwb_config_env,calibVer.Data());
  sprintf(vdqf[1].file, "%s/O3/DATA/%s/HVETO/H1/HVETO_H1_O3A_SEGMENTS_MERGED.txt",cwb_config_env,calibVer.Data());

}
