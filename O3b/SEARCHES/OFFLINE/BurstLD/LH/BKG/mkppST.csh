#!/bin/tcsh -f

onintr irq_ctrlc

unsetenv CWB_MERGE_LABEL
unsetenv CWB_PP_OPTIONS

if ((( $1 == '' ))) then
  setenv CWB_MERGE_LABEL "M1"   # default
else
  setenv CWB_MERGE_LABEL $1
endif

if ((( $4 != 'merge' ) && ( $4 != 'veto' ) && ( $4 != 'cut' ) && ( $4 != 'report' ) && ( $4 != 'all' ) &&( $4 != '' ))) then
  echo ""
  echo \'$4\' "is a wrong cwb pp option"
  echo ""
  echo "available options"
  echo "          no options -> all (default)"
  echo "          merge : execute merge"
  echo "          veto  : execute veto"
  echo "          cut   : execute veto"
  echo "          report: execute report"
  echo "          all   : execute merge+veto+cut+report"
  echo ""
  exit 1
else
  if ((( $4 == '' ))) then
    setenv CWB_PP_OPTIONS  "all"
  else
    setenv CWB_PP_OPTIONS  $4
  endif
endif

if ((( $CWB_PP_OPTIONS == "merge" ) || ( $CWB_PP_OPTIONS == "all" ))) then
  ${CWB_SCRIPTS}/cwb_merge.csh   $CWB_MERGE_LABEL '--nthreads 10'
  if ( $? != 0) exit 1
endif

if ((( $CWB_PP_OPTIONS == "veto" ) || ( $CWB_PP_OPTIONS == "all" ))) then
  ${CWB_SCRIPTS}/cwb_setveto.csh $CWB_MERGE_LABEL
  if ( $? != 0) exit 1
endif

if ((( $CWB_PP_OPTIONS == "cut" ) || ( $CWB_PP_OPTIONS == "all" ))) then
  ${CWB_SCRIPTS}/cwb_setcuts.csh $CWB_MERGE_LABEL.V_hvetoLH "--tcuts bin1_cut --label bin1_cut"
  if ( $? != 0) exit 1
endif

if ((( $CWB_PP_OPTIONS == "report" ) || ( $CWB_PP_OPTIONS == "all" ))) then
  ${CWB_SCRIPTS}/cwb_report.csh  $CWB_MERGE_LABEL.V_hvetoLH.C_bin1_cut create
  if ( $? != 0) exit 1

  ${CWB_SCRIPTS}/cwb_setifar.csh $CWB_MERGE_LABEL.V_hvetoLH.C_bin1_cut '--xtsel run>=0  --label ifar --xfile report/postprod/'$CWB_MERGE_LABEL'.V_hvetoLH.C_bin1_cut.R_rMRA_hveto_i0cc00_i0rho0_freq16_2048/data/far_rho.txt --mode exclusive'
  if ( $? != 0) exit 1
endif

unsetenv CWB_MERGE_LABEL
unsetenv CWB_PP_OPTIONS

exit 0
irq_ctrlc:
  ps T | grep root | awk '{print $1}' | xargs kill -9
  exit 1

