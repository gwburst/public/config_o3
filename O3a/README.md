# O3 Configuration Structure #

## setup of O3 environment ##
* setup.csh

## files with chunk periods ##
* CHUNKS

## dq & frame lists ##
* DATA
	* C00  
		* DQ (data quality cat0/1/2/4)
			* BURST(dq burst)
			* BURST_HF(dq burst hf)
			* BURST_IMBH(dq burst_imbh)

		* FRAMES (frame file lists)
			* CIT (cit frame file lists)

		* HVETO 
	* C01  
		* same structure as C00

	
## macros used to generate configuration files ##
* MACROS

## scripts used to manage chunk BKG/SIM productions/post-production ##
* SCRIPTS
	* cwb_clchunk.csh
	* cwb_lschunk.csh
	* cwb_mkchunk.csh
	* cwb_mklinks.csh
	* cwb_mksdirs.csh
	* cwb_obchunk.csh
	* cwb_ppchunk.csh

## files which contain the list of slags used in the search analysis & slagSizeMax.lst (max slagSize for eaxh chunk)##
* SLAGS
	* LH (LH network)
		* C00  	
		* C01  

## cwb searches ##
* SEARCHES
	* PLUGINS (cwb plugins used in the analysis)
	* OFFLINE
		* BurstLF (burst allsky low frequency)
			- LH/LHV
				* PP_Cuts.hh  (PP cuts file used for BurstLF)
				* BKG (background configuration files)
				  - mkpp.csh (post-production script used for BKG BurstLF)
				* SIM
				  - mkpp.csh (post-production script used for SIM BurstLF)
				  - Standard_InjBuiltin (Standard MDC set with built-in injections)
				  - Standard_InjFrames  (Standard MDC set with injections from frame files)
		* BurstHF (burst allsky high frequency)
			- LH/LHV
				* PP_Cuts.hh  (PP cuts file used for BurstHF)
				* BKG (background configuration files)
				  - mkpp.csh (post-production script used for BKG BurstHF)
			- LH
				* SIM
				  - mkpp.csh (post-production script used for SIM BurstLF)
				  - Standard_InjBuiltin (Standard MDC set with built-in injections)
				  - Standard_InjFrames  (Standard MDC set with injections from frame files)
		* BurstLD (burst allsky long duration)
			- LH/LHV
				* PP_Cuts.hh  (PP cuts file used for BurstLD)
				* BKG (background configuration files)
			- LH
				* SIM
				  - GRBplateau*
				  - ISCOchirp*
				  - Isp*
				  - NCSACAM*
				  - PT*
				  - barmodes*
				  - maXgnetar*
				  - msmagnetar*
				  - quad*
				  - wnb*	 

		* BBH (allsky stellar mass bbh)
			- LH
				* PP_Cuts.hh  (PP cuts file used for BBH)
				* BKG (background configuration files)
				* SIM
				  - BBH_BROAD_ISOTROPIC
		
		* IMBHB (allsky imbhb)
			- LH
				* PP_Cuts.hh  (PP cuts file used for IMBHB)
				* SIM
				  - NR-MIX

	* ONLINE (low latency searches)
		* BurstLF
			- LH
			- LHV
		* BBH
			- LH
			- LHV
		* IMBHB
			- LH
			- LHV


