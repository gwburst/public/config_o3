#include "CWB_Plugin.h"

void CWB_PluginConfig() {

  network** net;
  CWB_PLUGIN_IMPORT(network**,net);

  int* gIFOID;
  CWB_PLUGIN_IMPORT(int*,gIFOID);

  int* gIFACTOR;
  CWB_PLUGIN_IMPORT(int*,gIFACTOR);

  CWB::mdc* MDC;
  CWB_PLUGIN_IMPORT(CWB::mdc*,MDC);

  CWB::config** cfg;
  CWB_PLUGIN_IMPORT(CWB::config**,cfg);


  cout << "-----> CWB_Plugin_Config_Standard_InjBuiltin.C" << endl;

  int seed = (*net)->nRun;  // WARNING : seed must be the same for each detector within the same job


  waveform wf;
  vector<mdcpar> par; 


  par.resize(1);

  double F_GA[4]   = {0.0001,0.001,0.0025,0.004};
  for(int n=0;n<4;n++) {
    par[0].name="duration"; par[0].value=F_GA[n];
    MDC->AddWaveform(MDC_GA, par);
  }

  par.resize(2);

  double F_Q3[3] = {70,235,849};
  for(int n=0;n<3;n++) {
    par[0].name="frequency"; par[0].value=F_Q3[n];
    par[1].name="Q"; par[1].value=3.;
    MDC->AddWaveform(MDC_SGE, par);
  }

  double F_Q9a[4] = {70,100,235,361};
  for(int n=0;n<4;n++) {
    par[0].name="frequency"; par[0].value=F_Q9a[n];
    par[1].name="Q"; par[1].value=9.;
    MDC->AddWaveform(MDC_SG, par);
  }

  double F_Q9b[3] = {153,554,849};
  for(int n=0;n<3;n++) {
    par[0].name="frequency"; par[0].value=F_Q9b[n];
    par[1].name="Q"; par[1].value=9.;
    MDC->AddWaveform(MDC_SGE, par);
  }

  double F_Q100[3] = {70,235,849};
  for(int n=0;n<3;n++) {
    par[0].name="frequency"; par[0].value=F_Q100[n];
    par[1].name="Q"; par[1].value=100.;
    MDC->AddWaveform(MDC_SGE, par);
  }

  par.resize(6);

  double F_WNB[3] = {150,300,700};
  double B_WNB[3] = {100,100,100};
  double D_WNB[3] = {0.1,0.1,0.1};
  for(int n=0;n<3;n++) {
    par[0].name="frequency"; par[0].value=F_WNB[n];
    par[1].name="bandwidth"; par[1].value=B_WNB[n];
    par[2].name="duration";  par[2].value=D_WNB[n];
    for(int m=0;m<30;m++) {
      par[3].name="pseed";     par[3].value=seed+100000+n*100+m;
      par[4].name="xseed";     par[4].value=seed+100001+n*100+m;
      par[5].name="mode";      par[5].value=1;  // simmetric
      MDC->AddWaveform(MDC_WNB, par);
    }
  }

  MDC->Print();

  // --------------------------------------------------------
  // define injection parameters
  // --------------------------------------------------------
  MDC->SetInjHrss(5e-23);
  MDC->SetInjRate(1./300.);
  MDC->SetInjJitter(10.0);

  // --------------------------------------------------------
  // define sky distribution
  // --------------------------------------------------------
  par.resize(3);
  par[0].name="entries"; par[0].value=100000;
  par[1].name="rho_min"; par[1].value=0;
  par[2].name="rho_max"; par[2].value=0;
  MDC->SetSkyDistribution(MDC_RANDOM,par,seed);
}
