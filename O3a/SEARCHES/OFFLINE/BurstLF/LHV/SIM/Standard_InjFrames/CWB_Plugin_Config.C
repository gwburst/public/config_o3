#include "CWB_Plugin.h"

void CWB_PluginConfig() {

  int* gIFACTOR;
  CWB_PLUGIN_IMPORT(int*,gIFACTOR);

  CWB::config** cfg;
  CWB_PLUGIN_IMPORT(CWB::config**,cfg);

  // Import MDC frame files & log from plugin config
  std::vector<TString>* mdcPar;
  CWB_PLUGIN_IMPORT(std::vector<TString>*, mdcPar)
  // Import mdcType
  std::vector<std::string>* mdcType;
  CWB_PLUGIN_IMPORT(std::vector<std::string>*, mdcType)
  // Import channelName
  std::vector<std::string>* channelName;
  CWB_PLUGIN_IMPORT(std::vector<std::string>*, channelName)


  cout << "-----> CWB_Plugin_Config_Standard_InjFrames.C -> " << " gIFACTOR " << (*gIFACTOR) << endl;

  #define nMDC 19
  #define MDC_DIR "/home/daniel.williams/data/mdc/O2/frames/"

  if((*cfg)->nfactor > nMDC) {
    cout << "CWB_Plugin_Config_MDC_FRAME.C - Error : number of nfactor must be < " << nMDC << endl;
    gSystem->Exit(1);
  }

  // --------------------------------------------------------
  // define the mdcType 
  // --------------------------------------------------------

  mdcType->resize(nMDC);

  (*mdcType)[0]  = "ga_0d100";
  (*mdcType)[1]  = "ga_1d000";
  (*mdcType)[2]  = "ga_2d500";
  (*mdcType)[3]  = "ga_4d000";
  
  (*mdcType)[4]  = "sg_f70_q3_elliptical";
  (*mdcType)[5]  = "sg_f235_q3_elliptical";
  (*mdcType)[6]  = "sg_f849_q3_elliptical";

  (*mdcType)[7]  = "sg_f70_q9_linear";
  (*mdcType)[8]  = "sg_f100_q9_linear";
  (*mdcType)[9]  = "sg_f153_q9_elliptical";
  (*mdcType)[10] = "sg_f235_q9_linear";
  (*mdcType)[11] = "sg_f361_q9_linear";
  (*mdcType)[12] = "sg_f554_q9_elliptical";
  (*mdcType)[13] = "sg_f849_q9_elliptical";

  (*mdcType)[14] = "sg_f70_q100_elliptical";
  (*mdcType)[15] = "sg_f235_q100_elliptical";
  (*mdcType)[16] = "sg_f849_q100_elliptical";

  (*mdcType)[17] = "wnb_150d0b100d0tau0d1";
  (*mdcType)[18] = "wnb_300d0b100d0tau0d1";

  // we define xmdcType because log files uses a different format 
  std::vector<std::string> xmdcType(19);
  xmdcType = (*mdcType);
  xmdcType[0]  = "ga_d0100";
  xmdcType[1]  = "ga_d1000";
  xmdcType[2]  = "ga_d2500";
  xmdcType[3]  = "ga_d4000";

  xmdcType[17]  = "wnb150b100tau0p1";
  xmdcType[18]  = "wnb300b100tau0p1";

  // --------------------------------------------------------
  // define the mdcPar 
  // --------------------------------------------------------
  mdcPar->resize(2);
  channelName->resize(2);

  char injectionList[1024];
  char frFile[1024];

  TString type  = (*mdcType)[(*gIFACTOR)-1].c_str();
  TString xtype = xmdcType[(*gIFACTOR)-1].c_str();

  if((*gIFACTOR)<=4) {
    sprintf(injectionList,"%s/ga/%s/input/LAL%s_gps_sorted.log",MDC_DIR,type.Data(),xtype.Data());
  }
  if((*gIFACTOR)>4 && (*gIFACTOR)<=17) {
    sprintf(injectionList,"%s/sg/%s/input/LAL%s_gps_sorted.log",MDC_DIR,type.Data(),xtype.Data());
  }
  if((*gIFACTOR)>17) {
    sprintf(injectionList,"%s/wn/%s/input/LAL%s_gps_sorted.log",MDC_DIR,type.Data(),xtype.Data());
  }
  sprintf(frFile,"input/%s.frames",type.Data());

  (*mdcPar)[0] = injectionList;
  (*mdcPar)[1] = frFile;

  (*channelName)[0] = "L1:Science";
  (*channelName)[1] = "H1:Science";
/*
  if((*gIFACTOR)==2) {	// ga_1d000 only up job 1273
    (*channelName)[0] = "L1:SCIENCE";
    (*channelName)[1] = "H1:SCIENCE";
  }
*/
  if((*gIFACTOR)>17) {	// WNB
    (*channelName)[0] = "L1:SCIENCE";
    (*channelName)[1] = "H1:SCIENCE";
  } 

  // we define wmdcType because name have different format 
  if((*gIFACTOR)<=4) {
    (*mdcType)[0]  = "GA0d100";
    (*mdcType)[1]  = "GA1d000";
    (*mdcType)[2]  = "GA2d500";
    (*mdcType)[3]  = "GA4d000";
  }
}
