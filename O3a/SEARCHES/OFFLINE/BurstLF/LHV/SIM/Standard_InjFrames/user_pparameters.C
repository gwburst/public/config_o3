#define RUN_LABEL "BurstLF O3 #CWB_CALIB_VER K#CWB_CHUNK_NUMBER (Standard_InjFrames)"
//#define CAT2_VETO
//#define CAT3_VETO
#define HVETO_VETO

{
  #include <O3/SEARCHES/OFFLINE/BurstLF/LHV/PP_Cuts.hh>
  #include <O3/CHUNKS/Chunks_Cuts.hh>

  // read far vs rho background files from config/far_rho_bin1.lst & config/far_rho_bin2.lst (must be defined by the user)
  TString far_bin1_cut_file[1024];
  vector<TString> xfile_bin1 = CWB::Toolbox::readFileList("config/far_rho_bin1.lst");
  for(int n=0;n<xfile_bin1.size();n++) far_bin1_cut_file[n]=xfile_bin1[n];
  TString far_bin2_cut_file[1024];
  vector<TString> xfile_bin2 = CWB::Toolbox::readFileList("config/far_rho_bin2.lst");
  for(int n=0;n<xfile_bin2.size();n++) far_bin2_cut_file[n]=xfile_bin2[n];


  T_cor      = 0.0;       // cc cut
  T_cut      = 0.0;       // rho high frequency cut
  T_out	     = 0.0;
  hours      = 1.;        // bin size in hours for rate vs time plot

  T_ifar     = 4.;        // ifar cut yrs
  //T_ifar     = 100.;        // ifar cut yrs

  pp_irho    = 0;
  pp_inetcc  = 0;
  pp_rho_min = 5.0;
  pp_rho_max = 10;
  pp_drho    = 0.1;

  pp_max_nloudest_list = 20; 

//  pp_jet_benckmark = -1;
//  pp_mem_benckmark = -1;


  // ----------------------------------------------------------
  // VETO cuts
  // dq file list
  // {ifo, dqcat_file, dqcat[0/1/2], shift[sec], inverse[false/true], 4columns[true/false]}
  // ----------------------------------------------------------
  const int nvdqf=2;
  dqfile vdqf[nvdqf] = {
         {"L1" ,"",     CWB_HVETO, 0., false,   false},
         {"H1" ,"",     CWB_HVETO, 0., false,   false}
  };

  sprintf(vdqf[0].file, "%s/O3/DATA/%s/HVETO/L1/HVETO_L1_O3A_SEGMENTS_MERGED.txt",cwb_config_env,calibVer.Data());
  sprintf(vdqf[1].file, "%s/O3/DATA/%s/HVETO/H1/HVETO_H1_O3A_SEGMENTS_MERGED.txt",cwb_config_env,calibVer.Data());

}
