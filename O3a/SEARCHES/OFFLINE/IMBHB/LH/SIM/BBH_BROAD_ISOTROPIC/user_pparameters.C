
#define nRHO 300

  // -----------------------------------------------------------
  // CBC
  // -----------------------------------------------------------
//#define MIN_plot_mass1 0.0
//#define MAX_plot_mass1 150
//#define MIN_plot_mass2 0.0
//#define MAX_plot_mass2 100

//#define MAX_EFFECTIVE_RADIUS  2000.
//#define MASS_BIN 15.0
//#define MIN_MASS 0.0
//#define MAX_MASS 150

//#define MINCHI -1
// #define MAXCHI 1
//#define CHI_BIN 0.666666

//#define TOLERANCE 0.01
#define RUN_LABEL "O1: BBH rates from Broad (isotropic) distribution (SEOBNRv3_opt_rk4pseudoFourPN)"

//#define BKG_NTUPLE "True"
//#define PLOT_CHIRP
//#define PLOT_MASS_RATIO
//#define VOL_Mtotq  ///Volume calculated not correcting to make it uniform in M1 and M2
//#define FIXMINDISTANCE 0
//#define FIXMAXDISTANCE 2600000
//#define FIXMAXDISTANCE 5000000

//#define FIXMINRATIO 0.1
//#define FIXMAXRATIO 8.0
//#define FIXMINTOT 10
//#define FIXMAXTOT 150

//#define FIXMINRATIO 0.9
//#define FIXMAXRATIO 1.1
//#define FIXMINTOT 298
//#define FIXMAXTOT 302
//#define FIXMINTOT MIN_plot_mass1+MIN_plot_mass2
//#define FIXMAXTOT MAX_plot_mass1+MAX_plot_mass2
//#define WRITE_ASCII

//#define LIVE_ZERO  9910080.0 //WARNING: full O3 segments, i.e. 114.70 days!
#define REDSHIFT

//#define CAT2_VETO
//#define CAT3_VETO
#define HVETO_VETO
//#define FAR_BIN1_FILE_NAME "report/postprod/M1.V_hvetoLH.C_bin1_cut.R_rMRA_hveto_i0cc00_i1rho0_freq16_1024/data/far_rho.txt"

{

  #include <O3/SEARCHES/OFFLINE/BBH/LH/PP_Cuts.hh>
  #include <O3/CHUNKS/Chunks_Cuts.hh>

  #define nCHUNKS	21

  // ---------------------------------------------------------------
  // All numbers, files etc. related to the the MonteCarlo....
  // ---------------------------------------------------------------
 
  user_pp_label = "";
  //INCLUDE_INTERNAL_VOLUME = 0;
  //TRIALS = 1;

  // fiducial space-time volume
  double vt[nCHUNKS] = {
			    2.9407697637,
			    2.41406473139,
			    1.62583601985,
			    1.18051423039,
			    1.0424370431,
			    1.06986959687,
			    1.30076025773,
			    1.08861517527,
			    1.14119423666,
			    1.07535610762,
			    1.34648118067,
			    1.08952959373,
			    2.04098200018,
			    3.09713532016,
			    1.49233092486,
			    1.96142759426,
			    1.68435880122,
			    1.09730215063,
			    1.04792355385,
			    1.04838076308,
			    0.652437570397
    			};

  // number of events which do not pass the snr threshold used to generate the XML files
  int rejected[nCHUNKS] = {
			    143738,
			    88514,
			    62030,
			    55360,
			    57341,
			    72028,
			    57347,
			    59007,
			    55698,
			    72699,
			    59476,
			    106480,
			    144867,
			    71256,
			    90652,
			    109326,
			    70916,
			    166145,
			    66925,
			    69208,
			    43158
  			};

  // number of events which pass the snr threshold used to generate the XML files
  // events in the XML files
  int accept[nCHUNKS] = {
			    15840,
			    10668,
			    7746,
			    6840,
			    7020,
			    8535,
			    7143,
			    7488,
			    7056,
			    8835,
			    7149,
			    13392,
			    20322,
			    9792,
			    12870,
			    11052,
			    7200,
			    19296,
			    6876,
			    6879,
			    4281
  			};


  // chunk's start, stop times used for montecarlo
  int SEGS2[nCHUNKS][3] = {
			    {2,  1164556817, 1166486417},
			    {3,  1167523218, 1169107218},
			    {4,  1169107218, 1170174018},
			    {5,  1170174018, 1170948618},
			    {6,  1170948618, 1171632618},
			    {7,  1171632618, 1172334618},
			    {8,  1172334618, 1173188118},
			    {9,  1173188118, 1173902418},
			    {10, 1173902418, 1174651218},
			    {11, 1174651218, 1175356818},
			    {12, 1175356818, 1176240318},
			    {13, 1176240318, 1176955218},
			    {14, 1176955218, 1178294418},
			    {15, 1179813618, 1181845818},
			    {16, 1181845818, 1182825018},
			    {17, 1182825018, 1184112018},
			    {18, 1184112018, 1185217218},
			    {19, 1185217218, 1185937218},
			    {20, 1185937218, 1186624818},
			    {21, 1186624818, 1187312718},
			    {22, 1187312718, 1187740818}
  			};


  // compute fiducial space-time volume
  VT = 0.0;
  int NINJ[nCHUNKS];
  double acceptance[nCHUNKS];
  for(int n=2;n<=nCHUNKS+1;n++) {
     acceptance[n-2] = double(accept[n-2])/double(accept[n-2]+rejected[n-2]);
     VT += vt[n-2]*acceptance[n-2];
     NINJ[0] += accept[n-2];
  }

  // read far vs rho background files from config/far_rho_bin1.lst  (must be defined by the user)
  TString far_bin1_cut_file[1024];
  vector<TString> xfile_bin1 = CWB::Toolbox::readFileList("config/far_rho_bin1.lst");
  for(int n=0;n<xfile_bin1.size();n++) far_bin1_cut_file[n]=xfile_bin1[n];

  // read far vs rho background files 
  TString far_bin1_cut_file[1024];
  vector<TString> xfile = CWB::Toolbox::readFileList("config/far_rho.lst");
  for(int n=0;n<xfile.size();n++) far_bin1_cut_file[n+2]=xfile[n];
  if(xfile.size()!=nCHUNKS) {
     cout << endl;
     for(int n=0;n<xfile.size();n++) cout << n+1 << " " << far_bin1_cut_file[n+2] << endl;
     cout << endl << "user_pparameters.C error: config/far_rho.lst files' list size must be = "
          << nCHUNKS << endl << endl;
     exit(1);
  }

  // compute total MonteCarlo Time
  TMC = 0;
  TString XML[nCHUNKS];
  for(int i=0; i<nCHUNKS; i++){
     TMC += SEGS2[i][2]-SEGS2[i][1];
     XML[i].Form("bbh_broad_isotropic-%d-%d.xml", SEGS2[i][1], SEGS2[i][2]);
     //cout <<"XML[" << i <<"]=" <<XML[i]<<endl;
  }


  // post-production threshols

  T_cor      = 0.0;       // cc cut
  T_cut      = 0.0;       // rho high frequency cut
  T_out	     = 0.0;
  hours      = 1.;        // bin size in hours for rate vs time plot
  T_ifar     = 1;
  T_win      = 0.2;

  pp_irho    = 1;
  pp_inetcc  = 0;
  pp_rho_min = 4.5;
  pp_rho_max = 10;
  pp_drho    = 0.1;

  pp_max_nloudest_list = 20; 

//  pp_jet_benckmark = -1;
//  pp_mem_benckmark = -1;

  // ----------------------------------------------------------
  // VETO cuts
  // dq file list
  // {ifo, dqcat_file, dqcat[0/1/2], shift[sec], inverse[false/true], 4columns[true/false]}
  // ----------------------------------------------------------
  const int nvdqf=2;
  dqfile vdqf[2] = {
         {"L1" ,"",     CWB_HVETO, 0., false,   false},
         {"H1" ,"",     CWB_HVETO, 0., false,   false}
  };

  sprintf(vdqf[0].file, "%s/O3/DATA/%s/HVETO/L1/HVETO_L1_O3A_SEGMENTS_MERGED.txt",cwb_config_env,calibVer.Data());
  sprintf(vdqf[1].file, "%s/O3/DATA/%s/HVETO/H1/HVETO_H1_O3A_SEGMENTS_MERGED.txt",cwb_config_env,calibVer.Data());
}
