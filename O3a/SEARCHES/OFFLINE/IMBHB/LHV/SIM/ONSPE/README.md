# PE configuration setup used for ONSOURCE PE simulations

Example:

cd /home/vedovato/O3/SEARCHES/OFFLINE/BBH/LHV/EVT/S190521g

// ON SOURCE

cwb_mkchunk --run O3 --chunk 06 --cal C00 --net LHV --search BBH --type SIM/ONSPE --tag S190521g_EXP2_maxl_dev1
ln -sf ../../PE/EXP2/freffix/posterior_samples_S190521g_EXP2_maxl.xml O3_K06_C00_LHV_BBH_SIM_ONSPE_S190521g_EXP2_maxl_dev1/config/injections.xml
cd O3_K06_C00_LHV_BBH_SIM_ONSPE_S190521g_EXP2_maxl_dev1
cwb_inet @ 1242442967.44 ced
mv data/wave_1246526400_1200_O3_K06_C00_LHV_BBH_SIM_ONSPE_S190521g_EXP2_maxl_dev1_1_job588.root output/
mv data/ced_1246526400_1200_O3_K06_C00_LHV_BBH_SIM_ONSPE_S190521g_EXP2_maxl_dev1_1_job588/ report/ced/



