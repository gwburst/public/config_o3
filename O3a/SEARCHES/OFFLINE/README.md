# O3 cWB Searches #

This directory contains the necessary cWB configuration files to
run cWB searches on O3 data.

* SEARCHES

	* BurstLF (burst allsky low frequency)
	* BurstHF (burst allsky high frequency)
	* BurstLD (burst allsky long duration)

       	* BBH (allsky stellar mass bbh)
       	* IMBHB (allsky imbhb)

=======================================================================

Here are listed some examples showing how to use the search configuration files 
to process background/simulations searches.

## INITIALIZATION

* define $CWB_CONFIG in watenv script, for example:
	* setenv CWB_CONFIG "~waveburst/git/cWB-config/setup.csh"   # Location of CWB Configuration files, DQ files, FRAMES files
* source $CWB_CONFIG/setup.csh

## How to process the background BurstLF for network LH

In O3 there are XX chunks: K01,...,KXX. Each chunk must be processed independently.

Example: obs-run=O3, chunk-number=K04, calibration-version=C00, network=LH, search=BurstLF, search-type=BKG, user-tag=run1 (tag is an alphanumeric string defined by the user) 


1) create chunk04 working directory 

```bash
cwb_mkchunk --run O3 --chunk 04 --cal C00 --net LH --search BurstLF --type BKG --tag run1
```

the following directory is created with all necessary plugins and configuration files
 
O3_04_C00_LH_BurstLF_BKG_run1


2) post production M1 (if M1 option is not declared the default is M1)

```bash
cwb_ppchunk --run O3 --chunk 04 --cal C00 --net LH --search BurstLF --type BKG --tag run1 --mlabel M1
```

the following actions are performed: cwb_merge, cwb_veto, cwb_setcuts, cwb_report, cwb_setifar

for each bin cut the file far_rho.txt is produced in its specific report data directory.

* bin1_cut      "report/postprod/M1.V_hvetoLH.C_bin1_cut.R_rMRA_hveto_i0cc00_i0rho0_freq16_1024/data/far_rho.txt"
* bin2_cut      "report/postprod/M1.V_hvetoLH.C_bin2_cut.R_rMRA_hveto_i0cc00_i0rho0_freq16_1024/data/far_rho.txt"


## How to process the simulation for LH BurstLF search with Standard MDC set with built-in injections


Example: obs-run=O3, chunk-number=K99(full O3 run), calibration-version=C00, network=LH, search=BurstLF, search-type=SIM/Standard_InjBuiltin, user-tag=run1 (tag is an alphanumeric string defined by the user) 

Note; chunk K99 is the union of all O3 chunks

1) create working directory 

```bash
cwb_mkchunk --run O3 --chunk 99 --cal C00 --net LH --search BurstLF --type SIM/Standard_InjBuiltin --tag run1
```

the following directory is created with all necessary plugins and configuration files

O3_K99_C00_LH_BurstLF_SIM_Standard_InjBuiltin_run1
 
2) post production M1 (if M1 option is not declared the default is M1)

```bash
cwb_ppchunk --run O3 --chunk 99 --cal C00 --net LH --search BurstLF --type SIM/Standard_InjBuiltin --tag run1
```
or alternatively:

```bash
source $CWB_CONFIG/O3/SEARCHES/OFFLINE/BurstLF/LH/SIM/Standard_InjBuiltin/mkpp.csh M1 2 22
```

the following actions are performed: cwb_merge, cwb_veto, cwb_setifar, cwb_report

Note: cwb_setifar command associates to each event the corresponding IFAR based on the values computed for the different background sub-sets. 
The final report of the efficiency (cwb_report) is based on the IFAR threshold. 

Note: cwb_setifar command uses the far_rho.txt files produced by background of each chunk. 
Before to apply the cwb_ppchunk command the user must update the user_pparameters.C file 
defining the list of far_rho.txt paths produced by background of each chunk

Note: if the far_rho.txt is the entire O3 background the cwb_ppchunk command corrensponds to the following actions:
* cwb_merge M1
* cwb_setveto M1
* cwb_setifar M1 '--tsel bin1_cut  --label bin1_cut --xfile The_Path_Of_far_rho.txt --mode exclusive'
* cwb_setifar M1.S_bin1_cut '--tsel bin2_cut  --label bin2_cut --xfile The_Path_Of_far_rho.txt --mode exclusive'
* cwb_report M1 CWB_MERGE_LABEL.V_hvetoLH.S_bin1_cut.S_bin2_cut 

## How to produce a super-report of a sub-set background chunks for BurstLF and network LH

Consider the chunks K02,K03,K04

clone the O3_K02_C00_LH_BurstLF_BKG_run1 & its merged data to O3_K99_C00_LH_BurstLF_BKG_K02toK04_run1
```bash
cwb_clchunk --idir O3_K03_C00_LH_BurstLF_BKG_K02toK04_run1 --run O3 --chunk 99 --cal C00 --net LH --search BurstLF --type BKG --tag K02toK04_run1
```
add the O3_K03_C00_LH_BurstLF_BKG_run1 merged data to O3_K99_C00_LH_BurstLF_BKG_K02toK04_run1
```bash
cwb_clchunk --idir O3_K04_C00_LH_BurstLF_BKG_K02toK04_run1 --run O3 --chunk 99 --cal C00 --net LH --search BurstLF --type BKG --tag K02toK04_run1
```
add the O3_K04_C00_LH_BurstLF_BKG_run1 merged data to O3_K99_C00_LH_BurstLF_BKG_K02toK04_run1
```bash
cwb_clchunk --idir O3_K05_C00_LH_BurstLF_BKG_K02toK04_run1 --run O3 --chunk 99 --cal C00 --net LH --search BurstLF --type BKG --tag K02toK04_run1
```
post production of O3_K99_C00_LH_BurstLF_BKG_K02toK04_run1
```bash
cwb_ppchunk --run O3 --chunk 99 --cal C00 --net LH --search BurstLF --type BKG --tag K03_K04_K05_run1
```

## How to use BurstLF background for the IMBHB search

The IMBHB search uses the backgroud produced in the BurstLF search, only the post-production is different.

The results of background must be cloned in a new working directory including the specific IMBHB pp configuration files.

For chunk=03 the command is:

```bash
cwb_clchunk --run O3 --chunk 03 --cal C00 --net LH --search IMBHB --type BKG --tag run1
```

a new working dir O3_K03_C00_LH_IMBHB_BKG_run1 is created and the specific IMBHB pp configuration files are copied into.

then the post production is performed as:

```bash
cwb_ppchunk --run O3 --chunk 03 --cal C00 --net LH --search IMBHB --type BKG --tag run1
```

## How to open box for a chunk with the IMBHB search

```bash
cwb_obchunk --run O3 --bbh true --chunk 06 --wlabel O3_K06_C00_LH_IMBHB_BKG_dbg1 --merge 1 --mlabel C_bin1_cut --rlabel R_rMRA_i0cc00_i1rho0_freq16_512 --lag 0 --slag 0 --opt mkdir
```
```bash
cwb_obchunk --run O3 --bbh true --chunk 06 --wlabel O3_K06_C00_LH_IMBHB_BKG_dbg1 --merge 1 --mlabel C_bin1_cut --rlabel R_rMRA_i0cc00_i1rho0_freq16_512 --lag 0 --slag 0 --opt report
```
```bash
cwb_obchunk --run O3 --bbh true --chunk 06 --wlabel O3_K06_C00_LH_IMBHB_BKG_dbg1 --merge 1 --mlabel C_bin1_cut --rlabel R_rMRA_i0cc00_i1rho0_freq16_512 --lag 0 --slag 0 --opt plot
```
```bash
cwb_obchunk --run O3 --bbh true --chunk 06 --wlabel O3_K06_C00_LH_IMBHB_BKG_dbg1 --merge 1 --mlabel C_bin1_cut --rlabel R_rMRA_i0cc00_i1rho0_freq16_512 --lag 0 --slag 0 --opt html
```

## How to combine the IMBHB search with the BBH search

```bash
cwb_combine_cbc M1.C_bin1_cut.S_ifar '--search BBH --wdir (path)/O3_K06_C00_LH_BBH_BKG_dbg1 --mlabel M1.C_bin1_cut.S_ifar --fthr 80 --lag 0 --slag 0'
```

=======================================================================

## More Examples

```

// ----------------------------------------------------------------------------
// BBH BKG              
// ----------------------------------------------------------------------------

-> make working directory -> O3_K03_C00_LH_BBH_BKG_tst1
cwb_mkchunk --run O3 --chunk 03 --cal C00 --net LH --search BBH --type BKG --tag tst1                   // make dir & create dag
cwb_mkchunk --run O3 --chunk 03 --cal C00 --net LH --search BBH --type BKG --tag tst1 --opt create      // make dir & create dag
cwb_mkchunk --run O3 --chunk 03 --cal C00 --net LH --search BBH --type BKG --tag tst1 --opt submit      // make dir & create dag & submit dag

-> test in interactive mode job 95
cd O3_K03_C00_LH_BBH_BKG_tst1
cwb_inet 95

-> make post-production
cwb_ppchunk --run O3 --chunk 03 --cal C00 --net LH --search BBH --type BKG --tag cloned_tst1 

-> make post-production with merge label M2
cwb_ppchunk --run O3 --chunk 03 --cal C00 --net LH --search BBH --type BKG --tag cloned_tst1 --mlabel M2 // M2 must be used only if M1 is already present in the merge directory

// ----------------------------------------------------------------------------
// BBH BKG cloned             
// ----------------------------------------------------------------------------

-> make working directory -> O3_K03_C00_LH_BBH_BKG_cloned_tst1
cwb_clchunk --run O3 --chunk 03 --cal C00 --net LH --search BBH --type BKG --tag cloned_tst1 --idir /home/vedovato/O3/OFFLINE/TESTS/TEST_eBBH_CHUNKS/O3_K03_C00_LH_eBBH_BKG_dbg1

-> make post-production
cwb_ppchunk --run O3 --chunk 03 --cal C00 --net LH --search BBH --type BKG --tag cloned_tst1 
cwb_ppchunk --run O3 --chunk 03 --cal C00 --net LH --search BBH --type BKG --tag cloned_tst1 --mlabel M5

-> only merge
cwb_ppchunk --run O3 --chunk 03 --cal C00 --net LH --search BBH --type BKG --tag cloned_tst1 --opt merge  --mlabel M6

// ----------------------------------------------------------------------------
// BBH BKG super-report K03,K04,K05       
// ----------------------------------------------------------------------------

-> make clone directory+data -> O3_K99_C00_LH_BBH_BKG_K03_K04_K05_tst1
cwb_clchunk --run O3 --chunk 99 --cal C00 --net LH --search BBH --type BKG --tag K03_K04_K05_tst1 --idir /home/vedovato/O3/OFFLINE/TESTS/TEST_eBBH_CHUNKS/O3_K03_C00_LH_eBBH_BKG_dbg1
cwb_clchunk --run O3 --chunk 99 --cal C00 --net LH --search BBH --type BKG --tag K03_K04_K05_tst1 --idir /home/vedovato/O3/OFFLINE/TESTS/TEST_eBBH_CHUNKS/O3_K04_C00_LH_eBBH_BKG_dbg1
cwb_clchunk --run O3 --chunk 99 --cal C00 --net LH --search BBH --type BKG --tag K03_K04_K05_tst1 --idir /home/vedovato/O3/OFFLINE/TESTS/TEST_eBBH_CHUNKS/O3_K05_C00_LH_eBBH_BKG_dbg1

-> make post-production
cwb_ppchunk --run O3 --chunk 99 --cal C00 --net LH --search BBH --type BKG --tag K03_K04_K05_tst1

// ----------------------------------------------------------------------------
// BurstLF BKG                        
// ----------------------------------------------------------------------------

-> make working directory -> O3_K03_C00_LH_BurstLF_BKG_tst1
cwb_mkchunk --run O3 --chunk 03 --cal C00 --net LH --search BurstLF --type BKG --tag tst1               // make dir & create dag

-> test in interactive mode job 95
cd O3_K03_C00_LH_BurstLF_BKG_tst1
cwb_inet 260

-> make post-production
cwb_ppchunk --run O3 --chunk 03 --cal C00 --net LH --search BurstLF --type BKG --tag tst1 

// ----------------------------------------------------------------------------
// BurstLF BKG cloned           
// ----------------------------------------------------------------------------

-> make clone directory+data -> O3_K03_C00_LH_BurstLF_BKG_cloned_tst1
cwb_clchunk --run O3 --chunk 03 --cal C00 --net LH --search BurstLF --type BKG --tag cloned_tst1 --idir /home/vedovato/O3/OFFLINE/TESTS/TEST_BurstLF_CHUNKS/O3_K03_C00_LH_BurstLF_BKG_dbg3

-> make post-production
cwb_ppchunk --run O3 --chunk 03 --cal C00 --net LH --search BurstLF --type BKG --tag cloned_tst1 

// ----------------------------------------------------------------------------
// BurstLF BKG cloned to IMBHB        
// ----------------------------------------------------------------------------

-> make clone directory+data -> O3_K03_C00_LH_IMBHB_BKG_cloned_tst1
cwb_clchunk --run O3 --chunk 03 --cal C00 --net LH --search IMBHB --type BKG --tag cloned_tst1 --idir /home/vedovato/O3/OFFLINE/TESTS/TEST_BurstLF_CHUNKS/O3_K03_C00_LH_BurstLF_BKG_dbg3

-> make post-production
cwb_ppchunk --run O3 --chunk 03 --cal C00 --net LH --search IMBHB --type BKG --tag cloned_tst1 

// ----------------------------------------------------------------------------
// BurstLF BKG super-report K03,K04,K05  
// ----------------------------------------------------------------------------

-> make clone directory+data -> O3_K99_C00_LH_BurstLF_BKG_K03_K04_K05_tst1
cwb_clchunk --run O3 --chunk 99 --cal C00 --net LH --search BurstLF --type BKG --tag K03_K04_K05_tst1 --idir /home/vedovato/O3/OFFLINE/TESTS/TEST_BurstLF_CHUNKS/O3_K03_C00_LH_BurstLF_BKG_dbg3
cwb_clchunk --run O3 --chunk 99 --cal C00 --net LH --search BurstLF --type BKG --tag K03_K04_K05_tst1 --idir /home/vedovato/O3/OFFLINE/TESTS/TEST_BurstLF_CHUNKS/O3_K04_C00_LH_BurstLF_BKG_dbg3
cwb_clchunk --run O3 --chunk 99 --cal C00 --net LH --search BurstLF --type BKG --tag K03_K04_K05_tst1 --idir /home/vedovato/O3/OFFLINE/TESTS/TEST_BurstLF_CHUNKS/O3_K05_C00_LH_BurstLF_BKG_dbg3

-> make post-production
cwb_ppchunk --run O3 --chunk 99 --cal C00 --net LH --search BurstLF --type BKG --tag K03_K04_K05_tst1

// ----------------------------------------------------------------------------
// BurstLF SIM/Standard_InjBuiltin
// ----------------------------------------------------------------------------

-> make working directory -> O3_K99_C00_LH_BurstLF_SIM_Standard_InjBuiltin_tst1
cwb_mkchunk --run O3 --chunk 99 --cal C00 --net LH --search BurstLF --type SIM/Standard_InjBuiltin --tag tst1           // make dir & create dag

-> execute job 2 in interactive mode
cd O3_K99_C00_LH_BurstLF_SIM_Standard_InjBuiltin_tst1
cwb_inet 2

-> make post-production
cwb_ppchunk --run O3 --chunk 99 --cal C00 --net LH --search BurstLF --type SIM/Standard_InjBuiltin --tag tst1 

// ----------------------------------------------------------------------------
// BurstLF SIM/Standard_InjBuiltin cloned
// ----------------------------------------------------------------------------

-> make clone directory+data -> O3_K99_C00_LH_BurstLF_SIM_Standard_InjBuiltin_cloned_tst1
cwb_clchunk --run O3 --chunk 99 --cal C00 --net LH --search BurstLF --type SIM/Standard_InjBuiltin --tag clone_tst1 --idir /home/vedovato/O3/OFFLINE/TESTS/TEST_BurstLF_CHUNKS/O3_KALL_C00_LH_BurstLF_SIM1_dbg3

-> make post-production
cwb_ppchunk --run O3 --chunk 99 --cal C00 --net LH --search BurstLF --type SIM/Standard_InjBuiltin --tag clone_tst1 

-> post-production K02 + K03
cd O3_K99_C00_LH_BurstLF_SIM_Standard_InjBuiltin_cloned_tst1
source $CWB_CONFIG/O3/SEARCHES/OFFLINE/BurstLF/LH/SIM/Standard_InjBuiltin/mkpp.csh M1 2 3

// ----------------------------------------------------------------------------
// BurstHF BKG                        
// ----------------------------------------------------------------------------

-> make working directory -> O3_K03_C00_LH_BurstHF_BKG_tst1
cwb_mkchunk --run O3 --chunk 03 --cal C00 --net LH --search BurstHF --type BKG --tag tst1               // make dir & create dag

-> test in interactive mode job 95
cd O3_K03_C00_LH_BurstHF_BKG_tst1
cwb_inet 260

-> make post-production
cwb_ppchunk --run O3 --chunk 03 --cal C00 --net LH --search BurstHF --type BKG --tag tst1 

// ----------------------------------------------------------------------------
// BurstHF SIM/Standard_InjBuiltin
// ----------------------------------------------------------------------------

-> make working directory -> O3_K99_C00_LH_BurstHF_SIM_Standard_InjBuiltin_tst1
cwb_mkchunk --run O3 --chunk 99 --cal C00 --net LH --search BurstHF --type SIM/Standard_InjBuiltin --tag tst1           // make dir & create dag

-> execute job 2 in interactive mode
cd O3_K99_C00_LH_BurstHF_SIM_Standard_InjBuiltin_tst1
cwb_inet 2

-> make post-production
cwb_ppchunk --run O3 --chunk 99 --cal C00 --net LH --search BurstHF --type SIM/Standard_InjBuiltin --tag tst1 

// ----------------------------------------------------------------------------
// BurstLD BKG                 
// ----------------------------------------------------------------------------

-> make working directory -> O3_K03_C00_LH_BurstLD_BKG_tst1
cwb_mkchunk --run O3 --chunk 03 --cal C00 --net LH --search BurstLD --type BKG --tag tst1               // make dir & create dag

-> test in interactive mode job 95
cd O3_K03_C00_LH_BurstLD_BKG_tst1
cwb_inet 260

-> make post-production
cwb_ppchunk --run O3 --chunk 03 --cal C00 --net LH --search BurstLD --type BKG --tag tst1 


// ----------------------------------------------------------------------------
// BBH SIM/BBH_O1_ALLSKY
// ----------------------------------------------------------------------------

-> make working directory -> O3_K99_C00_LH_BBH_SIM_O1_ALLSKY_tst1
cwb_mkchunk --run O3 --chunk 99 --cal C00 --net LH --search BBH --type SIM/BBH_O1_ALLSKY --tag tst1

-> execute job 2 in interactive mode
cd O3_K99_C00_LH_BBH_SIM_O1_ALLSKY_tst1
cwb_inet 2

-> make post-production
cwb_ppchunk --run O3 --chunk 99 --cal C00 --net LH --search BBH --type SIM/BBH_O1_ALLSKY --tag tst1 



```


