# PE configuration setup used for ONSOURCE PE simulations

Example:

cd /home/vedovato/O3/SEARCHES/OFFLINE/BBH/LHV/EVT/S190707q

// ON SOURCE

cwb_mkchunk --run O3 --chunk 12 --cal C00 --net LHV --search BBH --type SIM/ONSPE --tag S190707q_EXP3_maxl_dev1
ln -sf ../../PE/EXP3/freffix/posterior_samples_S190707q_EXP3_maxl.xml O3_K12_C00_LHV_BBH_SIM_ONSPE_S190707q_EXP3_maxl_dev1/config/injections.xml
cd O3_K12_C00_LHV_BBH_SIM_ONSPE_S190707q_EXP3_maxl_dev1
cwb_inet @ 1246527224.18 ced
mv data/wave_1246526400_1200_O3_K12_C00_LHV_BBH_SIM_ONSPE_S190707q_EXP3_maxl_dev1_1_job467.root output/
mv data/ced_1246526400_1200_O3_K12_C00_LHV_BBH_SIM_ONSPE_S190707q_EXP3_maxl_dev1_1_job467/ report/ced/



