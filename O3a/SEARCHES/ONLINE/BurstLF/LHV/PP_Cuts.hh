// -------------------------------------------------------------------------------
// Definitions of the search bins used for the Offline Low Frequency Burst Search
// -------------------------------------------------------------------------------

// definition of the selection selection cuts
TCut norm_cut("norm_cut","norm>2.5");
TCut freq_cut("freq_cut","frequency[0]>24 && frequency[0]<2000");
TCut netcc_cut("netcc_cut","netcc[0]>0.8 && netcc[2]>0.8");
TCut qveto_cut("qveto_cut","Qveto[0]>0.3");
TCut lveto_cut("lveto_cut","!(bandwidth[0]<5 || (Lveto[1]<5 && Lveto[2]>0.8))");

// definition of the inclusive bins
TCut unmodeled_cut    = TCut("unmodeled_cut",(norm_cut+netcc_cut+freq_cut).GetTitle());
TCut constrained_cut  = TCut("constrained_cut",(norm_cut+netcc_cut+freq_cut+qveto_cut+lveto_cut).GetTitle());

//cout << "unmodeled_cut    : " << unmodeled_cut.GetTitle() << endl;
//cout << "constrained_cut  : " << constrained_cut.GetTitle() << endl;

// definition of the exclusive bins
TCut bin1_cut    = TCut("bin1_cut",(unmodeled_cut+!constrained_cut).GetTitle());
TCut bin2_cut    = TCut("bin2_cut",(constrained_cut).GetTitle());

//cout << "bin1_cut : " << bin1_cut.GetTitle() << endl;
//cout << "bin2_cut : " << bin2_cut.GetTitle() << endl;
