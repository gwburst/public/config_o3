#!/usr/bin/python

import commands, os, sys
import getpass

run="O3"
network="LHV"
search_type="BurstLF"
tag="ONLINE"

#general setup
user=getpass.getuser()
if (os.environ['SITE_CLUSTER']=="CASCINA"):
  cascina_user="cWB"
title="%s %s %s %s Allsky"%(run,network,search_type,tag)
accounting_group="ligo.prod.o3.burst.allsky.cwbonline"

label="%s_%s_%s_%s"%(run,network,search_type,tag)
online_dir="/home/%s/online/%s"%(user,label)
if (os.environ['SITE_CLUSTER']=="CASCINA"):
   online_dir="/data/procdata/burst/%s/online/%s"%(cascina_user,label)

ifos=["L1","H1","V1"]

channelname={}
DQ_channel={}
DQ_channel_rate={}
bitmask={}
inj_name=["BURST","CBC"]#,"STOCH","DETCHAR"]
inj_bitmask={}
veto_channel={}
veto_rate={}
cat2_rate={}
cat2_bitmask={}
cat2_channel={}
bitmask={}
for ifo in ifos[:-1]:
    channelname[ifo]="%s:GDS-CALIB_STRAIN_CLEAN"%(ifo)
    #channelname[ifo]="%s:GDS-CALIB_STRAIN_O2Replay"%(ifo) #O2replay
    DQ_channel[ifo]=["%s:GDS-CALIB_STATE_VECTOR"%(ifo),"%s:DMT-DQ_VECTOR"%(ifo)]
    DQ_channel_rate[ifo]=[16,16]
    bitmask[ifo]=[3,22]
    inj_bitmask[ifo]=[128,64]#,32,256]
    cat2_channel[ifo]=["%s:DMT-DQ_VECTOR_GATED"%(ifo)]
    cat2_rate[ifo]=[16]
    cat2_bitmask[ifo]=[8]
channelname["V1"]="V1:Hrec_hoft_16384Hz"
#channelname["V1"]="V1:Hrec_hoft_16384Hz_O2Replay"  #O2replay
DQ_channel["V1"]="V1:DQ_ANALYSIS_STATE_VECTOR"
DQ_channel_rate["V1"]=1
bitmask["V1"]=1027
inj_bitmask["V1"]=[128,64]#,32,256]
veto_channel["V1"]=["V1:DQ_VETO_CWB"]
veto_rate["V1"]=[50]

#Job setup
run_offset=0
job_offset=8
job_timeout=90*60
#min_seg_duration=30
seg_duration=180
moving_step=60
look_back=5*60
debug=0
sleep=3
max_jobs=20

#General cWB parameters
levelR = 2
l_high   = 10
search="r"
optim="false"
cwb_par="""
  strcpy(analysis,"2G");

  nIFO = %i;
  cfg_search = '%s';
  optim=%s;

  //frequency
  fLow  = 16.;       // low frequency of the search
  fHigh = 2048.;      // high frequency of the search

  levelR = %i;
  l_low    = 4;       // low frequency resolution level
  l_high   = %i;      // high frequency resolution level

  strcpy(wdmXTalk,"wdmXTalk/OverlapCatalog16-1024.bin");

  healpix=7;
  nSky=196608;

  bpp   = 0.001;
  subnet = 0.5;
  subcut = 0.0;
  netRHO = 5.0;
  netCC = 0.5;
  Acore = 1.7;
  Tgap  = 0.2;
  Fgap  = 128.0;
  delta = 0.5;
  cfg_gamma = -1.0;
  LOUD = 300;

  pattern = 10;

  //simulation
  nfactor = 1;
  simulation = 0;"""%(len(ifos),search,optim,levelR,l_high)

#Background information
bkg_delay=5*60
bkg_nlags=500
bkg_job_duration=600
bkg_job_minimum=600
bkg_split=1
bkg_njobs=20
bkg_framesize=1
if (os.environ['SITE_CLUSTER']=="CASCINA"):
  bkg_framesize=10000
bkg_superlaglist="%s/O3/SEARCHES/ONLINE/LAGS/Unique_n21.txt"%os.environ['CWB_CONFIG']
bkg_laglist="%s/O3/SEARCHES/ONLINE/LAGS/Unique_n501.lags"%os.environ['CWB_CONFIG']

#Directories
run_dir=online_dir+"/RUN_cWB"
jobs_dir="JOBS"
seg_dir="SEGMENTS"
summaries_dir="SUMMARIES"
config_dir="%s/config"%(online_dir)
zerolag_par="%s/user_parameters.C"%(config_dir)
bkg_par="%s/user_parameters_bkg.C"%(config_dir)
pp_par="%s/user_pparameters.C"%(config_dir)
bkg_run_dir="%s/TIME_SHIFTS"%(online_dir)
postprod_dir="POSTPRODUCTION"
considered_segments_file="considered.txt"
processed_segments_file="processed.txt"
running_segments_file="running.txt"
missing_segments_file="missing.txt"
run_segments_file="run.txt"
job_segments_file="jobs.txt"

if (os.environ['SITE_CLUSTER']=="CIT"):
   frames_dir=["/dev/shm/kafka/L1","/dev/shm/kafka/H1","/dev/shm/kafka/V1"]
   bkg_dir=["/ifocache/llcache/kafka/L1/L-L1_llhoft-??????/L-L1_llhoft-","/ifocache/llcache/kafka/H1/H-H1_llhoft-??????/H-H1_llhoft-","/ifocache/llcache/kafka/V1/V-V1_llhoft-??????/V-V1_llhoft-"]
   log_path="/usr1/%s"%(user)
   web_dir="/home/%s/public_html/online/%s"%(user,label)
   web_link="https://ldas-jobs.ligo.caltech.edu/~%s/online/%s"%(user,label)
   accounting_group_user="marek.szczepanczyk"
   condor_requirements_file="/home/waveburst/online/HTCondor/cwb_template.sub"
if (os.environ['SITE_CLUSTER']=="CASCINA"):
   frames_dir=["/dev/shm/LowLatencyAnalysis/CwbL1In","/dev/shm/LowLatencyAnalysis/CwbH1In","/dev/shm/LowLatencyAnalysis/CwbV1In"]
   bkg_dir=["/data/prod/hrec/L1Online/L1Online-","/data/prod/hrec/H1Online/H1Online-","/data/prod/hrec/V1CITOnline/V-V1CITOnline-"]
   log_path="/local/user/%s"%(cascina_user)
   web_dir="/data/procdata/web/%s/online/%s"%(cascina_user,label)
   web_link="https://scientists.virgo-gw.eu/DataAnalysis/Burst/%s/online/%s"%(cascina_user,label)
   #accounting_group_user="marco.drago"
   condor_requirements_file="/virgoData/HTCondor/cwb_template.sub"

#Data Quality
apply_veto=False

#E-mails
emails=["marco.drago@ligo.org","marek.szczepanczyk@ligo.org","sergei.klimenko@ligo.org","gabriele.vedovato@ligo.org","claudia.lazzaro@ligo.org","imre.bartos@ligo.org","andrea.miani@ligo.org","giovanni.prodi@ligo.org","shubhanshu.tiwari@ligo.org"]
error_emails=["marco.drago@ligo.org","marek.szczepanczyk@ligo.org"]

#Library information
version="1"
version_wat="2G"
code_version="wat6.2.6"
hoft_version="C00"

#gracedb
#gracedb_client="https://gracedb-playground.ligo.org/api/"  #O2replay
gracedb_group="Burst"
gracedb_analysis="CWB"
gracedb_search="AllSky"

#pp threshold
id_rho=0
#th_rho_off=8.
th_far_off=3.17e-08
th_rho_lum=6.
th_rho_mail=6.
id_cc=0
th_cc=0.6

#Plugins
#pe_par="%s/config/user_parameters_pe.C"%(run_dir)
#pe_plugin="%s/tools/online/cWB_Plugin_PE_on.C"%(os.environ['HOME_WAT'])
prod_plugins=["%s/%s/SEARCHES/PLUGINS/CWB_Plugin_Gating_QLveto.C"%(os.environ['CWB_CONFIG'],run)]

#TCuts
Cuts_file="%s/%s/SEARCHES/ONLINE/%s/%s/PP_Cuts.hh"%(os.environ['CWB_CONFIG'],run,search_type,network)
Cuts_list=["bin1_cut","bin2_cut"]
Cuts_name=["with blips","without blips"]
#Cuts_trials=3

#run_start=1126256840
#run_end=1126260736
