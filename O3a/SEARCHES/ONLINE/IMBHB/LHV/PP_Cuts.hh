
// -------------------------------------------------------------------------

// Definitions of the search bins used for Offline IMBH Search
// -------------------------------------------------------------------------

// definition of the selection selection cuts 
TCut fcut("fcut","frequency[0]>24 && frequency[0]<=256");
TCut netcc_cut("netcc_cut","netcc[0]>0.8 && netcc[2]>0.7");
TCut qveto_cut("qveto_cut","Qveto[0]>(frequency[0]<90?0.1:0.3) ");
TCut chirp_cut("chirp_cut","abs(chirp[1])>10 && chirp[1]>-100");
TCut chi4_cut("chi4_cut","log10(penalty)<0.4");
TCut norm4_cut("norm4_cut","norm>4");


// definition of the inclusive bins
TCut bin1_cut= TCut("bin1_cut",(norm4_cut+fcut+chi4_cut+netcc_cut+qveto_cut+chirp_cut).GetTitle());
