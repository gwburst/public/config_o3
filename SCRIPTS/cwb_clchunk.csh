#!/bin/tcsh -f

onintr irq_ctrlc

if ( $1 == '' ) then
  $CWB_SCRIPTS/cwb_help.csh cwb_clchunk
  exit
endif

if ($1 == '') then
  echo ""
  echo 'cwb_clchunk --idir="..." --run="..." --chunk="..." --cal="..." --net="..." --search="..." --type="..." --tag="..."'
  echo ""
  echo "idir    : input working directory"
  echo "run     : O1, O2, O3"
  echo "chunk   : integer number"
  echo "cal     : C00, C00c, C01, C01c, C02, C02c"
  echo "net     : LH LHV"
  echo "search  : BurstLF, BurstHF, BurstLD, IMBHB, BBH"
  echo "type    : BKG, SIM"
  echo "tag     : user string (Ex: run1, tst1, dev1)"
  echo ""
  echo 'Ex: cwb_clchunk --idir="O2_03_C00_LH_BurstLF_BKG_dev1" --run="O2" --chunk="03" --cal="C00" --net="LH" --search="BurstLF" --type="BKG" --tag="dev1" --opt="create"'
  echo "or"
  echo 'Ex: cwb_clchunk --idir=O2_03_C00_LH_BurstLF_BKG_dev1 --run=O2 --chunk=03 --cal=C00 --net=LH --search=BurstLF --type=BKG --tag=dev1 --opt=create'
  echo "or"
  echo 'Ex: cwb_clchunk --idir O2_03_C00_LH_BurstLF_BKG_dev1 --run O2 --chunk 03 --cal C00 --net LH --search BurstLF --type BKG --tag dev1 --opt create'
  echo ""
  exit 1
endif

setenv CWB_CLCHUNK_IDIR		""
setenv CWB_CLCHUNK_RUN		""
setenv CWB_CLCHUNK_CHUNK	""
setenv CWB_CLCHUNK_CAL		""
setenv CWB_CLCHUNK_NET		""
setenv CWB_CLCHUNK_SEARCH	""
setenv CWB_CLCHUNK_TYPE		""
setenv CWB_CLCHUNK_TAG		""

set cmd_line="$0 $argv"

set temp=(`getopt -s tcsh -o i:r:c:C:n:s:t:T: --long idir:,run:,chunk:,cal:,net:,search:,type:,tag: -- $argv:q`)
if ($? != 0) then
  echo "Terminating..." >/dev/stderr
  exit 1
endif
eval set argv=\($temp:q\)

while (1)
        switch($1:q)
        case -i:
        case --idir:
                setenv CWB_CLCHUNK_IDIR         $2:q
                shift ; shift
                breaksw
        case -r:
        case --run:
                setenv CWB_CLCHUNK_RUN          $2:q
                shift ; shift
                breaksw
        case -c:
        case --chunk:
                setenv CWB_CLCHUNK_CHUNK        $2:q
                shift ; shift
                breaksw
        case -C:
        case --cal:
                setenv CWB_CLCHUNK_CAL          $2:q
                shift ; shift
                breaksw
        case -n:
        case --net:
                setenv CWB_CLCHUNK_NET          $2:q
                shift ; shift
                breaksw
        case -s:
        case --search:
                setenv CWB_CLCHUNK_SEARCH       $2:q
                shift ; shift
                breaksw
        case -t:
        case --type:
                setenv CWB_CLCHUNK_TYPE         $2:q
                shift ; shift
                breaksw
        case -T:
        case --tag:
                setenv CWB_CLCHUNK_TAG          $2:q
                shift ; shift
                breaksw
        case --:
                shift
                break
        default:
                echo "error - missing parameters!" ; exit 1
        endsw
end


if ((( $CWB_CLCHUNK_IDIR == '' ))) then
  echo ""
  echo "Error: empty input working directory"
  echo "type cwb_clchunk to list the available options"
  echo ""
  exit 1
endif

if ((( $CWB_CLCHUNK_RUN != 'O1' ) && ( $CWB_CLCHUNK_RUN != 'O2' ) && ( $CWB_CLCHUNK_RUN != 'O3' )) then
  echo ""
  echo --run=\'$CWB_CLCHUNK_RUN\' "is a wrong cwb_clchunk option\n"
  echo "type cwb_clchunk to list the available options"
  echo ""
  exit 1
endif

if ( $CWB_CLCHUNK_CHUNK !~ ^[0-9]+$ ) then
  echo ""
  echo --chunk=\'$CWB_CLCHUNK_CHUNK\' "is a wrong cwb_clchunk option\n"
  echo "type cwb_clchunk to list the available options"
  echo ""
  exit 1
endif 

if ((( $CWB_CLCHUNK_CAL != 'C00' ) && ( $CWB_CLCHUNK_CAL != 'C00c' ) && ( $CWB_CLCHUNK_CAL != 'C01' ) && ( $CWB_CLCHUNK_CAL != 'C01c' ) && ( $CWB_CLCHUNK_CAL != 'C02' ) && ( $CWB_CLCHUNK_CAL != 'C02c' ))) then
  echo ""
  echo --cal=\'$CWB_CLCHUNK_CAL\' "is a wrong cwb_clchunk option\n"
  echo "type cwb_clchunk to list the available options"
  echo ""
  exit 1
endif

if ((( $CWB_CLCHUNK_NET != 'LH' ) && ( $CWB_CLCHUNK_NET != 'LV' ) && ( $CWB_CLCHUNK_NET != 'HV' ) && ( $CWB_CLCHUNK_NET != 'LHV' ))) then
  echo ""
  echo --net=\'$CWB_CLCHUNK_NET\' "is a wrong cwb_clchunk option\n"
  echo "type cwb_clchunk to list the available options"
  echo ""
  exit 1
endif

if ((( $CWB_CLCHUNK_SEARCH != 'BurstLF' ) && ( $CWB_CLCHUNK_SEARCH != 'BurstHF' ) && ( $CWB_CLCHUNK_SEARCH != 'BurstLD' ) && ( $CWB_CLCHUNK_SEARCH != 'IMBHB' ) && ( $CWB_CLCHUNK_SEARCH != 'BBH' ))) then
  echo ""
  echo --search=\'$CWB_CLCHUNK_SEARCH\' "is a wrong cwb_clchunk option\n"
  echo "type cwb_clchunk to list the available options"
  echo ""
  exit 1
endif

if ((( $CWB_CLCHUNK_TYPE == 'BKG' ) || ( $CWB_CLCHUNK_TYPE =~ 'SIM/*' ))) then
  setenv CWB_CLCHUNK_TYPE $CWB_CLCHUNK_TYPE
  set CWB_CLCHUNK_SIM=$CWB_CLCHUNK_TYPE
  set CWB_CLCHUNK_SIM_DIR=`echo $CWB_CLCHUNK_SIM | awk '{print substr($0, 5, length($0)-1)}'`
else
  echo ""
  echo --type=\'$CWB_CLCHUNK_TYPE\' "is a wrong cwb_clchunk option\n"
  echo "type cwb_clchunk to list the available options"
  echo ""
  exit 1
endif

if ((( $CWB_CLCHUNK_TAG == '' ))) then
  echo ""
  echo --tag=\'$CWB_CLCHUNK_TAG\' "is a wrong cwb_clchunk option\n"
  echo "type cwb_clchunk to list the available options"
  echo ""
  exit 1
endif

if (( $CWB_CLCHUNK_TYPE =~ 'SIM/*' )) then
  setenv CWB_CLCHUNK_ODIR $CWB_CLCHUNK_RUN\_K$CWB_CLCHUNK_CHUNK\_$CWB_CLCHUNK_CAL\_$CWB_CLCHUNK_NET\_$CWB_CLCHUNK_SEARCH\_SIM\_$CWB_CLCHUNK_SIM_DIR\_$CWB_CLCHUNK_TAG
  ${CWB_SCRIPTS}/cwb_clonedir.csh $CWB_CLCHUNK_IDIR $CWB_CLCHUNK_ODIR '--output merge --simulation true'
else
  setenv CWB_CLCHUNK_ODIR $CWB_CLCHUNK_RUN\_K$CWB_CLCHUNK_CHUNK\_$CWB_CLCHUNK_CAL\_$CWB_CLCHUNK_NET\_$CWB_CLCHUNK_SEARCH\_$CWB_CLCHUNK_TYPE\_$CWB_CLCHUNK_TAG
  ${CWB_SCRIPTS}/cwb_clonedir.csh $CWB_CLCHUNK_IDIR $CWB_CLCHUNK_ODIR '--output merge'
endif

cp $CWB_CONFIG/$CWB_CLCHUNK_RUN/SEARCHES/OFFLINE/$CWB_CLCHUNK_SEARCH/$CWB_CLCHUNK_NET/$CWB_CLCHUNK_TYPE/user_pparameters.C $CWB_CLCHUNK_ODIR/config/

if (( $CWB_CLCHUNK_TYPE =~ 'SIM/*' )) then
  cp $CWB_CONFIG/$CWB_CLCHUNK_RUN/SEARCHES/OFFLINE/$CWB_CLCHUNK_SEARCH/$CWB_CLCHUNK_NET/$CWB_CLCHUNK_TYPE/*.inj $CWB_CLCHUNK_ODIR/input/
endif

# Remove leading zeroes from CWB_CLCHUNK_CHUNK (08 -> 8)
set CWB_CLCHUNK_XCHUNK=`echo $CWB_CLCHUNK_CHUNK | sed 's/^0*//'`

sed -i "s/#CWB_CHUNK_NUMBER/$CWB_CLCHUNK_XCHUNK/g" $CWB_CLCHUNK_ODIR/config/user_pparameters.C
sed -i "s\#CWB_CALIB_VER\$CWB_CLCHUNK_CAL\g"      $CWB_CLCHUNK_ODIR/config/user_pparameters.C


# create cWB_config.log file
cd $CWB_CLCHUNK_ODIR
make -f $CWB_CONFIG/Makefile.log CMD_LINE="$cmd_line" git >& /dev/null
cd ..

unsetenv CWB_CLCHUNK_IDIR
unsetenv CWB_CLCHUNK_RUN
unsetenv CWB_CLCHUNK_CHUNK
unsetenv CWB_CLCHUNK_CAL
unsetenv CWB_CLCHUNK_NET
unsetenv CWB_CLCHUNK_SEARCH
unsetenv CWB_CLCHUNK_TYPE
unsetenv CWB_CLCHUNK_TAG
unsetenv CWB_CLCHUNK_ODIR

exit 0
irq_ctrlc:
  ps T | grep root | awk '{print $1}' | xargs kill -9
  exit 1
