#!/bin/tcsh -f

onintr irq_ctrlc

if ( $1 == '' ) then
  $CWB_SCRIPTS/cwb_help.csh cwb_mkchunk
  exit
endif

if ($1 == '') then
  echo ""
  echo 'cwb_mkchunk --run="..." --chunk="..." --cal="..." --net="..." --search="..." --type="..." --tag="..." --opt="..."'
  echo ""
  echo "run     : O1, O2, O3"
  echo "chunk   : integer number"
  echo "cal     : C00, C00c, C01, C01c, C02, C02c"
  echo "net     : LH LV HV LHV"
  echo "search  : BurstLF, BurstHF, BurstLD, IMBHB, BBH"
  echo "type    : BKG, SIM"
  echo "tag     : user string (Ex: run1, tst1, dev1)"
  echo ""
  echo "opt     : optional"
  echo "          no options -> disabled (default)"
  echo "          create : execute condor_create"
  echo "          submit : execute condor_create & condor_submit"
  echo ""
  echo 'Ex: cwb_mkchunk --run="O2" --chunk="03" --cal="C00" --net="LH" --search="BurstLF" --type="BKG" --tag="dev1" --opt="create"'
  echo "or"
  echo 'Ex: cwb_mkchunk --run=O2 --chunk=03 --cal=C00 --net=LH --search=BurstLF --type=BKG --tag=dev1 --opt=create'
  echo "or"
  echo 'Ex: cwb_mkchunk --run O2 --chunk 03 --cal C00 --net LH --search BurstLF --type BKG --tag dev1 --opt create'
  echo ""
  echo "creates working directory -> O2_K03_C00_LH_BurstLF_BKG_dev1"
  echo ""
  exit 1
endif

setenv CWB_MKCHUNK_RUN
setenv CWB_MKCHUNK_CHUNK
setenv CWB_MKCHUNK_CAL
setenv CWB_MKCHUNK_NET
setenv CWB_MKCHUNK_SEARCH
setenv CWB_MKCHUNK_TYPE
setenv CWB_MKCHUNK_TAG
setenv CWB_MKCHUNK_OPTIONS

set cmd_line="$0 $argv"

set temp=(`getopt -s tcsh -o r:c:C:n:s:t:T:o: --long run:,chunk:,cal:,net:,search:,type:,tag:,opt: -- $argv:q`)
if ($? != 0) then
  echo "Terminating..." >/dev/stderr
  exit 1
endif
eval set argv=\($temp:q\)

while (1)
        switch($1:q)
        case -r:
        case --run:
                setenv CWB_MKCHUNK_RUN          $2:q
                shift ; shift
                breaksw
        case -c:
        case --chunk:
                setenv CWB_MKCHUNK_CHUNK        $2:q
                shift ; shift
                breaksw
        case -C:
        case --cal:
                setenv CWB_MKCHUNK_CAL          $2:q
                shift ; shift
                breaksw
        case -n:
        case --net:
                setenv CWB_MKCHUNK_NET          $2:q
                shift ; shift
                breaksw
        case -s:
        case --search:
                setenv CWB_MKCHUNK_SEARCH       $2:q
                shift ; shift
                breaksw
        case -t:
        case --type:
                setenv CWB_MKCHUNK_TYPE         $2:q
                shift ; shift
                breaksw
        case -T:
        case --tag:
                setenv CWB_MKCHUNK_TAG          $2:q
                shift ; shift
                breaksw
        case -o:
        case --opt:
                setenv CWB_MKCHUNK_OPTIONS      $2:q
                shift ; shift
                breaksw
        case --:
                shift
                break
        default:
                echo "error - missing parameters!" ; exit 1
        endsw
end

if ((( $CWB_MKCHUNK_RUN != 'O1' ) && ( $CWB_MKCHUNK_RUN != 'O2' ) && ( $CWB_MKCHUNK_RUN != 'O3' ))) then
  echo ""
  echo --run=\'$CWB_MKCHUNK_RUN\' "is a wrong cwb_mkchunk option\n"
  echo "type cwb_mkchunk to list the available options"
  echo ""
  exit 1
endif

if ( $CWB_MKCHUNK_CHUNK !~ ^[0-9]+$ ) then
  echo ""
  echo --chunk=\'$CWB_MKCHUNK_CHUNK\' "is a wrong cwb_mkchunk option\n"
  echo "type cwb_mkchunk to list the available options"
  echo ""
  exit 1
endif 

if ((( $CWB_MKCHUNK_CAL != 'C00' ) && ( $CWB_MKCHUNK_CAL != 'C00c' ) && ( $CWB_MKCHUNK_CAL != 'C01' ) && ( $CWB_MKCHUNK_CAL != 'C01c' ) && ( $CWB_MKCHUNK_CAL != 'C02' ) && ( $CWB_MKCHUNK_CAL != 'C02c' ))) then
  echo ""
  echo --cal=\'$CWB_MKCHUNK_CAL\' "is a wrong cwb_mkchunk option\n"
  echo "type cwb_mkchunk to list the available options"
  echo ""
  exit 1
endif

if ((( $CWB_MKCHUNK_NET != 'LH' ) && ( $CWB_MKCHUNK_NET != 'LV' ) && ( $CWB_MKCHUNK_NET != 'HV' ) && ( $CWB_MKCHUNK_NET != 'LHV' ))) then
  echo ""
  echo --net=\'$CWB_MKCHUNK_NET\' "is a wrong cwb_mkchunk option\n"
  echo "type cwb_mkchunk to list the available options"
  echo ""
  exit 1
endif

if ((( $CWB_MKCHUNK_SEARCH != 'BurstLF' ) && ( $CWB_MKCHUNK_SEARCH != 'BurstHF' ) && ( $CWB_MKCHUNK_SEARCH != 'BurstLD' ) && ( $CWB_MKCHUNK_SEARCH != 'IMBHB' ) && ( $CWB_MKCHUNK_SEARCH != 'BBH' ))) then
  echo ""
  echo --search=\'$CWB_MKCHUNK_SEARCH\' "is a wrong cwb_mkchunk option\n"
  echo "type cwb_mkchunk to list the available options"
  echo ""
  exit 1
endif

if ((( $CWB_MKCHUNK_TYPE == 'BKG' ) || ( $CWB_MKCHUNK_TYPE =~ 'SIM/*' ))) then
  setenv CWB_MKCHUNK_TYPE $CWB_MKCHUNK_TYPE
  set CWB_MKCHUNK_SIM=$CWB_MKCHUNK_TYPE
  set CWB_MKCHUNK_SIM_DIR=`echo $CWB_MKCHUNK_SIM | awk '{print substr($0, 5, length($0)-1)}'`
else
  echo ""
  echo --type=\'$CWB_MKCHUNK_TYPE\' "is a wrong cwb_mkchunk option\n"
  echo "type cwb_mkchunk to list the available options"
  echo ""
  exit 1
endif

if ((( $CWB_MKCHUNK_TAG == '' ))) then
  echo ""
  echo --tag=\'$CWB_MKCHUNK_TAG\' "is a wrong cwb_mkchunk option\n"
  echo "type cwb_mkchunk to list the available options"
  echo ""
  exit 1
endif

if ((( $CWB_MKCHUNK_OPTIONS != 'create' ) && ( $CWB_MKCHUNK_OPTIONS != 'submit' ) && ( $CWB_MKCHUNK_OPTIONS != '' ))) then
  echo ""
  echo --opt=\'$CWB_MKCHUNK_OPTIONS\' "is a wrong cwb_mkchunk option\n"
  echo "type cwb_mkchunk to list the available options"
  echo ""
  exit 1
endif

setenv SN_PROD_CONDOR_TAG 	""
setenv SN_SIM_CONDOR_TAG 	""
setenv BURSTLF_PROD_CONDOR_TAG 	""
setenv BURSTLF_SIM_CONDOR_TAG 	""
setenv BURSTHF_PROD_CONDOR_TAG 	""
setenv BURSTHF_SIM_CONDOR_TAG 	""
setenv BURSTLD_PROD_CONDOR_TAG 	""
setenv BURSTLD_SIM_CONDOR_TAG 	""
setenv BBH_PROD_CONDOR_TAG 	""
setenv BBH_SIM_CONDOR_TAG 	""
setenv IMBHB_PROD_CONDOR_TAG 	""
setenv IMBHB_SIM_CONDOR_TAG 	""
if ((( $CWB_MKCHUNK_RUN == 'O1' ))) then
  setenv SN_PROD_CONDOR_TAG 		$O1_SN_PROD_CONDOR_TAG
  setenv SN_SIM_CONDOR_TAG 		$O1_SN_SIM_CONDOR_TAG
  setenv BURSTLF_PROD_CONDOR_TAG 	$O1_BURSTLF_PROD_CONDOR_TAG
  setenv BURSTLF_SIM_CONDOR_TAG 	$O1_BURSTLF_SIM_CONDOR_TAG
  setenv BURSTHF_PROD_CONDOR_TAG 	$O1_BURSTHF_PROD_CONDOR_TAG
  setenv BURSTHF_SIM_CONDOR_TAG 	$O1_BURSTHF_SIM_CONDOR_TAG
  setenv BURSTLD_PROD_CONDOR_TAG 	$O1_BURSTLD_PROD_CONDOR_TAG
  setenv BURSTLD_SIM_CONDOR_TAG 	$O1_BURSTLD_SIM_CONDOR_TAG
  setenv BBH_PROD_CONDOR_TAG 		$O1_BBH_PROD_CONDOR_TAG
  setenv BBH_SIM_CONDOR_TAG 		$O1_BBH_SIM_CONDOR_TAG
  setenv IMBHB_PROD_CONDOR_TAG 		$O1_IMBHB_PROD_CONDOR_TAG
  setenv IMBHB_SIM_CONDOR_TAG 		$O1_IMBHB_SIM_CONDOR_TAG
endif
if ((( $CWB_MKCHUNK_RUN == 'O2' ))) then
  setenv SN_PROD_CONDOR_TAG 		$O2_SN_PROD_CONDOR_TAG
  setenv SN_SIM_CONDOR_TAG 		$O2_SN_SIM_CONDOR_TAG
  setenv BURSTLF_PROD_CONDOR_TAG 	$O2_BURSTLF_PROD_CONDOR_TAG
  setenv BURSTLF_SIM_CONDOR_TAG 	$O2_BURSTLF_SIM_CONDOR_TAG
  setenv BURSTHF_PROD_CONDOR_TAG 	$O2_BURSTHF_PROD_CONDOR_TAG
  setenv BURSTHF_SIM_CONDOR_TAG 	$O2_BURSTHF_SIM_CONDOR_TAG
  setenv BURSTLD_PROD_CONDOR_TAG 	$O2_BURSTLD_PROD_CONDOR_TAG
  setenv BURSTLD_SIM_CONDOR_TAG 	$O2_BURSTLD_SIM_CONDOR_TAG
  setenv BBH_PROD_CONDOR_TAG 		$O2_BBH_PROD_CONDOR_TAG
  setenv BBH_SIM_CONDOR_TAG 		$O2_BBH_SIM_CONDOR_TAG
  setenv IMBHB_PROD_CONDOR_TAG 		$O2_IMBHB_PROD_CONDOR_TAG
  setenv IMBHB_SIM_CONDOR_TAG 		$O2_IMBHB_SIM_CONDOR_TAG
endif
if ((( $CWB_MKCHUNK_RUN == 'O3' ))) then
  setenv SN_PROD_CONDOR_TAG 		$O3_SN_PROD_CONDOR_TAG
  setenv SN_SIM_CONDOR_TAG 		$O3_SN_SIM_CONDOR_TAG
  setenv BURSTLF_PROD_CONDOR_TAG 	$O3_BURSTLF_PROD_CONDOR_TAG
  setenv BURSTLF_SIM_CONDOR_TAG 	$O3_BURSTLF_SIM_CONDOR_TAG
  setenv BURSTHF_PROD_CONDOR_TAG 	$O3_BURSTHF_PROD_CONDOR_TAG
  setenv BURSTHF_SIM_CONDOR_TAG 	$O3_BURSTHF_SIM_CONDOR_TAG
  setenv BURSTLD_PROD_CONDOR_TAG 	$O3_BURSTLD_PROD_CONDOR_TAG
  setenv BURSTLD_SIM_CONDOR_TAG 	$O3_BURSTLD_SIM_CONDOR_TAG
  setenv BBH_PROD_CONDOR_TAG 		$O3_BBH_PROD_CONDOR_TAG
  setenv BBH_SIM_CONDOR_TAG 		$O3_BBH_SIM_CONDOR_TAG
  setenv IMBHB_PROD_CONDOR_TAG 		$O3_IMBHB_PROD_CONDOR_TAG
  setenv IMBHB_SIM_CONDOR_TAG 		$O3_IMBHB_SIM_CONDOR_TAG
endif

setenv CWB_MKCHUNK_V_CHNAME ""
if ((( $CWB_MKCHUNK_CAL == 'C00' ))) then
  if ((( $CWB_MKCHUNK_RUN == 'O1' ))) then
    setenv CWB_MKCHUNK_CHNAME $O1_CWB_CONFIG_C00_CHNAME
  endif
  if ((( $CWB_MKCHUNK_RUN == 'O2' ))) then
    setenv CWB_MKCHUNK_CHNAME $O2_CWB_CONFIG_C00_CHNAME
  endif
  if ((( $CWB_MKCHUNK_RUN == 'O3' ))) then
    setenv CWB_MKCHUNK_CHNAME   $O3_CWB_CONFIG_C00_CHNAME
    setenv CWB_MKCHUNK_V_CHNAME $O3_CWB_CONFIG_C00_V_CHNAME
  endif
endif
if ((( $CWB_MKCHUNK_CAL == 'C00c' ))) then
  if ((( $CWB_MKCHUNK_RUN == 'O1' ))) then
    setenv CWB_MKCHUNK_CHNAME 	$O1_CWB_CONFIG_C00c_CHNAME
  endif
  if ((( $CWB_MKCHUNK_RUN == 'O2' ))) then
    setenv CWB_MKCHUNK_CHNAME 	$O2_CWB_CONFIG_C00c_CHNAME
  endif
  if ((( $CWB_MKCHUNK_RUN == 'O3' ))) then
    setenv CWB_MKCHUNK_CHNAME   $O3_CWB_CONFIG_C00c_CHNAME
    setenv CWB_MKCHUNK_V_CHNAME $O3_CWB_CONFIG_C00c_V_CHNAME
  endif
endif
if ((( $CWB_MKCHUNK_CAL == 'C01' ))) then
  if ((( $CWB_MKCHUNK_RUN == 'O1' ))) then
    setenv CWB_MKCHUNK_CHNAME 	$O1_CWB_CONFIG_C01_CHNAME
  endif
  if ((( $CWB_MKCHUNK_RUN == 'O2' ))) then
    setenv CWB_MKCHUNK_CHNAME 	$O2_CWB_CONFIG_C01_CHNAME
  endif
  if ((( $CWB_MKCHUNK_RUN == 'O3' ))) then
    setenv CWB_MKCHUNK_CHNAME 	$O3_CWB_CONFIG_C01_CHNAME
    setenv CWB_MKCHUNK_V_CHNAME $O3_CWB_CONFIG_C01_V_CHNAME
  endif
endif
if ((( $CWB_MKCHUNK_CAL == 'C01c' ))) then
  if ((( $CWB_MKCHUNK_RUN == 'O1' ))) then
    setenv CWB_MKCHUNK_CHNAME   $O1_CWB_CONFIG_C01c_CHNAME
    setenv CWB_MKCHUNK_V_CHNAME $O1_CWB_CONFIG_C01c_V_CHNAME
  endif
  if ((( $CWB_MKCHUNK_RUN == 'O2' ))) then
    setenv CWB_MKCHUNK_CHNAME   $O2_CWB_CONFIG_C01c_CHNAME
    setenv CWB_MKCHUNK_V_CHNAME $O2_CWB_CONFIG_C01c_V_CHNAME
  endif
  if ((( $CWB_MKCHUNK_RUN == 'O3' ))) then
    setenv CWB_MKCHUNK_CHNAME 	$O3_CWB_CONFIG_C01c_CHNAME
    setenv CWB_MKCHUNK_V_CHNAME $O3_CWB_CONFIG_C01c_V_CHNAME
  endif
endif
if ((( $CWB_MKCHUNK_CAL == 'C02' ))) then
  if ((( $CWB_MKCHUNK_RUN == 'O1' ))) then
    setenv CWB_MKCHUNK_CHNAME 	$O1_CWB_CONFIG_C02_CHNAME
  endif
  if ((( $CWB_MKCHUNK_RUN == 'O2' ))) then
    setenv CWB_MKCHUNK_CHNAME 	$O2_CWB_CONFIG_C02_CHNAME
  endif
  if ((( $CWB_MKCHUNK_RUN == 'O3' ))) then
    setenv CWB_MKCHUNK_CHNAME 	$O3_CWB_CONFIG_C02_CHNAME
    setenv CWB_MKCHUNK_V_CHNAME $O3_CWB_CONFIG_C02_V_CHNAME
  endif
endif
if ((( $CWB_MKCHUNK_CAL == 'C02c' ))) then
  if ((( $CWB_MKCHUNK_RUN == 'O1' ))) then
    setenv CWB_MKCHUNK_CHNAME   $O1_CWB_CONFIG_C02c_CHNAME
    setenv CWB_MKCHUNK_V_CHNAME $O1_CWB_CONFIG_C02c_V_CHNAME
  endif
  if ((( $CWB_MKCHUNK_RUN == 'O2' ))) then
    setenv CWB_MKCHUNK_CHNAME   $O2_CWB_CONFIG_C02c_CHNAME
    setenv CWB_MKCHUNK_V_CHNAME $O2_CWB_CONFIG_C02c_V_CHNAME
  endif
  if ((( $CWB_MKCHUNK_RUN == 'O3' ))) then
    setenv CWB_MKCHUNK_CHNAME 	$O3_CWB_CONFIG_C02c_CHNAME
    setenv CWB_MKCHUNK_V_CHNAME $O3_CWB_CONFIG_C02c_V_CHNAME
  endif
endif

# check if config stuff exist in the input config repository
${CWB_CONFIG}/SCRIPTS/cwb_ckchunk.csh --run=$CWB_MKCHUNK_RUN --chunk=$CWB_MKCHUNK_CHUNK --cal=$CWB_MKCHUNK_CAL --net=$CWB_MKCHUNK_NET --search=$CWB_MKCHUNK_SEARCH --type=$CWB_MKCHUNK_TYPE
if ( $? != 0) exit 1

echo "--------------------------------------------------------------------------------"
echo $CWB_MKCHUNK_CHNAME
echo "--------------------------------------------------------------------------------"

if (( $CWB_MKCHUNK_TYPE =~ 'SIM/*' )) then
  setenv CWB_MKCHUNK_DIR $CWB_MKCHUNK_RUN\_K$CWB_MKCHUNK_CHUNK\_$CWB_MKCHUNK_CAL\_$CWB_MKCHUNK_NET\_$CWB_MKCHUNK_SEARCH\_SIM\_$CWB_MKCHUNK_SIM_DIR\_$CWB_MKCHUNK_TAG
else
  setenv CWB_MKCHUNK_DIR $CWB_MKCHUNK_RUN\_K$CWB_MKCHUNK_CHUNK\_$CWB_MKCHUNK_CAL\_$CWB_MKCHUNK_NET\_$CWB_MKCHUNK_SEARCH\_$CWB_MKCHUNK_TYPE\_$CWB_MKCHUNK_TAG
endif

echo "Search plugin name ..."
set CWB_MKCHUNK_PLUGIN = $CWB_CONFIG/$CWB_MKCHUNK_RUN/SEARCHES/OFFLINE/$CWB_MKCHUNK_SEARCH/$CWB_MKCHUNK_NET/$CWB_MKCHUNK_TYPE/CWB_Plugin*.C

${CWB_SCRIPTS}/cwb_mkdir.csh $CWB_MKCHUNK_DIR

cp $CWB_CONFIG/$CWB_MKCHUNK_RUN/SEARCHES/OFFLINE/$CWB_MKCHUNK_SEARCH/$CWB_MKCHUNK_NET/$CWB_MKCHUNK_TYPE/user_parameters.C  $CWB_MKCHUNK_DIR/config/
cp $CWB_CONFIG/$CWB_MKCHUNK_RUN/SEARCHES/OFFLINE/$CWB_MKCHUNK_SEARCH/$CWB_MKCHUNK_NET/$CWB_MKCHUNK_TYPE/user_pparameters.C $CWB_MKCHUNK_DIR/config/
cp $CWB_MKCHUNK_PLUGIN      											           $CWB_MKCHUNK_DIR/macro/

if (( $CWB_MKCHUNK_TYPE =~ 'SIM/*' )) then
  cp $CWB_CONFIG/$CWB_MKCHUNK_RUN/SEARCHES/OFFLINE/$CWB_MKCHUNK_SEARCH/$CWB_MKCHUNK_NET/$CWB_MKCHUNK_TYPE/*.inj $CWB_MKCHUNK_DIR/input/
endif

# Remove leading zeroes from CWB_MKCHUNK_CHUNK (08 -> 8)
set CWB_MKCHUNK_XCHUNK=`echo $CWB_MKCHUNK_CHUNK | sed 's/^0*//'`

sed -i "s/#SN_PROD_CONDOR_TAG/$SN_PROD_CONDOR_TAG/g" 		$CWB_MKCHUNK_DIR/config/user_parameters.C
sed -i "s/#SN_SIM_CONDOR_TAG/$SN_SIM_CONDOR_TAG/g" 		$CWB_MKCHUNK_DIR/config/user_parameters.C
sed -i "s/#BURSTLF_PROD_CONDOR_TAG/$BURSTLF_PROD_CONDOR_TAG/g" 	$CWB_MKCHUNK_DIR/config/user_parameters.C
sed -i "s/#BURSTLF_SIM_CONDOR_TAG/$BURSTLF_SIM_CONDOR_TAG/g" 	$CWB_MKCHUNK_DIR/config/user_parameters.C
sed -i "s/#BURSTHF_PROD_CONDOR_TAG/$BURSTHF_PROD_CONDOR_TAG/g" 	$CWB_MKCHUNK_DIR/config/user_parameters.C
sed -i "s/#BURSTHF_SIM_CONDOR_TAG/$BURSTHF_SIM_CONDOR_TAG/g" 	$CWB_MKCHUNK_DIR/config/user_parameters.C
sed -i "s/#BURSTLD_PROD_CONDOR_TAG/$BURSTLD_PROD_CONDOR_TAG/g" 	$CWB_MKCHUNK_DIR/config/user_parameters.C
sed -i "s/#BURSTLD_SIM_CONDOR_TAG/$BURSTLD_SIM_CONDOR_TAG/g" 	$CWB_MKCHUNK_DIR/config/user_parameters.C
sed -i "s/#BBH_PROD_CONDOR_TAG/$BBH_PROD_CONDOR_TAG/g" 		$CWB_MKCHUNK_DIR/config/user_parameters.C
sed -i "s/#BBH_SIM_CONDOR_TAG/$BBH_SIM_CONDOR_TAG/g" 		$CWB_MKCHUNK_DIR/config/user_parameters.C
sed -i "s/#IMBHB_PROD_CONDOR_TAG/$IMBHB_PROD_CONDOR_TAG/g" 	$CWB_MKCHUNK_DIR/config/user_parameters.C
sed -i "s/#IMBHB_SIM_CONDOR_TAG/$IMBHB_SIM_CONDOR_TAG/g" 	$CWB_MKCHUNK_DIR/config/user_parameters.C

sed -i "s/#CWB_CHUNK_NUMBER/$CWB_MKCHUNK_XCHUNK/g" 		$CWB_MKCHUNK_DIR/config/user_parameters.C
sed -i "s\#CWB_CALIB_VER\$CWB_MKCHUNK_CAL\g"      		$CWB_MKCHUNK_DIR/config/user_parameters.C
sed -i "s\#CWB_CHANNEL_NAME\$CWB_MKCHUNK_CHNAME\g"		$CWB_MKCHUNK_DIR/config/user_parameters.C
sed -i "s\#CWB_L_CHANNEL_NAME\$CWB_MKCHUNK_CHNAME\g"		$CWB_MKCHUNK_DIR/config/user_parameters.C
sed -i "s\#CWB_H_CHANNEL_NAME\$CWB_MKCHUNK_CHNAME\g"		$CWB_MKCHUNK_DIR/config/user_parameters.C
sed -i "s\#CWB_V_CHANNEL_NAME\$CWB_MKCHUNK_V_CHNAME\g"		$CWB_MKCHUNK_DIR/config/user_parameters.C
sed -i "s/#CWB_CHUNK_NUMBER/$CWB_MKCHUNK_XCHUNK/g" 		$CWB_MKCHUNK_DIR/config/user_pparameters.C
sed -i "s\#CWB_CALIB_VER\$CWB_MKCHUNK_CAL\g"      		$CWB_MKCHUNK_DIR/config/user_pparameters.C
sed -i "s\#CWB_MDC_TYPE\$CWB_MKCHUNK_SIM_DIR\g"         	$CWB_MKCHUNK_DIR/macro/CWB_Plugin_Config.C 


# compile plugin
set CWB_MKCHUNK_PLUGIN = $CWB_MKCHUNK_DIR/macro/CWB_Plugin*.C

if (( $CWB_MKCHUNK_TYPE =~ 'SIM/*' )) then
  # extract plugin from plugin + plugin_config
  set split = ($CWB_MKCHUNK_PLUGIN:as/ / /)
  echo $split[2]
  ${CWB_SCRIPTS}/cwb_compile.csh $split[2]
else
  echo $CWB_MKCHUNK_PLUGIN
  ${CWB_SCRIPTS}/cwb_compile.csh $CWB_MKCHUNK_PLUGIN
endif

cd $CWB_MKCHUNK_DIR

# create cWB_config.log file
make -f $CWB_CONFIG/Makefile.log CMD_LINE="$cmd_line" git >& /dev/null

if ((( $CWB_MKCHUNK_OPTIONS == 'create' ) || ( $CWB_MKCHUNK_OPTIONS == 'submit' ))) then
  ${CWB_SCRIPTS}/cwb_condor.csh create
endif
if ((( $CWB_MKCHUNK_OPTIONS == 'submit' ))) then
  echo "excecute cwb_condor.csh submit ..."
  #${CWB_SCRIPTS}/cwb_condor.csh submit
endif

echo ''
echo 'The new working dir is :	'$CWB_MKCHUNK_DIR
echo ''

cd ..

unsetenv CWB_MKCHUNK_RUN
unsetenv CWB_MKCHUNK_CHUNK
unsetenv CWB_MKCHUNK_CAL
unsetenv CWB_MKCHUNK_NET
unsetenv CWB_MKCHUNK_SEARCH
unsetenv CWB_MKCHUNK_TYPE
unsetenv CWB_MKCHUNK_TAG
unsetenv CWB_MKCHUNK_OPTIONS

unsetenv CWB_MKCHUNK_DIR
unsetenv CWB_MKCHUNK_PLUGIN
unsetenv CWB_MKCHUNK_CHNAME
unsetenv CWB_MKCHUNK_TYPE_DIR

exit 0
irq_ctrlc:
  ps T | grep root | awk '{print $1}' | xargs kill -9
  exit 1
