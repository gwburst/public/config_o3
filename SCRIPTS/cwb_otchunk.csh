#!/bin/tcsh -f

onintr irq_ctrlc

#if ( $1 == '' ) then
#  $CWB_SCRIPTS/cwb_help.csh cwb_otchunk
#  exit
#endif

if ($1 == '') then
  echo ""
  echo "-------------------------------------------------------------------------------------------------------------------------"
  echo "What it does: compute the effective observational time processed with jobs at zero lag after cat1 & cat2 subtraction     "
  echo "              call 'cwb_dump sjob/job' -> produces a file with list of standard/extended (fixed/varable length) segments " 
  echo "-------------------------------------------------------------------------------------------------------------------------"
  echo ""
  echo 'cwb_otchunk --run=... --chunk=... --cal=... --net=... --search=... --dir=... --label=...'
  echo ""
  echo "run     : O1, O2, O3"
  echo "chunk   : integer number"
  echo "cal     : C00, C00c, C01, C01c, C02, C02c"
  echo "net     : LH LV HV LHV"
  echo "search  : BurstLF, BurstHF, BurstLD, IMBHB, BBH"
  echo "dir     : output dir"
  echo "label   : output file label"
  echo ""
  echo 'Ex: cwb_otchunk --run=O2 --chunk=03 --cal=C00 --net=LH --search=BurstLF --dir=output_dir --label=output_file_label'
  echo ""
  echo "cwb_otchunk without arguments shows help"
  echo ""
  echo "----------------------------------------------------------------------------------"
  echo ""
  exit 1
endif

setenv CWB_OTCHUNK_RUN		""
setenv CWB_OTCHUNK_CHUNK	""
setenv CWB_OTCHUNK_CAL		""
setenv CWB_OTCHUNK_NET		""
setenv CWB_OTCHUNK_SEARCH	""
setenv CWB_OTCHUNK_DIR	        "empty"
setenv CWB_OTCHUNK_LABEL	""

set cmd_line="$0 $argv"

set temp=(`getopt -s tcsh -o r:c:C:n:s:d:l: --long run:,chunk:,cal:,net:,search:,dir:,label: -- $argv:q`)
if ($? != 0) then
  echo "Terminating..." >/dev/stderr
  exit 1
endif
eval set argv=\($temp:q\)

while (1)
        switch($1:q)
        case -r:
        case --run:
                setenv CWB_OTCHUNK_RUN          $2:q
                shift ; shift
                breaksw
        case -c:
        case --chunk:
                setenv CWB_OTCHUNK_CHUNK        $2:q
                shift ; shift
                breaksw
        case -C:
        case --cal:
                setenv CWB_OTCHUNK_CAL          $2:q
                shift ; shift
                breaksw
        case -n:
        case --net:
                setenv CWB_OTCHUNK_NET          $2:q
                shift ; shift
                breaksw
        case -s:
        case --search:
                setenv CWB_OTCHUNK_SEARCH       $2:q
                shift ; shift
                breaksw
        case -d:
        case --dir:
                setenv CWB_OTCHUNK_DIR          $2:q
                shift ; shift
                breaksw
        case -l:
        case --label:
                setenv CWB_OTCHUNK_LABEL        $2:q
                shift ; shift
                breaksw
        case --:
                shift
                break
        default:
                echo "error - missing parameters!" ; exit 1
        endsw
end

if ((( $CWB_OTCHUNK_RUN != 'O1' ) && ( $CWB_OTCHUNK_RUN != 'O2' ) && ( $CWB_OTCHUNK_RUN != 'O3' ))) then
  echo ""
  echo --run=\'$CWB_OTCHUNK_RUN\' "is a wrong cwb_otchunk option\n"
  echo "type cwb_otchunk to list the available options"
  echo ""
  exit 1
endif

if ( $CWB_OTCHUNK_CHUNK !~ ^[0-9]+$ ) then
  echo ""
  echo --chunk=\'$CWB_OTCHUNK_CHUNK\' "is a wrong cwb_otchunk option\n"
  echo "type cwb_otchunk to list the available options"
  echo ""
  exit 1
endif 

if ((( $CWB_OTCHUNK_CAL != 'C00' ) && ( $CWB_OTCHUNK_CAL != 'C00c' ) && ( $CWB_OTCHUNK_CAL != 'C01' ) && ( $CWB_OTCHUNK_CAL != 'C01c' ) && ( $CWB_OTCHUNK_CAL != 'C02' ) && ( $CWB_OTCHUNK_CAL != 'C02c' ))) then
  echo ""
  echo --cal=\'$CWB_OTCHUNK_CAL\' "is a wrong cwb_otchunk option\n"
  echo "type cwb_otchunk to list the available options"
  echo ""
  exit 1
endif

if ((( $CWB_OTCHUNK_NET != 'LH' ) && ( $CWB_OTCHUNK_NET != 'LV' ) && ( $CWB_OTCHUNK_NET != 'HV' ) && ( $CWB_OTCHUNK_NET != 'LHV' ))) then
  echo ""
  echo --net=\'$CWB_OTCHUNK_NET\' "is a wrong cwb_otchunk option\n"
  echo "type cwb_otchunk to list the available options"
  echo ""
  exit 1
endif

if ((( $CWB_OTCHUNK_SEARCH != 'BurstLF' ) && ( $CWB_OTCHUNK_SEARCH != 'BurstHF' ) && ( $CWB_OTCHUNK_SEARCH != 'BurstLD' ) && ( $CWB_OTCHUNK_SEARCH != 'IMBHB' ) && ( $CWB_OTCHUNK_SEARCH != 'BBH' ))) then
  echo ""
  echo --search=\'$CWB_OTCHUNK_SEARCH\' "is a wrong cwb_otchunk option\n"
  echo "type cwb_otchunk to list the available options"
  echo ""
  exit 1
endif

if ($CWB_OTCHUNK_DIR != "") then
  if (! -d $CWB_OTCHUNK_DIR ) then
    echo ""
    echo "Error: the output directory " "\n"
    echo "  "  \'$CWB_OTCHUNK_DIR\' "\n"
    echo "not initialized or not exist, check --dir option. Abort execution " "\n"
    echo ""
    exit 1
  endif
endif

# check if config stuff exist in the input config repository
${CWB_CONFIG}/SCRIPTS/cwb_ckchunk.csh --run=$CWB_OTCHUNK_RUN --chunk=$CWB_OTCHUNK_CHUNK --cal=$CWB_OTCHUNK_CAL --net=$CWB_OTCHUNK_NET --search=$CWB_OTCHUNK_SEARCH --type=BKG
if ( $? != 0) exit 1

echo "--------------------------------------------------------------------------------"
echo ""

setenv CWB_OTCHUNK_V_CHNAME ""
if ((( $CWB_OTCHUNK_CAL == 'C00' ))) then
  if ((( $CWB_OTCHUNK_RUN == 'O1' ))) then
    setenv CWB_OTCHUNK_CHNAME $O1_CWB_CONFIG_C00_CHNAME
  endif
  if ((( $CWB_OTCHUNK_RUN == 'O2' ))) then
    setenv CWB_OTCHUNK_CHNAME $O2_CWB_CONFIG_C00_CHNAME
  endif
  if ((( $CWB_OTCHUNK_RUN == 'O3' ))) then
    setenv CWB_OTCHUNK_CHNAME   $O3_CWB_CONFIG_C00_CHNAME
    setenv CWB_OTCHUNK_V_CHNAME $O3_CWB_CONFIG_C00_V_CHNAME
  endif
endif
if ((( $CWB_OTCHUNK_CAL == 'C00c' ))) then
  if ((( $CWB_OTCHUNK_RUN == 'O1' ))) then
    setenv CWB_OTCHUNK_CHNAME   $O1_CWB_CONFIG_C00c_CHNAME
  endif
  if ((( $CWB_OTCHUNK_RUN == 'O2' ))) then
    setenv CWB_OTCHUNK_CHNAME   $O2_CWB_CONFIG_C00c_CHNAME
  endif
  if ((( $CWB_OTCHUNK_RUN == 'O3' ))) then
    setenv CWB_OTCHUNK_CHNAME   $O3_CWB_CONFIG_C00c_CHNAME
    setenv CWB_OTCHUNK_V_CHNAME $O3_CWB_CONFIG_C00c_V_CHNAME
  endif
endif
if ((( $CWB_OTCHUNK_CAL == 'C01' ))) then
  if ((( $CWB_OTCHUNK_RUN == 'O1' ))) then
    setenv CWB_OTCHUNK_CHNAME   $O1_CWB_CONFIG_C01_CHNAME
  endif
  if ((( $CWB_OTCHUNK_RUN == 'O2' ))) then
    setenv CWB_OTCHUNK_CHNAME   $O2_CWB_CONFIG_C01_CHNAME
  endif
  if ((( $CWB_OTCHUNK_RUN == 'O3' ))) then
    setenv CWB_OTCHUNK_CHNAME   $O3_CWB_CONFIG_C01_CHNAME
    setenv CWB_OTCHUNK_V_CHNAME $O3_CWB_CONFIG_C01_V_CHNAME
  endif
endif
if ((( $CWB_OTCHUNK_CAL == 'C01c' ))) then
  if ((( $CWB_OTCHUNK_RUN == 'O1' ))) then
    setenv CWB_OTCHUNK_CHNAME   $O1_CWB_CONFIG_C01c_CHNAME
    setenv CWB_OTCHUNK_V_CHNAME $O1_CWB_CONFIG_C01c_V_CHNAME
  endif
  if ((( $CWB_OTCHUNK_RUN == 'O2' ))) then
    setenv CWB_OTCHUNK_CHNAME   $O2_CWB_CONFIG_C01c_CHNAME
    setenv CWB_OTCHUNK_V_CHNAME $O2_CWB_CONFIG_C01c_V_CHNAME
  endif
  if ((( $CWB_OTCHUNK_RUN == 'O3' ))) then
    setenv CWB_OTCHUNK_CHNAME   $O3_CWB_CONFIG_C01c_CHNAME
    setenv CWB_OTCHUNK_V_CHNAME $O3_CWB_CONFIG_C01c_V_CHNAME
  endif
endif
if ((( $CWB_OTCHUNK_CAL == 'C02' ))) then
  if ((( $CWB_OTCHUNK_RUN == 'O1' ))) then
    setenv CWB_OTCHUNK_CHNAME   $O1_CWB_CONFIG_C02_CHNAME
  endif
  if ((( $CWB_OTCHUNK_RUN == 'O2' ))) then
    setenv CWB_OTCHUNK_CHNAME   $O2_CWB_CONFIG_C02_CHNAME
  endif
  if ((( $CWB_OTCHUNK_RUN == 'O3' ))) then
    setenv CWB_OTCHUNK_CHNAME   $O3_CWB_CONFIG_C02_CHNAME
    setenv CWB_OTCHUNK_V_CHNAME $O3_CWB_CONFIG_C02_V_CHNAME
  endif
endif
if ((( $CWB_OTCHUNK_CAL == 'C02c' ))) then
  if ((( $CWB_OTCHUNK_RUN == 'O1' ))) then
    setenv CWB_OTCHUNK_CHNAME   $O1_CWB_CONFIG_C02c_CHNAME
    setenv CWB_OTCHUNK_V_CHNAME $O1_CWB_CONFIG_C02c_V_CHNAME
  endif
  if ((( $CWB_OTCHUNK_RUN == 'O2' ))) then
    setenv CWB_OTCHUNK_CHNAME   $O2_CWB_CONFIG_C02c_CHNAME
    setenv CWB_OTCHUNK_V_CHNAME $O2_CWB_CONFIG_C02c_V_CHNAME
  endif
  if ((( $CWB_OTCHUNK_RUN == 'O3' ))) then
    setenv CWB_OTCHUNK_CHNAME   $O3_CWB_CONFIG_C02c_CHNAME
    setenv CWB_OTCHUNK_V_CHNAME $O3_CWB_CONFIG_C02c_V_CHNAME
  endif
endif

setenv SN_PROD_CONDOR_TAG            "" 
setenv BURSTLF_PROD_CONDOR_TAG       "" 
setenv BURSTHF_PROD_CONDOR_TAG       "" 
setenv BURSTLD_PROD_CONDOR_TAG       "" 
setenv BBH_PROD_CONDOR_TAG           "" 
setenv IMBHB_PROD_CONDOR_TAG         "" 

# copy $CWB_OTCHUNK_UPARAMETERS_FILE file & replace parameters

set CWB_OTCHUNK_UPARAMETERS_FILE=$CWB_OTCHUNK_DIR/user_parameters_otchunk_tmp.C

if ( -f $CWB_OTCHUNK_UPARAMETERS_FILE ) then
  echo ""
  echo "the output temporary cfg file"
  echo ""
  echo "  "  \'$PWD/$CWB_OTCHUNK_UPARAMETERS_FILE\'
  echo ""
  echo "is already created. To continue, remove manually such file. Abort eecution"
  echo ""
  exit 1
endif

cp $CWB_CONFIG/$CWB_OTCHUNK_RUN/SEARCHES/OFFLINE/$CWB_OTCHUNK_SEARCH/$CWB_OTCHUNK_NET/BKG/user_parameters.C  $CWB_OTCHUNK_UPARAMETERS_FILE
echo "Created configuration file"  \'$CWB_OTCHUNK_UPARAMETERS_FILE\' "\n"

# Remove leading zeroes from CWB_OTCHUNK_CHUNK (08 -> 8)
set CWB_OTCHUNK_XCHUNK=`echo $CWB_OTCHUNK_CHUNK | sed 's/^0*//'`

sed -i "s/#SN_PROD_CONDOR_TAG/$SN_PROD_CONDOR_TAG/g"            $CWB_OTCHUNK_UPARAMETERS_FILE
sed -i "s/#BURSTLF_PROD_CONDOR_TAG/$BURSTLF_PROD_CONDOR_TAG/g"  $CWB_OTCHUNK_UPARAMETERS_FILE
sed -i "s/#BURSTHF_PROD_CONDOR_TAG/$BURSTHF_PROD_CONDOR_TAG/g"  $CWB_OTCHUNK_UPARAMETERS_FILE
sed -i "s/#BURSTLD_PROD_CONDOR_TAG/$BURSTLD_PROD_CONDOR_TAG/g"  $CWB_OTCHUNK_UPARAMETERS_FILE
sed -i "s/#BBH_PROD_CONDOR_TAG/$BBH_PROD_CONDOR_TAG/g"          $CWB_OTCHUNK_UPARAMETERS_FILE
sed -i "s/#IMBHB_PROD_CONDOR_TAG/$IMBHB_PROD_CONDOR_TAG/g"      $CWB_OTCHUNK_UPARAMETERS_FILE

sed -i "s/#CWB_CHUNK_NUMBER/$CWB_OTCHUNK_XCHUNK/g"              $CWB_OTCHUNK_UPARAMETERS_FILE
sed -i "s\#CWB_CALIB_VER\$CWB_OTCHUNK_CAL\g"                    $CWB_OTCHUNK_UPARAMETERS_FILE
sed -i "s\#CWB_CHANNEL_NAME\$CWB_OTCHUNK_CHNAME\g"              $CWB_OTCHUNK_UPARAMETERS_FILE
sed -i "s\#CWB_L_CHANNEL_NAME\$CWB_OTCHUNK_CHNAME\g"            $CWB_OTCHUNK_UPARAMETERS_FILE
sed -i "s\#CWB_H_CHANNEL_NAME\$CWB_OTCHUNK_CHNAME\g"            $CWB_OTCHUNK_UPARAMETERS_FILE
sed -i "s\#CWB_V_CHANNEL_NAME\$CWB_OTCHUNK_V_CHNAME\g"          $CWB_OTCHUNK_UPARAMETERS_FILE

setenv CWB_OTCHUNK_PARMS_FILES "${CWB_ROOTLOGON_FILE} ${CWB_PARAMETERS_FILE} ${CWB_OTCHUNK_UPARAMETERS_FILE} ${CWB_EPARAMETERS_FILE}"

# produce standard segments
root -n -l -b $CWB_OTCHUNK_PARMS_FILES $CWB_CONFIG/MACROS/ComputeJobProcDataTime.C\(\"standard\",\"$CWB_OTCHUNK_CHUNK\",\"$CWB_OTCHUNK_DIR\",\"$CWB_OTCHUNK_LABEL\"\)
if ( $? != 0) then 
  # abort execution
  if ( -f $CWB_OTCHUNK_UPARAMETERS_FILE ) then
    rm $CWB_OTCHUNK_UPARAMETERS_FILE
  endif 
  exit 1
endif 

# produce extended segments
root -n -l -b $CWB_OTCHUNK_PARMS_FILES $CWB_CONFIG/MACROS/ComputeJobProcDataTime.C\(\"extended\",\"$CWB_OTCHUNK_CHUNK\",\"$CWB_OTCHUNK_DIR\",\"$CWB_OTCHUNK_LABEL\"\)
if ( $? != 0) then 
  # abort execution
  if ( -f $CWB_OTCHUNK_UPARAMETERS_FILE ) then
    rm $CWB_OTCHUNK_UPARAMETERS_FILE
  endif 
  exit 1
endif 

if ( -f $CWB_OTCHUNK_UPARAMETERS_FILE ) then
  rm $CWB_OTCHUNK_UPARAMETERS_FILE
endif 

unsetenv CWB_OTCHUNK_RUN
unsetenv CWB_OTCHUNK_CHUNK
unsetenv CWB_OTCHUNK_CAL
unsetenv CWB_OTCHUNK_NET
unsetenv CWB_OTCHUNK_SEARCH
unsetenv CWB_OTCHUNK_DIR
unsetenv CWB_OTCHUNK_LABEL

unsetenv CWB_OTCHUNK_PARMS_FILES

exit 0
irq_ctrlc:
  ps T | grep root | awk '{print $1}' | xargs kill -9
  exit 1
