#!/bin/tcsh -f

onintr irq_ctrlc

if ( $1 == '' ) then
  $CWB_SCRIPTS/cwb_help.csh cwb_obchunk
  exit
endif

if ($1 == '') then
  echo ""
  echo 'cwb_obchunk --run="..." --search="..." --bin="..." --wlabel="..." --list="..." --trials="..." --bbh="..." --chunk="..." --lag="..." --slag="..." --merge="..." --opt="..."'
  echo ""
  echo "run     : O1, O2, O3 (def=O2)"
  echo "search  : BurstLF, BurstHF, BurstLD, IMBHB, BBH (def=BBH)"
  echo "bin     : 1,2,... -> bin1_cut, bin2_cut, ..."
  echo "wlabel  : working directory label -> O2_K03_C00_LH_BurstLF_BKG_dev1"
  echo "list    : list of root files used to merge together the chunks"
  echo "trials  : trials to applied to the detected events"
  echo "bbh     : true/false (def=true)"
  echo "chunk   : chunk number: integer number, Ex 02,03,...,15 "
  echo "merge   : merge id: integer number"
  echo "mlabel  : post merge label: Used when non standard labels are used in post-production"
  echo "          Ex: for M1.V_hvetoLH.C_bin1_cut it is V_hvetoLH.C_bin1_cut"
  echo "rlabel  : report label: Used when non standard labels are used in post-production reports"
  echo "          Ex: for M1.V_hvetoLH.C_bin1_cut.R_rMRA_hveto_i0cc00_i1rho0_freq16_512 it is R_rMRA_hveto_i0cc00_i1rho0_freq16_512"
  echo "lag     : lag number: integer number"
  echo "slag    : slag number: integer number"
  echo ""
  echo "opt     : optional"
  echo "          no options -> disable (default)"
  echo "          mkdir  : creates open box directory"
  echo "          report : creates report for lag e slag"
  echo "          ced    : submit CEDs"
  echo "          plot   : creates plots"
  echo "          html   : creates chunk html report"
  echo "          all    : mkdir+report+ced+plot+html"
  echo ""
  echo "question: if not blank then skip question -> Open Box Result, do you want to procede (y/n)? " 
  echo ""
  echo 'Ex: cwb_obchunk --wlabel="O2_K03_C00_LH_BurstLF_BKG_dev1" --chunk="03" --lag="1" --slag="0" --merge="1" --opt="create"'
  echo ""
  exit 1
endif

setenv CWB_OBCHUNK_RUN		"O2"
setenv CWB_OBCHUNK_NET   	""
setenv CWB_OBCHUNK_SEARCH	"BBH"
setenv CWB_OBCHUNK_BIN		1
setenv CWB_OBCHUNK_WLABEL	""
setenv CWB_OBCHUNK_LIST         ""
setenv CWB_OBCHUNK_TRIALS	1
setenv CWB_OBCHUNK_BBH		"true"
setenv CWB_OBCHUNK_CHUNK	""
setenv CWB_OBCHUNK_LAG		""
setenv CWB_OBCHUNK_SLAG		""
setenv CWB_OBCHUNK_MERGE	""
setenv CWB_OBCHUNK_MLABEL	""
setenv CWB_OBCHUNK_RLABEL	""
setenv CWB_OBCHUNK_OPTIONS	""
setenv CWB_OBCHUNK_QUESTION	""

set cmd_line="$0 $argv"

set temp=(`getopt -s tcsh -o r:n:S:b:w:c:l:s:m:M:R:o:q:B:L:T: --long run:,net:,search:,bin:,wlabel:,chunk:,lag:,slag:,merge:,mlabel:,rlabel:,opt:,question:,bbh:,list:,trials: -- $argv:q`)
if ($? != 0) then
  echo "Terminating..." >/dev/stderr
  exit 1
endif
eval set argv=\($temp:q\)

while (1)
        switch($1:q)
        case -r:
        case --run:
                setenv CWB_OBCHUNK_RUN          $2:q
                shift ; shift
                breaksw
        case -n:
        case --net:
                setenv CWB_OBCHUNK_NET          $2:q
                shift ; shift
                breaksw
        case -S:
        case --search:
                setenv CWB_OBCHUNK_SEARCH       $2:q
                shift ; shift
                breaksw
        case -b:
        case --bin:
                setenv CWB_OBCHUNK_BIN          $2:q
                shift ; shift
                breaksw
        case -w:
        case --wlabel:
                setenv CWB_OBCHUNK_WLABEL       $2:q
                shift ; shift
                breaksw
        case -L:
        case --list:
                setenv CWB_OBCHUNK_LIST         $2:q
                shift ; shift
                breaksw
        case -T:
        case --trials:
                setenv CWB_OBCHUNK_TRIALS       $2:q
                shift ; shift
                breaksw
        case -c:
        case --chunk:
                setenv CWB_OBCHUNK_CHUNK        $2:q
                shift ; shift
                breaksw
        case -l:
        case --lag:
                setenv CWB_OBCHUNK_LAG          $2:q
                shift ; shift
                breaksw
        case -s:
        case --slag:
                setenv CWB_OBCHUNK_SLAG         $2:q
                shift ; shift
                breaksw
        case -m:
        case --merge:
                setenv CWB_OBCHUNK_MERGE        $2:q
                shift ; shift
                breaksw
        case -M:
        case --mlabel:
                setenv CWB_OBCHUNK_MLABEL       $2:q
                shift ; shift
                breaksw
        case -R:
        case --rlabel:
                setenv CWB_OBCHUNK_RLABEL       $2:q
                shift ; shift
                breaksw
        case -o:
        case --opt:
                setenv CWB_OBCHUNK_OPTIONS      $2:q
                shift ; shift
                breaksw
        case -q:
        case --question:
                setenv CWB_OBCHUNK_QUESTION     $2:q
                shift ; shift
                breaksw
        case -B:
        case --bbh:
                setenv CWB_OBCHUNK_BBH     	$2:q
                shift ; shift
                breaksw
        case --:
                shift
                break
        default:
                echo "error - missing parameters!" ; exit 1
        endsw
end

if ((( $CWB_OBCHUNK_RUN != 'O1' ) && ( $CWB_OBCHUNK_RUN != 'O2' ) && ( $CWB_OBCHUNK_RUN != 'O3' ))) then
  echo ""
  echo --run=\'$CWB_OBCHUNK_RUN\' "is a wrong cwb_obchunk option\n"
  echo "type cwb_obchunk to list the available options"
  echo ""
  exit 1
endif

if ((( $CWB_OBCHUNK_SEARCH != 'BurstLF' ) && ( $CWB_OBCHUNK_SEARCH != 'BurstHF' ) && ( $CWB_OBCHUNK_SEARCH != 'BurstLD' ) && ( $CWB_OBCHUNK_SEARCH != 'IMBHB' ) && ( $CWB_OBCHUNK_SEARCH != 'BBH' ))) then
  echo ""
  echo --search=\'$CWB_OBCHUNK_SEARCH\' "is a wrong cwb_obchunk option\n"
  echo "type cwb_obchunk to list the available options"
  echo ""
  exit 1
endif

if ( $CWB_OBCHUNK_BIN !~ ^[0-9]+$ ) then
  echo ""
  echo --bin=\'$CWB_OBCHUNK_BIN\' "is a wrong cwb_obchunk option\n"
  echo "type cwb_obchunk to list the available options"
  echo ""
  exit 1
endif 

if ( $CWB_OBCHUNK_TRIALS !~ ^[0-9]+$ ) then
  echo ""
  echo --trials=\'$CWB_OBCHUNK_TRIALS\' "is a wrong cwb_obchunk option\n"
  echo "type cwb_obchunk to list the available options"
  echo ""
  exit 1
endif 

if (! -d $CWB_OBCHUNK_WLABEL ) then
  echo ""
  echo --wlabel=\'$CWB_OBCHUNK_WLABEL\' "working directory do not exist\n"
  echo "type cwb_obchunk to list the available options"
  echo ""
  exit 1
endif

if ((( $CWB_OBCHUNK_BBH != 'true' ) && ( $CWB_OBCHUNK_BBH != 'false' ))) then
  echo ""
  echo --run=\'$CWB_OBCHUNK_BBH\' "is a wrong cwb_obchunk option\n"
  echo "type cwb_obchunk to list the available options"
  echo ""
  exit 1
endif

if ( $CWB_OBCHUNK_CHUNK !~ ^[0-9]+$ ) then
  echo ""
  echo --chunk=\'$CWB_OBCHUNK_CHUNK\' "is a wrong cwb_obchunk option\n"
  echo "type cwb_obchunk to list the available options"
  echo ""
  exit 1
endif 

if ( $CWB_OBCHUNK_MERGE !~ ^[0-9]+$ ) then
  echo ""
  echo --merge=\'$CWB_OBCHUNK_MERGE\' "is not an integer\n"
  echo "type cwb_obchunk to list the available options"
  echo ""
  exit 1
endif 

if ( $CWB_OBCHUNK_LAG !~ ^[0-9]+$ ) then
  echo ""
  echo --lag=\'$CWB_OBCHUNK_LAG\' "is not an integer\n"
  echo "type cwb_obchunk to list the available options"
  echo ""
  exit 1
endif 

if ( $CWB_OBCHUNK_SLAG !~ ^[0-9]+$ ) then
  echo ""
  echo --slag=\'$CWB_OBCHUNK_SLAG\' "is not an integer\n"
  echo "type cwb_obchunk to list the available options"
  echo ""
  exit 1
endif 

if ((( $CWB_OBCHUNK_OPTIONS != 'mkdir' ) && ( $CWB_OBCHUNK_OPTIONS != 'plot' ) && ( $CWB_OBCHUNK_OPTIONS != 'report' ) && ( $CWB_OBCHUNK_OPTIONS != 'ced' ) && ( $CWB_OBCHUNK_OPTIONS != 'html' ) && ( $CWB_OBCHUNK_OPTIONS != 'all' ))) then
  echo ""
  echo --opt=\'$CWB_OBCHUNK_OPTIONS\' "is a wrong cwb_obchunk option\n"
  echo "type cwb_obchunk to list the available options"
  echo ""
  exit 1
endif

if (( $CWB_OBCHUNK_LAG == 0 ) && ( $CWB_OBCHUNK_SLAG == 0 ) && ( $CWB_OBCHUNK_QUESTION == "" )) then
  echo "Open Box Result, do you want to procede (y/n)?"
  set answer = $<
  if ( $answer == "n" ) then
    echo ""
    exit 1
  endif
endif

if ( $CWB_OBCHUNK_MLABEL == '' ) then
  set PMERGE_LABEL = V_hvetoLH.C_bin$CWB_OBCHUNK_BIN\_cut
else
  set PMERGE_LABEL = $CWB_OBCHUNK_MLABEL
endif

if ( $CWB_OBCHUNK_SEARCH == 'BurstLF' ) then
  if ( $CWB_OBCHUNK_RLABEL == '' ) then
    set PP_LABEL = $PMERGE_LABEL.R_rMRA_hveto_i0cc00_i0rho0_freq16_1024
  else 
    set PP_LABEL = $PMERGE_LABEL.$CWB_OBCHUNK_RLABEL
  endif
  set DIR_REP = report/dump/M$CWB_OBCHUNK_MERGE.$PMERGE_LABEL.Box_Result_Bin$CWB_OBCHUNK_BIN\_Lag$CWB_OBCHUNK_LAG\_Slag$CWB_OBCHUNK_SLAG
endif

if ( $CWB_OBCHUNK_SEARCH == 'BurstHF' ) then
  if ( $CWB_OBCHUNK_RLABEL == '' ) then
    set PP_LABEL = $PMERGE_LABEL.R_rMRA_hveto_i0cc00_i0rho0_freq512_4096
  else 
    set PP_LABEL = $PMERGE_LABEL.$CWB_OBCHUNK_RLABEL
  endif
  set DIR_REP = report/dump/M$CWB_OBCHUNK_MERGE.$PMERGE_LABEL.Box_Result_Bin$CWB_OBCHUNK_BIN\_Lag$CWB_OBCHUNK_LAG\_Slag$CWB_OBCHUNK_SLAG
endif

if ( $CWB_OBCHUNK_SEARCH == 'BurstLD' ) then
  if ( $CWB_OBCHUNK_RLABEL == '' ) then
    set PP_LABEL = $PMERGE_LABEL.R_rMRA_hveto_i0cc00_i0rho0_freq16_2048
  else 
    set PP_LABEL = $PMERGE_LABEL.$CWB_OBCHUNK_RLABEL
  endif
  set DIR_REP = report/dump/M$CWB_OBCHUNK_MERGE.$PMERGE_LABEL.Box_Result_Bin$CWB_OBCHUNK_BIN\_Lag$CWB_OBCHUNK_LAG\_Slag$CWB_OBCHUNK_SLAG
endif

if (( $CWB_OBCHUNK_SEARCH == 'IMBHB' ) || ( $CWB_OBCHUNK_SEARCH == 'BBH' )) then
  if ( $CWB_OBCHUNK_RLABEL == '' ) then
    set PP_LABEL = $PMERGE_LABEL.R_rMRA_hveto_i0cc00_i1rho0_freq16_512
  else 
    set PP_LABEL = $PMERGE_LABEL.$CWB_OBCHUNK_RLABEL
  endif
  set DIR_REP = report/dump/M$CWB_OBCHUNK_MERGE.$PMERGE_LABEL.Box_Result_Lag$CWB_OBCHUNK_LAG\_Slag$CWB_OBCHUNK_SLAG
endif

set DIR_PWD = $PWD


if (( $CWB_OBCHUNK_OPTIONS == 'plot' ) && ( $CWB_OBCHUNK_LIST != '' ) && ( $CWB_OBCHUNK_CHUNK == 99 )) then    # create IFAR plots using a list of output root files
  root -l -b -q $CWB_CONFIG/MACROS/Make_PP_IFAR.C\(\"$CWB_OBCHUNK_LIST\",\"--search=$CWB_OBCHUNK_SEARCH\:bin$CWB_OBCHUNK_BIN\ --run=$CWB_OBCHUNK_RUN\ --bbh=true\ --trials=$CWB_OBCHUNK_TRIALS\ --lag=$CWB_OBCHUNK_LAG\ --slag=$CWB_OBCHUNK_SLAG\ --chunk=$CWB_OBCHUNK_CHUNK\ --pfname=$DIR_PWD/$CWB_OBCHUNK_WLABEL/$DIR_REP/CumulativeNumberVsIFAR.png\"\)
  root -l -b -q $CWB_CONFIG/MACROS/Make_PP_IFAR.C\(\"$CWB_OBCHUNK_LIST\",\"--search=$CWB_OBCHUNK_SEARCH\:bin$CWB_OBCHUNK_BIN\ --run=$CWB_OBCHUNK_RUN\ --bbh=false\ --trials=$CWB_OBCHUNK_TRIALS\ --lag=$CWB_OBCHUNK_LAG\ --slag=$CWB_OBCHUNK_SLAG\ --chunk=$CWB_OBCHUNK_CHUNK\ --pfname=$DIR_PWD/$CWB_OBCHUNK_WLABEL/$DIR_REP/CumulativeNumberVsIFAR.png\"\)
  exit 0
endif
if (( $CWB_OBCHUNK_OPTIONS == 'html' ) && ( $CWB_OBCHUNK_LIST != '' ) && ( $CWB_OBCHUNK_CHUNK == 99 )) then    # create html using a list of output root files
  root -l -b -q $CWB_CONFIG/MACROS/cwb_mkhtml_all.C\(\"$CWB_OBCHUNK_LIST\",\"$CWB_OBCHUNK_RUN\",\"$CWB_OBCHUNK_RUN\:$CWB_OBCHUNK_NET\:$CWB_OBCHUNK_SEARCH\:Bin$CWB_OBCHUNK_BIN\",$CWB_OBCHUNK_LAG,$CWB_OBCHUNK_SLAG,\"$CWB_OBCHUNK_WLABEL\",\"$DIR_PWD/$CWB_OBCHUNK_WLABEL/$DIR_REP/CumulativeNumberVsIFAR.png\"\)
  exit 0
endif


set DIR_BKG = postprod/M$CWB_OBCHUNK_MERGE.$PP_LABEL
set DIR_FRG = postprod/M$CWB_OBCHUNK_MERGE.$PP_LABEL\_lag$CWB_OBCHUNK_LAG\_slag$CWB_OBCHUNK_SLAG

if ("$PMERGE_LABEL" !~ "*.S_*") then
  set WAVE_FILE     = ../../../merge/wave_$CWB_OBCHUNK_WLABEL.M$CWB_OBCHUNK_MERGE.$PMERGE_LABEL.S_ifar.root
else
  set WAVE_FILE     = ../../../merge/wave_$CWB_OBCHUNK_WLABEL.M$CWB_OBCHUNK_MERGE.$PMERGE_LABEL.root
endif
set RHO_FREQ_FILE   = ../../$DIR_BKG/data/rho_frequency.gif
set RHO_TIME_FILE   = ../../$DIR_BKG/data/rho_time.gif

if (( $CWB_OBCHUNK_OPTIONS == 'mkdir' ) || ( $CWB_OBCHUNK_OPTIONS == 'all' )) then
  echo $CWB_OBCHUNK_WLABEL/$DIR_REP
  mkdir $CWB_OBCHUNK_WLABEL/$DIR_REP
endif

if (( $CWB_OBCHUNK_OPTIONS == 'report' ) || ( $CWB_OBCHUNK_OPTIONS == 'all' )) then
  cd $CWB_OBCHUNK_WLABEL
  ${CWB_SCRIPTS}/cwb_report.csh M$CWB_OBCHUNK_MERGE.$PMERGE_LABEL create $CWB_OBCHUNK_LAG $CWB_OBCHUNK_SLAG
endif

set CED_DAG = condor/$CWB_OBCHUNK_WLABEL.M$CWB_OBCHUNK_MERGE.$PP_LABEL.ced.dag

if (( $CWB_OBCHUNK_OPTIONS == 'ced' ) || ( $CWB_OBCHUNK_OPTIONS == 'all' )) then
  cd $DIR_PWD/$CWB_OBCHUNK_WLABEL
  if ( $? != 0) then
    echo "do you wat to force submit (y/n)?"
    set answer = $<
    if ( $answer == "n" ) then
      echo ""
      exit 1
    endif
  endif
  rm $CED_DAG.*
  ${CWB_SCRIPTS}/cwb_condor.csh submit $CED_DAG
endif

if (( $CWB_OBCHUNK_OPTIONS == 'plot' ) || ( $CWB_OBCHUNK_OPTIONS == 'all' )) then

  if (! -f $DIR_PWD/$CWB_OBCHUNK_WLABEL/$DIR_REP/$WAVE_FILE ) then
    echo ""
    echo $DIR_PWD/$CWB_OBCHUNK_WLABEL/$DIR_REP/$WAVE_FILE " not exist\n"
    echo ""
    exit 1
  endif

  cd $DIR_PWD/$CWB_OBCHUNK_WLABEL/$DIR_REP
  ln -sf $RHO_TIME_FILE
  ln -sf $RHO_FREQ_FILE

  root -l -b $CWB_CONFIG/MACROS/Make_PP_IFAR.C\(\"$WAVE_FILE\",\"--search=$CWB_OBCHUNK_SEARCH\:bin$CWB_OBCHUNK_BIN\ --run=$CWB_OBCHUNK_RUN\ --bbh=$CWB_OBCHUNK_BBH\ --lag=$CWB_OBCHUNK_LAG\ --slag=$CWB_OBCHUNK_SLAG\ --chunk=$CWB_OBCHUNK_CHUNK\ --pfname=CumulativeNumberVsIFAR.png\"\)
  root -l -b $CWB_CONFIG/MACROS/Draw_FARvsRHO.C\(\"../../$DIR_BKG/data/far_rho.txt\",\"../../$DIR_FRG/data/far_rho.txt\",\"FARvsRank\"\)
endif

if (( $CWB_OBCHUNK_OPTIONS == 'html' ) || ( $CWB_OBCHUNK_OPTIONS == 'all' )) then
  cd $DIR_PWD/$CWB_OBCHUNK_WLABEL/$DIR_REP

  # Remove leading zeroes from CWB_OBCHUNK_CHUNK (08 -> 8)
  set CWB_OBCHUNK_XCHUNK=`echo $CWB_OBCHUNK_CHUNK | sed 's/^0*//'`

  root -l -b $CWB_CONFIG/MACROS/cwb_mkhtml_chunk.C\(\"$CWB_OBCHUNK_RUN\",\"$CWB_OBCHUNK_SEARCH\",\"$CWB_OBCHUNK_BIN\",$CWB_OBCHUNK_XCHUNK,$CWB_OBCHUNK_BBH,\"$DIR_BKG\",\"$DIR_FRG\",\"$DIR_PWD/$CWB_OBCHUNK_WLABEL/$DIR_REP\",\"$CWB_OBCHUNK_WLABEL\",\"$WAVE_FILE\",$CWB_OBCHUNK_LAG,$CWB_OBCHUNK_SLAG,2\)
endif

# create cWB_config.log file
# make -f $CWB_CONFIG/Makefile.log CMD_LINE="$cmd_line" git >& /dev/null

cd $DIR_PWD

unsetenv CWB_OBCHUNK_RUN
unsetenv CWB_OBCHUNK_NET
unsetenv CWB_OBCHUNK_SEARCH
unsetenv CWB_OBCHUNK_BIN
unsetenv CWB_OBCHUNK_WLABEL
unsetenv CWB_OBCHUNK_CHUNK
unsetenv CWB_OBCHUNK_LAG
unsetenv CWB_OBCHUNK_SLAG
unsetenv CWB_OBCHUNK_MERGE
unsetenv CWB_OBCHUNK_MLABEL
unsetenv CWB_OBCHUNK_RLABEL
unsetenv CWB_OBCHUNK_OPTIONS
unsetenv CWB_OBCHUNK_QUESTION

exit 0
irq_ctrlc:
  ps T | grep root | awk '{print $1}' | xargs kill -9
  exit 1
