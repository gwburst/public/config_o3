#!/bin/tcsh -f

onintr irq_ctrlc

if ( $1 == '' ) then
  $CWB_SCRIPTS/cwb_help.csh cwb_lschunk
  exit
endif

if ($1 == '') then
  echo ""
  echo 'cwb_lschunk --run="..." --search="..."'
  echo ""
  echo "run     : O1, O2, O3"
  echo "search  : BBH, IMBHB, BurstLF, BurstHF, BurstLD, by default it list the CBC chunks"
  echo ""
  echo "Ex: cwb_lschunk --run O2"
  echo "Ex: cwb_lschunk --run O2 --search BBH"
  echo ""
  exit 1
endif

setenv CWB_LSCHUNK_RUN          ""
setenv CWB_LSCHUNK_SEARCH       ""

set temp=(`getopt -s tcsh -o r:s: --long run:,search: -- $argv:q`)
if ($? != 0) then
  echo "Terminating..." >/dev/stderr
  exit 1
endif
eval set argv=\($temp:q\)

while (1)
        switch($1:q)
        case -r:
        case --run:
                setenv CWB_LSCHUNK_RUN          $2:q
                shift ; shift
                breaksw
        case -s:
        case --search:
                setenv CWB_LSCHUNK_SEARCH       $2:q
                shift ; shift
                breaksw
        case --:
                shift
                break
        default:
                echo "error - missing parameters!" ; exit 1
        endsw
end

if ((( $CWB_LSCHUNK_RUN == '' ))) then
  echo ""
  echo "Error: empty input working directory"
  echo "type cwb_lschunk to list the available options"
  echo ""
  exit 1
endif

root -l -b '${CWB_CONFIG}/MACROS/cwb_lschunk.C("'$CWB_LSCHUNK_RUN'","'$CWB_LSCHUNK_SEARCH'")'

unsetenv CWB_LSCHUNK_RUN
unsetenv CWB_LSCHUNK_SEARCH

exit 0
irq_ctrlc:
  ps T | grep root | awk '{print $1}' | xargs kill -9
  exit 1
