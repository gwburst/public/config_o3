#!/bin/tcsh -f

onintr irq_ctrlc

#if ( $1 == '' ) then
#  $CWB_SCRIPTS/cwb_help.csh cwb_ckchunk
#  exit
#endif

if ($1 == '') then
  echo ""
  echo "-------------------------------------------------------------------------------------------------------------------"
  echo "What it does: check if the chunk parameters are in agreement with the setting provided in the git config repository"
  echo "-------------------------------------------------------------------------------------------------------------------"
  echo ""
  echo 'cwb_ckchunk --run=... --chunk=... --cal=... --net=... --search=... --type=...'
  echo ""
  echo "run     : O1, O2, O3"
  echo "chunk   : integer number"
  echo "cal     : C00, C00c, C01, C01c, C02, C02c"
  echo "net     : LH LV HV LHV"
  echo "search  : BurstLF, BurstHF, BurstLD, IMBHB, BBH"
  echo "type    : BKG, SIM"
  echo ""
  echo 'Ex: cwb_ckchunk --run=O2 --chunk=03 --cal=C00 --net=LH --search=BurstLF --type=BKG'
  echo ""
  echo "cwb_ckchunk without arguments shows help and git config status"
  echo ""
  echo "----------------------------------------------------------------------------------"
  echo ""
  echo "The local cWB config git repository is:"
  echo ""
  echo " " $CWB_CONFIG
  echo ""
  echo "and the config symbolic links are:"
  echo ""
  if ( -d $CWB_CONFIG/O3 ) then
    echo -n " " $CWB_CONFIG/O3 " -> "
    realpath $CWB_CONFIG/O3 
  else
    echo "  link " \'$CWB_CONFIG/O3\' " doesn't exist !"
  endif
  echo ""
  if ( -d $CWB_CONFIG/DATA ) then
    echo -n " " $CWB_CONFIG/DATA " -> "
    realpath $CWB_CONFIG/DATA 
  else
    echo "  link " \'$CWB_CONFIG/DATA\' " doesn't exist !"
  endif
  echo ""
  if ( -d $CWB_CONFIG/DATA/O3 ) then
    echo -n " " $CWB_CONFIG/DATA/O3 " -> "
    realpath $CWB_CONFIG/DATA/O3 
  else
    echo "  link " \'$CWB_CONFIG/DATA/O3\' " doesn't exist !"
  endif
  echo ""
  exit 1
endif

setenv CWB_CKCHUNK_RUN
setenv CWB_CKCHUNK_CHUNK
setenv CWB_CKCHUNK_CAL
setenv CWB_CKCHUNK_NET
setenv CWB_CKCHUNK_SEARCH
setenv CWB_CKCHUNK_TYPE

set cmd_line="$0 $argv"

set temp=(`getopt -s tcsh -o r:c:C:n:s:t: --long run:,chunk:,cal:,net:,search:,type: -- $argv:q`)
if ($? != 0) then
  echo "Terminating..." >/dev/stderr
  exit 1
endif
eval set argv=\($temp:q\)

while (1)
        switch($1:q)
        case -r:
        case --run:
                setenv CWB_CKCHUNK_RUN          $2:q
                shift ; shift
                breaksw
        case -c:
        case --chunk:
                setenv CWB_CKCHUNK_CHUNK        $2:q
                shift ; shift
                breaksw
        case -C:
        case --cal:
                setenv CWB_CKCHUNK_CAL          $2:q
                shift ; shift
                breaksw
        case -n:
        case --net:
                setenv CWB_CKCHUNK_NET          $2:q
                shift ; shift
                breaksw
        case -s:
        case --search:
                setenv CWB_CKCHUNK_SEARCH       $2:q
                shift ; shift
                breaksw
        case -t:
        case --type:
                setenv CWB_CKCHUNK_TYPE         $2:q
                shift ; shift
                breaksw
        case --:
                shift
                break
        default:
                echo "error - missing parameters!" ; exit 1
        endsw
end

if ((( $CWB_CKCHUNK_RUN != 'O1' ) && ( $CWB_CKCHUNK_RUN != 'O2' ) && ( $CWB_CKCHUNK_RUN != 'O3' ))) then
  echo ""
  echo --run=\'$CWB_CKCHUNK_RUN\' "is a wrong cwb_ckchunk option\n"
  echo "type cwb_ckchunk to list the available options"
  echo ""
  exit 1
endif

if ( $CWB_CKCHUNK_CHUNK !~ ^[0-9]+$ ) then
  echo ""
  echo --chunk=\'$CWB_CKCHUNK_CHUNK\' "is a wrong cwb_ckchunk option\n"
  echo "type cwb_ckchunk to list the available options"
  echo ""
  exit 1
endif 

if ((( $CWB_CKCHUNK_CAL != 'C00' ) && ( $CWB_CKCHUNK_CAL != 'C00c' ) && ( $CWB_CKCHUNK_CAL != 'C01' ) && ( $CWB_CKCHUNK_CAL != 'C01c' ) && ( $CWB_CKCHUNK_CAL != 'C02' ) && ( $CWB_CKCHUNK_CAL != 'C02c' ))) then
  echo ""
  echo --cal=\'$CWB_CKCHUNK_CAL\' "is a wrong cwb_ckchunk option\n"
  echo "type cwb_ckchunk to list the available options"
  echo ""
  exit 1
endif

if ((( $CWB_CKCHUNK_NET != 'LH' ) && ( $CWB_CKCHUNK_NET != 'LV' ) && ( $CWB_CKCHUNK_NET != 'HV' ) && ( $CWB_CKCHUNK_NET != 'LHV' ))) then
  echo ""
  echo --net=\'$CWB_CKCHUNK_NET\' "is a wrong cwb_ckchunk option\n"
  echo "type cwb_ckchunk to list the available options"
  echo ""
  exit 1
endif

if ((( $CWB_CKCHUNK_SEARCH != 'BurstLF' ) && ( $CWB_CKCHUNK_SEARCH != 'BurstHF' ) && ( $CWB_CKCHUNK_SEARCH != 'BurstLD' ) && ( $CWB_CKCHUNK_SEARCH != 'IMBHB' ) && ( $CWB_CKCHUNK_SEARCH != 'BBH' ))) then
  echo ""
  echo --search=\'$CWB_CKCHUNK_SEARCH\' "is a wrong cwb_ckchunk option\n"
  echo "type cwb_ckchunk to list the available options"
  echo ""
  exit 1
endif

if ((( $CWB_CKCHUNK_TYPE == 'BKG' ) || ( $CWB_CKCHUNK_TYPE =~ 'SIM/*' ))) then
  setenv CWB_CKCHUNK_TYPE $CWB_CKCHUNK_TYPE
  set CWB_CKCHUNK_SIM=$CWB_CKCHUNK_TYPE
  set CWB_CKCHUNK_SIM_DIR=`echo $CWB_CKCHUNK_SIM | awk '{print substr($0, 5, length($0)-1)}'`
else
  echo ""
  echo --type=\'$CWB_CKCHUNK_TYPE\' "is a wrong cwb_ckchunk option\n"
  echo "type cwb_ckchunk to list the available options"
  echo ""
  exit 1
endif

echo ""
echo "--------------------------------------------------------------------------------"
echo "Check input configuration stuff ..."
echo "--------------------------------------------------------------------------------"

if (( $CWB_CKCHUNK_TYPE =~ 'SIM/ONSPE' ) || ( $CWB_CKCHUNK_TYPE =~ 'SIM/OFSPE' )) then
  setenv CWB_CKCHUNK_PERIOD $CWB_CONFIG/$CWB_CKCHUNK_RUN/CHUNKS/K$CWB_CKCHUNK_CHUNK.period
else
  setenv CWB_CKCHUNK_PERIOD $CWB_CONFIG/$CWB_CKCHUNK_RUN/CHUNKS/$CWB_CKCHUNK_SEARCH/K$CWB_CKCHUNK_CHUNK.period
endif

if (! -f $CWB_CKCHUNK_PERIOD ) then
  echo ""
  echo "the chunk period " "\n"
  echo "  "  \'$CWB_CKCHUNK_PERIOD\' "\n"
  echo "is not present in the config repository " "\n"
  echo "  "  \'$CWB_CONFIG\' "\n"
  echo ""
  echo "The available chunk periods for search $CWB_CKCHUNK_SEARCH are:"
  echo ""
  if (( $CWB_CKCHUNK_TYPE =~ 'SIM/ONSPE' ) || ( $CWB_CKCHUNK_TYPE =~ 'SIM/OFSPE' )) then
    ls -1 $CWB_CONFIG/$CWB_CKCHUNK_RUN/CHUNKS/K*.period | xargs realpath
  else
    ls -1 $CWB_CONFIG/$CWB_CKCHUNK_RUN/CHUNKS/$CWB_CKCHUNK_SEARCH/K*.period | xargs realpath
  endif
  echo ""
  echo "abort execution " "\n"
  echo ""
  exit 1
endif

setenv CWB_CKCHUNK_DIR $CWB_CONFIG/$CWB_CKCHUNK_RUN/SEARCHES/OFFLINE/$CWB_CKCHUNK_SEARCH/$CWB_CKCHUNK_NET/$CWB_CKCHUNK_TYPE

if (! -d $CWB_CKCHUNK_DIR ) then
  echo ""
  echo "the chunk input config directory " "\n"
  echo "  "  \'$CWB_CKCHUNK_DIR\' "\n"
  echo "is not present in the config repository " "\n"
  echo "  "  \'$CWB_CONFIG\' "\n"
  echo ""
  echo "abort execution " "\n"
  echo ""
  exit 1
endif

setenv CWB_CKCHUNK_DATA $CWB_CONFIG/DATA/$CWB_CKCHUNK_RUN/DATA/$CWB_CKCHUNK_CAL

if (! -d $CWB_CKCHUNK_DATA ) then
  echo ""
  echo "the chunk data config directory " "\n"
  echo "  "  \'$CWB_CKCHUNK_DATA\' "\n"
  echo "is not present in the config repository " "\n"
  echo "  "  \'$CWB_CONFIG\' "\n"
  echo ""
  echo "abort execution " "\n"
  echo ""
  exit 1
endif

echo ''
echo 'Check Passed: the chunk parameters are in agreement with the setting provided in the git config repository:' "\n"
echo "  "  \'$CWB_CONFIG\' "\n"
echo ''

unsetenv CWB_CKCHUNK_RUN
unsetenv CWB_CKCHUNK_CHUNK
unsetenv CWB_CKCHUNK_CAL
unsetenv CWB_CKCHUNK_NET
unsetenv CWB_CKCHUNK_SEARCH
unsetenv CWB_CKCHUNK_TYPE

unsetenv CWB_CKCHUNK_PERIOD
unsetenv CWB_CKCHUNK_DIR
unsetenv CWB_CKCHUNK_DATA

exit 0
irq_ctrlc:
  ps T | grep root | awk '{print $1}' | xargs kill -9
  exit 1
