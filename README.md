# cWB Pipeline Configuration Files #

This repository contains all publicly avalable cWB configuration files in order to
run cWB searches on *O1, O2 & O3a/O3b data*.

## O1, O2, O3 Data ##

The *O1, O2, O3a/O3b data* files are:

 * DQ: Data Quality
 * FRAMES: frame file lists
 * SLAG: time shifts' list used for background estimation

data files are specific to:
 
 * data calibration (Ex: C00, C00c, C01, C01c, C02, C02c, ...)
 * clusters used for analysis (Ex: CIT, ATLAS, ...) 
 * collaboration groups (Ex: LVC, AEI, Public, ...)

For each specific case there is a dedicated DATA directory. 
Only *GWOSC* is included in the public config repository, such directory 
refer to the [GWOSC public data](https://www.gw-openscience.org/data/).
To set such DATA for analysis do:

```bash
  # after the config installation do:
  cd config
  make DATA=GWOSC O3=O3a
  or
  make DATA=GWOSC O3=O3b
```

For *LVC* collaboration (private) there is a specific DATA repository. 
To install and set such DATA for analysis do:

```bash
  # after the config installation do:
  cd config
  git clone -b LVC git@git.ligo.org:cWB/config.git LVC
  make DATA=LVC O3=O3a
  or
  make DATA=LVC O3=O3b
```

To cleanup symbolic links do:

```bash
  cd config
  make DATA=LVC O3=O3a clean
  or
  make DATA=LVC O3=O3b clean
```

=======================================================================

## Git command line fundamentals: 
 * [git cheat sheet](https://services.github.com/on-demand/downloads/github-git-cheat-sheet/) 
 * [youtube video for beginners (~30 mins)](https://www.youtube.com/watch?v=HVsySz-h9r4)

## Cloning cWB config repository

    git clone git@gitlab.com:gwburst/public/config.git


Large files, e.g. xml files are stored via git LFS

## Install Git LFS

*If you already have Git LFS installed please read this section to ensure Git
LFS is configured as required*

Download and install [Git LFS](https://git-lfs.github.com/) for your system.
You need to run the following command to setup the command line extension and you only have to do this once.

```bash
git lfs install --skip-smudge
```

The `--skip-smudge` option ensures that no files under LFS control are
downloaded automatically when cloning the repository or checking out new
commits containing LFS controlled files.[**Global automatic downloads with local manual downloads**](#global-automatic-downloads-with-local-manual-downloads).
We are now ready for cloning the repository.

## Downloading git lfs files

To download a specific file run the following command

```bash
git lfs pull -I <file-path>
```

This will download specific file identified by `<file-path>` or you can use a
glob pattern to download a batch of files. Note `<file-path>` has to be the full path relative to the base git repository. e.g.

```bash
git lfs pull -I O1/SEARCHES/OFFLINE/BBH/LH/SIM/BBH_O1_ALLSKY/XML/HL-INJECTIONS_78901_BBH1_cWB-1126051217-11206883.xml
```

You can also use glob patterns to download multiple files. Such arguments must
be wrapped in quotation marks

```bash
git lfs pull -I "O1/SEARCHES/OFFLINE/BBH/LH/SIM/BBH_O1_ALLSKY/XML/*.xml"
```
