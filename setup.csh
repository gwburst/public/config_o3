#!/bin/tcsh -f

alias cwb_mkchunk          "${CWB_CONFIG}/SCRIPTS/cwb_mkchunk.csh"   #_SKIP_csh2sh_

alias cwb_ckchunk          "${CWB_CONFIG}/SCRIPTS/cwb_ckchunk.csh"   #_SKIP_csh2sh_

alias cwb_clchunk          "${CWB_CONFIG}/SCRIPTS/cwb_clchunk.csh"   #_SKIP_csh2sh_

alias cwb_ppchunk          "${CWB_CONFIG}/SCRIPTS/cwb_ppchunk.csh"   #_SKIP_csh2sh_

alias cwb_lschunk          "${CWB_CONFIG}/SCRIPTS/cwb_lschunk.csh"   #_SKIP_csh2sh_

alias cwb_mksdirs          "${CWB_CONFIG}/SCRIPTS/cwb_mksdirs.csh"   #_SKIP_csh2sh_

alias cwb_obchunk          "${CWB_CONFIG}/SCRIPTS/cwb_obchunk.csh"   #_SKIP_csh2sh_

alias cwb_mklinks          "${CWB_CONFIG}/SCRIPTS/cwb_mklinks.csh"   #_SKIP_csh2sh_

alias cwb_otchunk          "${CWB_CONFIG}/SCRIPTS/cwb_otchunk.csh"   #_SKIP_csh2sh_

source ${CWB_CONFIG}/O1/setup.csh	# O1 configuration 
source ${CWB_CONFIG}/O2/setup.csh	# O2 configuration 
source ${CWB_CONFIG}/O3/setup.csh	# O3 configuration 
