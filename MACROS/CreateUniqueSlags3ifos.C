/*
# Copyright (C) 2019 Gabriele Vedovato
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


void CreateUniqueSlags3ifos(TString odir, int nSlags=9999) {

  if(nSlags%2==0) {
    cout << "CreateUniqueSlags3ifos - Error : nSlags must be odd !!!" << endl;
    exit(1);
  } 

  char ofname[1024];
  sprintf(ofname,"%s/Unique_n%d.txt",odir.Data(),nSlags);

  ofstream out;
  out.open(ofname,ios::out);
  if(!out.good()) {cout << "CreateUniqueSlags3ifos - Error : Opening File : " << ofname << endl;gSystem->Exit(1);}

  out << "#  0 - SLAG number" << endl;
  out << "#  1 - slag[0]" << endl;
  out << "#  2 - slag[1]" << endl;
  out << "#  3 - slag[2]" << endl;
 
  out << 0 << "\t" << 0 << "\t" << 0 << "\t" << 0 << endl;
  for(int i=1;i<nSlags;i++) {
    int lag0 = 0;
    int lag1 = i%2 ? i : nSlags-i+1;
    int lag2 = i%2 ? nSlags-i : i-1;
    out << i << "\t" << lag0 << "\t" << lag1 << "\t" << lag2 << endl;
  }

  out.close();

  cout << endl << "Created file : " << ofname << endl << endl;

  exit(0);
}

