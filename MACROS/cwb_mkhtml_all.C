/*
# Copyright (C) 2019 Gabriele Vedovato
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include "ReadLoudestList.C"
#include "ReadPeriodList.C"

#define PERIOD_MAX_SIZE         10
#define LOUDEST_MAX_SIZE        1000
#define LOUDEST_LIST_SIZE       100
#define MAX_LIST_SIZE   	100     // maximum number of input root files in the input file list

#define DAY			(24.*3600.)

#define WWW_PUBLIC		"https://ldas-jobs.ligo.caltech.edu/~waveburst/reports/"
#define WWW_LAG_MANUAL          "https://gwburst.gitlab.io/documentation/latest/html/faq.html?highlight=lag#what-are-lags-and-how-to-use-them"
#define WWW_SLAG_MANUAL         "https://gwburst.gitlab.io/documentation/latest/html/faq.html?highlight=lag#what-are-super-lags-and-how-to-use-them"


int  ReadChunkDirs(TString fList, vector<TString>& chunk_dir, vector<int>& chunk_id);
void mkhtml_index(vector<TString> chunk_report, TString odir);
void GetPeriod(TString ifile, TString irun, double& xstart, double& xstop, double& obsTime);
void ModifyFontSizeCSS(TString tabber);

void cwb_mkhtml_all(TString fList, TString run, TString search, int lag, int slag, TString wlabel, TString path) {

  cout<<"cwb_mkhtml_all.C starts..."<<endl;

  bool bbh = false;	// the loudest event list in the html page is without known BBH

  bool AddFileEntriesToTabs=true;
  if(fList.Sizeof()>0 && fList[0]=='#') {
    AddFileEntriesToTabs=false;
    fList = fList(1,fList.Sizeof()-1);
  }

  // extract odir, tag name from path = odir/tag.png
  TString odir = path(0,path.Last('/'));
  if(odir=="") odir = ".";
  TString rep_dir = odir(odir.Last('/')+1,odir.Sizeof());

  TString file = path(path.Last('/')+1,path.Sizeof());
    
  TString type = file(file.Last('.')+1,file.Sizeof());
  if(type!="png") {
    cout << "cwb_mkhtml_all - Error: path must be: odir/tag.png" << endl;
    exit(1);
  }

  TString tag = file(0,file.Index(".",0));
  if(tag=="") {
    cout << "cwb_mkhtml_all - Error: empty tag -> path must be: odir/tag.png" << endl;
    exit(1);
  }

  CWB::Toolbox::checkFile(odir);	// check if odir exist

  // copy input file list to output report dir
  char cmd[1024];
  sprintf(cmd,"cp %s %s/InputChunkList.txt",fList.Data(),odir.Data());
  gSystem->Exec(cmd);

  // get CWB_USER_URL
  char cwb_user_url[1024] = WWW_PUBLIC;
  if(gSystem->Getenv("CWB_USER_URL")!=NULL) {
    strcpy(cwb_user_url,TString(gSystem->Getenv("CWB_USER_URL")).Data());
  }

  search.ReplaceAll(":"," ");
  char search_title[512];
  sprintf(search_title,"%s Search : Multiple Chunks",search.Data());

  char ifile_period[1024];
  if(bbh) sprintf(ifile_period,"%s/%s_bbh_period.txt",odir.Data(),tag.Data());
  else    sprintf(ifile_period,"%s/%s_nobbh_period.txt",odir.Data(),tag.Data());
  cout << ifile_period << endl;

  double xstart=0; 
  double xstop=0;
  double obsTime=0;
  GetPeriod(ifile_period, run, xstart, xstop, obsTime);
  cout.precision(14);
  cout << xstart << " " << xstop << " " << obsTime << endl;

  double interval = (xstop-xstart)/DAY;

  wat::Time beg_date(xstart);
  wat::Time end_date(xstop);

  TString sbeg_date = beg_date.GetDateString();sbeg_date.Resize(19);
  TString send_date = end_date.GetDateString();send_date.Resize(19);

  char period[1024];
  sprintf(period,"GPS Interval [%d,%d]. UTC Interval %s - %s. Interval duration = %.2f days.",int(xstart),int(xstop),sbeg_date.Data(),send_date.Data(),interval);

  char box_title[1024];
  if(lag==0 && slag==0) 
    sprintf(box_title,"Open Box Result");
  else
    sprintf(box_title,"Fake Open Box Result - ( <td><a href=\"%s\" target=\"_blank\">LAG</a></td> = %d - <td><a href=\"%s\" target=\"_blank\">SLAG</a></td> = %d )",WWW_LAG_MANUAL,lag,WWW_SLAG_MANUAL,slag);

  // get livetime   
  char livetime[1024];
  sprintf(livetime,"Livetime -   Foreground: %.2f days",obsTime/DAY);

  char ifile_loudest_bbh[1024];
  sprintf(ifile_loudest_bbh,"%s_bbh_loudest.txt",tag.Data());
  char ifile_loudest_nobbh[1024];
  sprintf(ifile_loudest_nobbh,"%s_nobbh_loudest.txt",tag.Data());

  // create body.html file

  ofstream out;
  char fileout[1024];
  sprintf(fileout,"%s/body.html", odir.Data());
  cout << fileout << endl;
  out.open(fileout,ios::out);
  if (!out.good()) {cout << "Error Opening File : " << fileout << endl;exit(1);}

  out << "<html>" << endl;

//  out << "<br>" << endl;
  out << "<div align=\"center\"><font color=\"blue\"><h1>" << search_title << "</h1></font></div>" << endl;
  out << "<div align=\"center\"><font color=\"red\"><h4>"<< box_title <<"</h4></font></div>" << endl;
  out << "<div align=\"center\">" << endl;
  out << "<a target=\"_blank\" href=\"InputChunkList.txt\">(Input Chunk List) </a>" << endl;
  out << "</div>" << endl;
  out << "<div align=\"center\"><font color=\"black\"><h4>"<< period <<"</h4></font></div>" << endl;
  out << "<div align=\"center\"><font color=\"black\"><h4>"<< livetime <<"</h4></font></div>" << endl;
//  out << "<br>" << endl;

  out << "<hr>" << endl;
  out << "<br>" << endl;
  out << "<br>" << endl;

  out << "<table>" << endl;
  out << "<tr><td width=\"50%\"><div align=\"center\">" << endl;
  out << "<font color=\"red\"><h2>Cumulative Number vs IFAR</h2></font>" << endl;
  out << "<font color=\"red\"><h3>(Included Known BBH)</h3></font>" << endl;
  out << "<a target=\"_blank\" href=\""<< ifile_loudest_bbh << "\">(Loudest Event List) </a>" << endl;
  out << "</div><div align=\"center\"><ul><br/>" << endl;
  out << "<a class=\"image\" title=\"" << tag << "\">" << endl;
  out << "<img src=\"" << tag << "_bbh_plot.png\" width=\"470\"> </a>" << endl;
  out << "</br></ul>" << endl;
  out << "</div><br><br></td><td width=\"50%\"><div align=\"center\">" << endl;
  out << "<font color=\"red\"><h2>Cumulative Number vs IFAR</h2></font>" << endl;
  out << "<font color=\"red\"><h3>(Excluded Known BBH)</h3></font>" << endl;
  out << "<a target=\"_blank\" href=\""<< ifile_loudest_nobbh << "\">(Loudest Event List) </a>" << endl;
  out << "</div><div align=\"center\"><ul><br/>" << endl;
  out << "<a class=\"image\" title=\"" << tag << "\">" << endl;
  out << "<img src=\"" << tag << "_nobbh_plot.png\" width=\"470\"> </a>" << endl;
  out << "</br></ul>" << endl;
  out << "</div><br><br></td></tr>" << endl;
  out << "</table>" << endl;


  gSystem->Exec("date");

  // ---------------------------------------------------------------------
  // OPEN LOUDEST LIST FILE
  // ---------------------------------------------------------------------

  TString run_loudest[LOUDEST_MAX_SIZE];
  int     chunk_loudest[LOUDEST_MAX_SIZE];
  double  gps_loudest[LOUDEST_MAX_SIZE];
  TString bbh_name_loudest[LOUDEST_MAX_SIZE];
  double  ifar_sec_loudest[LOUDEST_MAX_SIZE];
  double  ifar_year_loudest[LOUDEST_MAX_SIZE];
  double  obs_time_sec_loudest[LOUDEST_MAX_SIZE];
  double  obs_time_day_loudest[LOUDEST_MAX_SIZE];
  double  expected_loudest[LOUDEST_MAX_SIZE];
  int     observed_loudest[LOUDEST_MAX_SIZE];
  double  cumul_FAP_loudest[LOUDEST_MAX_SIZE];
  double  sigma_loudest[LOUDEST_MAX_SIZE];

  char ifile_bbh_loudest[1024];
  sprintf(ifile_bbh_loudest,"%s/%s_bbh_loudest.txt",odir.Data(),tag.Data());
  char ifile_nobbh_loudest[1024];
  sprintf(ifile_nobbh_loudest,"%s/%s_nobbh_loudest.txt",odir.Data(),tag.Data());
  cout << ifile_nobbh_loudest << endl;

  int nLoudest = ReadLoudestList(ifile_nobbh_loudest, run_loudest, chunk_loudest, gps_loudest, bbh_name_loudest, ifar_sec_loudest, ifar_year_loudest,
                                 obs_time_sec_loudest, obs_time_day_loudest, expected_loudest, observed_loudest, cumul_FAP_loudest, sigma_loudest); 
  cout << "nLoudest = " << nLoudest << endl;

  char os[1024];

  out << "<head>" << endl;
  out << "<style type=\"text/css\">" << endl;
  out << ".datagrid tr:hover td" << endl;
  out << "{" << endl;
  out << "        background-color:#F1F1F2;" << endl;
  out << "}" << endl;
  out << "</style>" << endl;
  out << "</head>" << endl;
  out << "<b>" << endl;
  out << "<hr>" << endl;
  out << "<br>" << endl;
  out << "<font color=\"red\" style=\"font-weight:bold;\"><center><p><h2>Foreground Loudest Event List (Excluded known BBH)</h2><p><center></font>" << endl;
  out << "<font color=\"black\" style=\"font-weight:bold;\"><center><p><h3>(Ranked with IFAR)</h3><p><center></font>" << endl;
  out << "<br>" << endl;
  out << "</html>" << endl;

  out << "<table border=0 cellpadding=2 class=\"datagrid\">" << endl;
  out << "<tr align=\"center\">"<< endl;
  out << "<td>ID</td>"<< endl;
  out << "<td>IFAR(yrs)</td>"<< endl;
  out << "<td>     GPS            </td>"<< endl;
  out << "<td>chunk</td>"<< endl;
  out << "<td>Expected-Events</td>"<< endl;
  out << "<td>Observed-Events</td>"<< endl;
  out << "<td>Cumulative-FAP</td>"<< endl;
  out << "<td>Sigma</td>"<< endl;
  out << "</tr>"<< endl;

  int nList=0;
  for(int i=0; i<nLoudest; i++) {

    if(i>=LOUDEST_LIST_SIZE) continue;

    out << "<tr align=\"center\">"<< endl;

    sprintf(os,"<td>%.i</td>",i+1);
    out << os << endl;
    sprintf(os,"<td>%.2f</td>",ifar_year_loudest[i]);
    out << os << endl;
    sprintf(os,"<td>%3.3f</td>",gps_loudest[i]);
    out << os << endl;
    sprintf(os,"<td>%i</td>",chunk_loudest[i]);
    out << os << endl;
    sprintf(os,"<td>%3.3f</td>",expected_loudest[i]);
    out << os << endl;
    sprintf(os,"<td>%i</td>",observed_loudest[i]);
    out << os << endl;
    sprintf(os,"<td>%.3f</td>",cumul_FAP_loudest[i]);
    out << os << endl;
    sprintf(os,"<td>%.2f</td>",sigma_loudest[i]);
    out << os << endl;

    out << "</tr>" << endl;

    nList++;

  } // End event loop

  out << "</table>" << endl;
  out << "<p>" << endl;
  out << endl;
  out.close();

  // create html index
  vector<TString> chunk_report;
  char link[1024];

  char options[1024];
  sprintf(link,"%s/%s/dump/%s/",cwb_user_url,wlabel.Data(),rep_dir.Data());
  int high = 1100+26*nList;
  sprintf(options,"--link %s --label ALL --name body.html --high %d",link,high);
  chunk_report.push_back(options);

  if(fList!="" && AddFileEntriesToTabs) {
    vector<TString> chunk_dir;
    vector<int> chunk_id;
    ReadChunkDirs(fList, chunk_dir, chunk_id);
    for(int i=0;i<chunk_dir.size();i++) {
      sprintf(link,"%s/%s/dump/%s/",cwb_user_url,chunk_dir[i].Data(),rep_dir.Data());
      sprintf(options,"--link %s --label K%d --name body.html",link,chunk_id[i]);
      chunk_report.push_back(options);
      cout << i << "\t" << options << endl;
      //cout << i << "\t" << chunk_dir[i] << " " << rep_dir << endl;
    }
  }
  mkhtml_index(chunk_report, odir);

  exit(0);
}

void GetPeriod(TString ifile, TString irun, double& xstart, double& xstop, double& obsTime) {

  TString run[PERIOD_MAX_SIZE];
  double  gps_start[PERIOD_MAX_SIZE];
  TString date_start[PERIOD_MAX_SIZE];
  double  gps_stop[PERIOD_MAX_SIZE];
  TString date_stop[PERIOD_MAX_SIZE];
  double  interval_day[PERIOD_MAX_SIZE];
  int     obs_time_sec[PERIOD_MAX_SIZE];
  double  obs_time_day[PERIOD_MAX_SIZE];

  int nPeriod = ReadPeriodList(ifile, run, gps_start, date_start, gps_stop, date_stop,
                                  interval_day, obs_time_sec, obs_time_day); 
  
  for(int i=0;i<nPeriod;i++) {
    if(run[i]==irun) {
      xstart = gps_start[i];
      xstop  = gps_stop[i];
      obsTime = obs_time_sec[i];
    }
  }
}

void mkhtml_index(vector<TString> chunk_report, TString odir) {

  CWB::Toolbox TB;

  ofstream out;
  char ofile[1024];
  if(odir=="") {
    sprintf(ofile,"index.html");
  } else {
    sprintf(ofile,"%s/index.html", odir.Data());
  }
  cout << "make index html file : " << ofile << endl;
  out.open(ofile,ios::out);
  if (!out.good()) {cout << "mkhtml_index : Error Opening File : " << ofile << endl;exit(1);}

  // open input index template file
  char  html_index_template[1024]="";

  if(gSystem->Getenv("CWB_HTML_INDEX")==NULL) {
    cout << "Error : environment CWB_HTML_INDEX is not defined!!!" << endl;exit(1);
  } else {
    strcpy(html_index_template,gSystem->Getenv("CWB_HTML_INDEX"));
  }
  TB.checkFile(html_index_template);

  ifstream in;
  in.open(html_index_template,ios::in);
  if (!in.good()) {
    cout << "mkhtml_index : Error Opening File : " << html_index_template << endl;
    exit(1);
  }

  char istring[1024];
  while (1) {
    in.getline(istring,1024);
    if (!in.good()) break;
    TString ostring(istring);
    out << ostring.Data() << endl;
  }

  out << "<html>" << endl;
  out << "<br>" << endl;

  // make tabber
  char sbody_height[256];
  sprintf(sbody_height,"%d",1800);
  out << "<div class=\"tabber\">" << endl;
  for(int i=0;i<chunk_report.size();i++) if(chunk_report[i]!="") {

    TString chunk_report_link = CWB::Toolbox::getParameter(chunk_report[i],"--link");
    if(chunk_report_link=="" && i!=0) {
      cout<<"mkhtml_index : Error : chunk_report --link not defined"<<endl;exit(1);}

    TString chunk_report_label = CWB::Toolbox::getParameter(chunk_report[i],"--label");
    if((chunk_report_link!="</tab>/")&&(chunk_report_label=="")) {
      cout<<"mkhtml_index : Error : chunk_report --label not defined"<<endl;exit(1);}

    TString chunk_report_high = CWB::Toolbox::getParameter(chunk_report[i],"--high");
    if(chunk_report_high=="") chunk_report_high=sbody_height;
    int ichunk_report_high = chunk_report_high.Atoi();

    TString chunk_report_name = CWB::Toolbox::getParameter(chunk_report[i],"--name");

    if(chunk_report_link=="<tab>/") {           // open a sub tab
      out << "<div class=\"tabbertab\">" << endl;
      out << "  <h2>" << chunk_report_label << "</h2>" << endl;
      out << "<div class=\"tabber\">" << endl;
    } else if(chunk_report_link=="</tab>/") {   // close sub tab
      out << "</div>" << endl;
      out << "</div>" << endl;
    } else {                                  // add a tab
      out << "<div class=\"tabbertab\">" << endl;
      out << "  <h2>" << chunk_report_label << "</h2>" << endl;

      if(chunk_report_name=="") {
        out << "  <iframe src=\"" << chunk_report_link << "header.html\" width=\"100%\" height=\"900px\" "
            << "marginwidth=\"15\" marginheight=\"15\" frameborder=\"0\"></iframe>" << endl;

        out << "  <iframe src=\"" << chunk_report_link << "body.html\" width=\"100%\" "
            << " height=\"" << ichunk_report_high << "px\" frameborder=\"0\"></iframe>" << endl;
      } else {
        out << "  <iframe src=\"" << chunk_report_link << chunk_report_name << "\" width=\"100%\" "
            << " height=\"" << ichunk_report_high << "px\" frameborder=\"0\"></iframe>" << endl;
      }

      out << "</div>" << endl;
    }
  }
  out << "</div>" << endl;

  out << "</html>" << endl;

  in.close();
  out.close();

  // copy javascripts & Cascading Style Sheets to report the directory
  char cmd[1024];
  sprintf(cmd,"cp %s/html/etc/html/ROOT.css %s/",gSystem->ExpandPathName("$HOME_WAT"),odir.Data());
  gSystem->Exec(cmd);
  sprintf(cmd,"cp %s/html/etc/html/ROOT.js %s/",gSystem->ExpandPathName("$HOME_WAT"),odir.Data());
  gSystem->Exec(cmd);
  sprintf(cmd,"cp %s/html/etc/html/tabber.css %s/",gSystem->ExpandPathName("$HOME_WAT"),odir.Data());
  gSystem->Exec(cmd);
  sprintf(cmd,"cp %s/html/etc/html/tabber.js %s/",gSystem->ExpandPathName("$HOME_WAT"),odir.Data());
  gSystem->Exec(cmd);

  ModifyFontSizeCSS(odir+"/tabber.css");
}

int ReadChunkDirs(TString fList, vector<TString>& chunk_dir, vector<int>& chunk_id) {

  // Read chunk file dirs

  ifstream in;
  in.open(fList.Data(),ios::in);
  if (!in.good()) {cout << "Error Opening File : " << fList.Data() << endl;exit(1);}

  int size=0;
  char str[1024];
  int fpos=0;
  while(true) {
    in.getline(str,1024);
    if (!in.good()) break;
    if(str[0] != '#') size++;
  }
  in.clear(ios::goodbit);
  in.seekg(0, ios::beg);
  if (size==0) {cout << "Error : File " << fList.Data() << " is empty" << endl;exit(1);}
  if (size>MAX_LIST_SIZE) {cout << "Error : Files in " << fList.Data() << " > " << MAX_LIST_SIZE << endl;exit(1);}

  char sfile[1024];
  int xlag,xslag,ichunk,ibin;
  char srun[256];
  int nfile=0;
  while(true) {

    in >> sfile >> xlag >> xslag >> ichunk >> ibin >> srun;
    if(!in.good()) break;
    if(sfile[0]=='#') continue;

    // extract working dir from wave file name
    TString path=sfile;
    TString file = path(path.Last('/')+1,path.Sizeof());
    file.ReplaceAll("wave_","");
    TString dir = file(0,file.First('.'));
    //TString mlabel = file(file.First('.')+1,file.Sizeof());
    //mlabel = mlabel(0,mlabel.First('.'));
    chunk_dir.push_back(dir);
    chunk_id.push_back(ichunk);

    nfile++;
  }
  in.close();

  return nfile;
}

void ModifyFontSizeCSS(TString tabber) {

  ifstream in;
  in.open(tabber.Data(),ios::in);
  if (!in.good()) {cout << "Error Opening File : " << tabber.Data() << endl;exit(1);}

  ofstream out;
  TString tabber_tmp = tabber+".tmp";
  out.open(tabber_tmp,ios::out);
  if (!out.good()) {cout << "Error Opening File : " << tabber_tmp << endl;exit(1);}

  char str[1024];
  while(true) {
    in.getline(str,1024);
    if (!in.good()) break;
    TString ostr = str;
    ostr.ReplaceAll("0.8em","0.75em");
    out << ostr.Data() << endl; 
  }
  out.close();
  in.close();

  char cmd[1024];
  sprintf(cmd,"mv %s %s",tabber_tmp.Data(),tabber.Data());
  //cout << cmd << endl;
  gSystem->Exec(cmd);
}
