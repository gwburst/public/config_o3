/*
# Copyright (C) 2019 Gabriele Vedovato
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#define LOUDEST_MAX_SIZE	1000

/*
  TString run[LOUDEST_MAX_SIZE];
  int     chunk[LOUDEST_MAX_SIZE];
  double  gps[LOUDEST_MAX_SIZE];
  TString bbh_name[LOUDEST_MAX_SIZE];
  double  ifar_sec[LOUDEST_MAX_SIZE];
  double  ifar_year[LOUDEST_MAX_SIZE];
  int     obs_time_sec[LOUDEST_MAX_SIZE];
  double  obs_time_day[LOUDEST_MAX_SIZE];
  double  expected[LOUDEST_MAX_SIZE];
  int     observed[LOUDEST_MAX_SIZE];
  double  cumul_FAP[LOUDEST_MAX_SIZE];
  double  sigma[LOUDEST_MAX_SIZE];

  int nLoudest = ReadLoudestList(ifile, run, chunk, gps, bbh_name, ifar_sec, ifar_year,
                                 obs_time_sec, obs_time_day, expected, observed, cumul_FAP, sigma); 
  
*/

int ReadLoudestList(TString ifile, TString* run, int* chunk, double* gps, TString* bbh_name, double* ifar_sec, double* ifar_year,
                    double* obs_time_sec, double* obs_time_day, double* expected, int* observed, double* cumul_FAP, double* sigma) {

  CWB::Toolbox::checkFile(ifile);

  // Open chunk list
  ifstream in;
  in.open(ifile.Data(),ios::in);
  if (!in.good()) {cout << "Error Opening File : " << ifile << endl;exit(1);}

  int isize=0;
  char str[1024];
  int fpos=0;
  while(true) {
    in.getline(str,1024);
    if (!in.good()) break;
    if(str[0] != '#') isize++;
  }
//  cout << "size " << isize << endl;
  in.clear(ios::goodbit);
  in.seekg(0, ios::beg);
  if(isize==0)  {cout << "Error : File " << ifile << " is empty" << endl;return 0;}
  if(isize>LOUDEST_MAX_SIZE) {cout << "Error : File " << ifile << " > " << LOUDEST_MAX_SIZE << endl;exit(1);}
 
  char srun[256];
  char sbbh[256];

  int k=0;
  while(true) {
    fpos=in.tellg();
    in.getline(str,1024);
    if (!in.good()) break;
    if(str[0] == '#' || str[0]=='\0') continue;
    in.seekg(fpos, ios::beg);
    in >> srun >> chunk[k] >> gps[k] >> sbbh >> ifar_sec[k] >> ifar_year[k] >> obs_time_sec[k] 
       >> obs_time_day[k] >> expected[k] >> observed[k] >> cumul_FAP[k] >> sigma[k];
    if(!in.good()) break;
    run[k]=srun;
    bbh_name[k]=sbbh;
    //cout << "\t" << run[k] << "\t" << chunk[k] << "\t" << gps[k] << "\t" << bbh_name[k] << "\t" << ifar_sec[k] << "\t" << ifar_year[k] << "\t" << obs_time_sec[k] 
    //     << "\t" << obs_time_day[k] << "\t" << expected[k] << "\t" << observed[k] << "\t" << cumul_FAP[k] << "\t" << sigma[k] << endl;
    k++;
    if(k>=LOUDEST_MAX_SIZE) {
      cout << "WARNING: loudest events exceed LOUDEST_MAX_SIZE = " << LOUDEST_MAX_SIZE << endl; 
      break;
    }
  }
  in.close();

  return k;
}
